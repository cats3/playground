﻿namespace MEFPlayground
{
    public interface ICalculator
    {
        string Calculate(string input);
    }
}
