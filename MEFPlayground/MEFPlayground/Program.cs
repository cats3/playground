﻿using MEFPlayground;
using MEFPlayground.CommonLibrary;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;

public class Program
{
    [ImportMany]
    public IEnumerable<Lazy<IOperation, IOperationData>>? operations;

    private CompositionContainer? _container;

    [Import(typeof(string))]
    public ICalculator? calculator;

    private Program()
    {
        try
        {
            // An aggregate catalog that combines multiple catalogs.
            var catalog = new AggregateCatalog();
            // Adds all the parts found in the same assembly as the Program class.
            catalog.Catalogs.Add(new AssemblyCatalog(typeof(Program).Assembly));
            catalog.Catalogs.Add(new DirectoryCatalog("C:\\cubus-dev\\playground\\MEFPlayground\\MEFPlayground\\Extensions"));
            // Create the CompositionContainer with the parts in the catalog.
            _container = new CompositionContainer(catalog);
            _container.ComposeParts(this);
        }
        catch (CompositionException compositionException)
        {
            Console.WriteLine(compositionException.ToString());
        }
    }
    static void Main(string[] args)
    {
        // Composition is performed in the constructor.
        var p = new Program();
        Console.WriteLine("Operations are:");
        foreach (Lazy<IOperation, IOperationData> i in p.operations)
        {
            Console.WriteLine(i.Value.ToString() + $"'{i.Metadata.Symbol}'");
        }
        Console.WriteLine("Enter Command:");
        while (true)
        {
            string? s = Console.ReadLine();
            Console.WriteLine(p.calculator?.Calculate(s ?? ""));
        }
    }
}