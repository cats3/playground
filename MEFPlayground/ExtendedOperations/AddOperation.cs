﻿using MEFPlayground.CommonLibrary;
using System.ComponentModel.Composition;

namespace MEFPlayground
{
    [Export(typeof(IOperation))]
    [ExportMetadata("Symbol", '+')]
    class Add : IOperation
    {
        public int Operate(int left, int right)
        {
            return left + right;
        }
    }
}
