﻿namespace MEFPlayground.CommonLibrary
{
    public interface IOperationData
    {
        char Symbol { get; }
    }
}
