﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace ManyItemsListView.MVVM.Model
{
    public class ProjectItem
    {
        public string Path { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        //public List<SpecimenItem> Specimens { get; private set; }
        public ProjectItem()
        {
        }
        public ProjectItem(string path)
        {
            Path = path;
            try
            {
                if (path == default)
                    throw new ArgumentNullException();

                Type = GetProjectTypeFromPath(path);
                Created = File.GetCreationTime(path);
                Modified = File.GetLastWriteTime(path);
                Name = System.IO.Path.GetFileNameWithoutExtension(path);
                //Specimens = GetSpecimensFromPath(path);
            }
            catch
            {
                Name = "Null project";
                Type = "Unknown type";
                Created = DateTime.Now;
                Modified = DateTime.Now;
            }
        }

        //private List<SpecimenItem> GetSpecimensFromPath(string path)
        //{
        //    List<SpecimenItem> specimens = new List<SpecimenItem>();
        //    if (String.IsNullOrEmpty(path))
        //        return specimens;

        //    if (ProjectTypes.IsGenericProjectType(path))
        //        specimens.AddRange(GetGenericSpecimensFromPath(System.IO.Path.GetDirectoryName(path)));
        //    else
        //        specimens.AddRange(GetLegacySpecimensFromPath(path));

        //    return specimens;
        //}

        //private IEnumerable<SpecimenItem> GetLegacySpecimensFromPath(string path)
        //{
        //    return new List<SpecimenItem>();
        //}

        //private IEnumerable<SpecimenItem> GetGenericSpecimensFromPath(string path)
        //{
        //    List<SpecimenItem> specimenItems = new List<SpecimenItem>();
        //    string specimensPath = System.IO.Path.Combine(path, "Data");
        //    if (Directory.Exists(specimensPath))
        //    {
        //        foreach (var d in Directory.EnumerateDirectories(specimensPath))
        //        {
        //            specimenItems.Add(new SpecimenItem(System.IO.Path.Combine(specimensPath, d)));
        //        }
        //    }
        //    return specimenItems;
        //}

        private string GetProjectTypeFromPath(string path)
        {
            string type = ProjectTypes.GetProjectTypeFromPathExtension(path);
            if (!ProjectTypes.IsGenericProjectType(type))
                return type;
            return GetProjectTypeFromXML(path) ?? type;
        }

        private string GetProjectTypeFromXML(string path)
        {
            string type = default;
            try
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(path);
                XmlElement root = doc.DocumentElement;
                if (root.HasAttribute("ProjectGUID"))
                {
                    var guid = root.GetAttribute("ProjectGUID");
                    //if (Features.CustomTests.ContainsKey(guid))
                    //    type = Features.CustomTests[guid]?.Name;
                }
            }
            catch (IOException)
            {
                //Debug.WriteLine("IOException");
            }
            return type;
        }
    }

}
