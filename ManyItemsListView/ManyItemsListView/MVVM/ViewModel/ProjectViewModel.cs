﻿using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using ManyItemsListView.MVVM.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ManyItemsListView.MVVM.ViewModel
{
    public class ProjectViewModel : ViewModelBase
    {
        private readonly IProjectService _projectService;
        private ObservableCollection<ProjectItem> _projects = new ObservableCollection<ProjectItem>();
        public ObservableCollection<ProjectItem> Projects
        {
            get { return _projects; }
            set { Set(ref _projects, value); }
        }
        public CollectionView Items => ItemsViewSource.View as CollectionView;
        public CollectionViewSource ItemsViewSource { get; set; }
        private string _filterText;
        public string FilterText { get { return _filterText; } set { _filterText = value; Items.Refresh(); } }

        private RelayCommand sortByIdCommand;
        public RelayCommand SortByIdCommand
        {
            get { return sortByIdCommand ?? (sortByIdCommand = new RelayCommand(SortByCreatedDate)); }
        }

        private RelayCommand sortByNameCommand;
        public RelayCommand SortByNameCommand
        {
            get { return sortByNameCommand ?? (sortByNameCommand = new RelayCommand(SortByName)); }
        }

        private void SortByCreatedDate()
        {
            ItemsViewSource.SortDescriptions.Clear();
            ItemsViewSource.SortDescriptions.Add(new SortDescription(nameof(ItemViewModel.Created), ListSortDirection.Ascending));
        }

        private void SortByName()
        {
            ItemsViewSource.SortDescriptions.Clear();
            ItemsViewSource.SortDescriptions.Add(new SortDescription(nameof(ItemViewModel.Name), ListSortDirection.Ascending));
        }


        public ProjectViewModel()
        {
            Projects = new ObservableCollection<ProjectItem>();
        }
        public ProjectViewModel(IProjectService projectService)
        {
            _projectService = projectService;
            _projectService.ProjectsChanged += _projectService_ProjectsChanged;
            GetProjects();
            ItemsViewSource = new CollectionViewSource { Source = Projects };
            ItemsViewSource.GroupDescriptions.Add(new PropertyGroupDescription(nameof(ProjectItem.Type)));
            ItemsViewSource.Filter += ItemsViewSource_Filter;
        }

        private void _projectService_ProjectsChanged(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(delegate { foo(); });
        }

        private void foo()
        {
            GetProjects();
        }

        private void ItemsViewSource_Filter(object sender, FilterEventArgs e)
        {
            if (String.IsNullOrEmpty(FilterText))
                return;
            if (e.Item is ProjectItem item)
            {
                e.Accepted = item.Name.Contains(FilterText);
            }
        }

        public async void GetProjects()
        {
            _projectService.ProjectsChanged -= _projectService_ProjectsChanged;
            List<ProjectItem> projectItems = await _projectService.GetProjects("C:\\Cubus Projects\\");
            Projects.Clear();
            foreach(ProjectItem item in projectItems)
                Projects.Add(item);
            //Projects = new ObservableCollection<ProjectItem>(projectItems);
            RaisePropertyChanged(nameof(Projects));
            _projectService.ProjectsChanged += _projectService_ProjectsChanged;
        }
    }
}
