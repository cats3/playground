﻿using ManyItemsListView.MVVM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyItemsListView
{
    public interface IProjectService
    {
        Task<List<ProjectItem>> GetProjects(string path);
        event EventHandler ProjectsChanged;
    }
}
