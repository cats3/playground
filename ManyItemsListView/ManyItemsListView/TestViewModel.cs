﻿using GalaSoft.MvvmLight.Command;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Data;

namespace ManyItemsListView
{
    public class TestViewModel
    {
        private readonly List<ItemViewModel> _items;
        public CollectionView Items => ItemsViewSource.View as CollectionView;
        public CollectionViewSource ItemsViewSource { get; set; }
        private string _filterText;
        public string FilterText { get { return _filterText; } set { _filterText = value; Items.Refresh(); } } 

        private RelayCommand sortByIdCommand;
        public RelayCommand SortByIdCommand
        {
            get { return sortByIdCommand ?? (sortByIdCommand = new RelayCommand(SortById)); }
        }

        private RelayCommand sortByNameCommand;
        public RelayCommand SortByNameCommand
        {
            get { return sortByNameCommand ?? (sortByNameCommand = new RelayCommand(SortByName)); }
        }

        private void SortById()
        {
            ItemsViewSource.SortDescriptions.Clear();
            ItemsViewSource.SortDescriptions.Add(new SortDescription(nameof(ItemViewModel.Id), ListSortDirection.Ascending));
        }

        private void SortByName()
        {
            ItemsViewSource.SortDescriptions.Clear();
            ItemsViewSource.SortDescriptions.Add(new SortDescription(nameof(ItemViewModel.Name), ListSortDirection.Ascending));
        }

        public TestViewModel()
        {
            _items = new List<ItemViewModel>();
            for (int i = 0; i < 500; i++)
            {
                _items.Add(new ItemViewModel("Apples"));
                _items.Add(new ItemViewModel("Pears"));
                _items.Add(new ItemViewModel("Bananas"));
            }

            for (int i = 0; i < 123; i++)
            {
                _items.Add(new ItemViewModel("Grapes"));
                _items.Add(new ItemViewModel("Oranges"));
                _items.Add(new ItemViewModel("Melons"));
            }

            ItemsViewSource = new CollectionViewSource { Source = _items };
            SortById();
            GroupByFruit();
            ItemsViewSource.Filter += ItemsViewSource_Filter;
        }

        private void ItemsViewSource_Filter(object sender, FilterEventArgs e)
        {
            if (String.IsNullOrEmpty(FilterText))
                return;
            if (e.Item is ItemViewModel itemViewModel)
            {
                e.Accepted = itemViewModel.Name.Contains(FilterText);
            }
        }

        private void GroupByFruit()
        {
            ItemsViewSource.GroupDescriptions.Clear();
            ItemsViewSource.GroupDescriptions.Add(new PropertyGroupDescription(nameof(ItemViewModel.Group)));
        }
    }

    public class ItemViewModel
    {
        private static int _count = 0;
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public string Group { get; set; }
        public List<SubItemViewModel> Items { get; set; }
        public ItemViewModel(string group)
        {
            Id = _count++;
            Name = Guid.NewGuid().ToString();
            Created = DateTime.Now;
            Modified = DateTime.Now;
            Group = group;
            Items = new List<SubItemViewModel>();
            for (int i = 0; i < 5; i++)
                Items.Add(new SubItemViewModel());
        }
    }

    public class SubItemViewModel
    {
        private static int _count = 0;
        public int Id { get; set; }
        public string Name { get; set; }

        public SubItemViewModel()
        {
            Id = _count++;
            Name = $"Specimen {Id:D02}";
        }
    }


}
