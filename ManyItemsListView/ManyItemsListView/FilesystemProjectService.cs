﻿using ManyItemsListView.MVVM.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyItemsListView
{
    public class FilesystemProjectService : IProjectService
    {
        private FileSystemWatcher watcher;
        private int events = 0;

        public event EventHandler ProjectsChanged;
        private string Path;

        public Task<List<ProjectItem>> GetProjects(string path)
        {
            var projects = new List<ProjectItem>();
            Path = path;
            try
            {
                foreach (var p in Directory.EnumerateFiles(path, "*.*", SearchOption.AllDirectories).Where(e => CheckForPathExtension(e)))
                {
                    projects.Add(new ProjectItem(p));
                }
                CreatePathWatcher();
            }
            catch
            {

            }
            return Task.FromResult(projects);
        }
        private void CreatePathWatcher()
        {
            try
            {
                watcher = new FileSystemWatcher(Path);
                watcher.NotifyFilter = NotifyFilters.DirectoryName | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.Attributes;
                //watcher.Changed += WatcherEvent;
                watcher.Created += WatcherEvent;
                watcher.Deleted += WatcherEvent;
                watcher.Renamed += WatcherEvent;
                watcher.IncludeSubdirectories = true;
                watcher.EnableRaisingEvents = true;
            }
            catch
            {

            }
        }

        private void WatcherEvent(object sender, FileSystemEventArgs e)
        {
            OnProjectsChanged(e);
        }

        protected virtual void OnProjectsChanged(EventArgs e)
        {
            events++;
            ProjectsChanged?.Invoke(this, e);
        }

        private bool CheckForPathExtension(string path)
        {
            if (String.IsNullOrEmpty(path))
                return false;
            return ProjectTypes.ProjectExtensions.Contains(System.IO.Path.GetExtension(path)) && !path.Contains(".backup");
        }
    }

    public static class ProjectTypes
    {
        private static readonly string GENERIC_STRING = "Generic";
        private static readonly string GENERIC_EXTENSION = ".cprj";
        private static Dictionary<string, string> projectExtensions = new Dictionary<string, string>()
        {
            { ".proj_sba", "SBA" },
            { ".proj_advcyc", "Cyclic Pro" },
            { ".proj_blk", "Block" },
            { ".proj_ramp", "Ramp" },
            { ".proj_dura", "Dura" },
            { ".proj_ext", "External" },
            { ".proj_cyc", "Cyclic" },
            { ".proj_tensile", "Tensile" },
            { ".proj_side", "Side Intrusion" },
            { ".proj_albcomp", "Albany Compression" },
            { ".proj_damper", "Damper" },
            { GENERIC_EXTENSION, GENERIC_STRING }
        };

        public static List<string> ProjectExtensions => projectExtensions.Keys.ToList();

        public static string GetProjectTypeFromPathExtension(string file)
        {
            var ext = Path.GetExtension(file);
            if (projectExtensions.ContainsKey(ext))
                return projectExtensions[ext];
            else
                return "Uncategorised";
        }

        public static bool IsGenericProjectType(string file)
        {
            return Path.GetExtension(file).Equals(GENERIC_EXTENSION);
        }
    }
}
