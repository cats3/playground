﻿using ManyItemsListView.MVVM.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ManyItemsListView
{
    public class FakeProjectService : IProjectService
    {
        private readonly IEnumerable<string> _types = new List<string>
        {
            "Elastomer",
            "Cyclic",
            "Cyclic Pro",
            "Ramp",
            "Generic"
        };

        public event EventHandler ProjectsChanged;

        public Task<List<ProjectItem>> GetProjects(string path)
        {
            List<ProjectItem> list = new List<ProjectItem>();
            foreach (var type in _types)
                for (int i = 0; i < 200; i++)
                {
                    list.Add(new ProjectItem
                    {
                        Created = DateTime.Now,
                        Name = Path.GetRandomFileName(),
                        Modified = DateTime.Now,
                        Type = type
                    }); ;
                }
            return Task.FromResult(list);
        }
    }
}
