﻿using GalaSoft.MvvmLight;
using ManyItemsListView;
using ManyItemsListView.MVVM.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Host
{
    public class MainViewModel : ViewModelBase
    {
        private readonly ProjectViewModelFactory _projectViewModelFactory;
        public ProjectViewModel ProjectVM { get; private set; }
        public MainViewModel(ProjectViewModelFactory projectViewModelFactory)
        {
            //_projectViewModelFactory = projectViewModelFactory;
            //CurrentViewModel = _projectViewModelFactory.CreateViewModel();
            ProjectVM = new ProjectViewModel(new FilesystemProjectService());
            RaisePropertyChanged(nameof(ProjectVM));
        }
    }

    public class ProjectViewModelFactory
    {
        private readonly IProjectService _projectService;
        public ProjectViewModelFactory(IProjectService projectService)
        {
            _projectService = projectService;
        }

        public ProjectViewModel CreateViewModel()
        {
            return new ProjectViewModel(_projectService);
        }
    }
}
