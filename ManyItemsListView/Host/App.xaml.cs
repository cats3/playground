﻿using ManyItemsListView;
using ManyItemsListView.MVVM.ViewModel;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace Host
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        private readonly IHost _host;

        public App()
        {
            _host = new HostBuilder().ConfigureServices((services) =>
            {
                services.AddSingleton<IProjectService, FilesystemProjectService>();
                services.AddSingleton<MainViewModel>();
                services.AddSingleton<ProjectViewModelFactory>();
                services.AddSingleton<MainWindow>();
            }).Build();
        }
        protected override void OnStartup(StartupEventArgs e)
        {
            var mainWindow = _host.Services.GetRequiredService<MainWindow>();
            mainWindow.DataContext = _host.Services.GetRequiredService<MainViewModel>();
            mainWindow.Show();
        }

    }
}
