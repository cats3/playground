﻿using TestCheckBoxes.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCheckBoxes.MVVM.ViewModels
{
    public class DummyObjectViewModel : ObservableObject
    {
        private bool selected = false;
        public bool Selected { get { return selected; } set { selected = value; OnPropertyChanged(); } }
        //public bool Selected { get; set; } 
        private string name;
        public string Name { get { return name; } set { name = value; OnPropertyChanged(); } }
        public DummyObjectViewModel(string name)
        {
            Name = name;
            Selected = false;
        }
    }
}
