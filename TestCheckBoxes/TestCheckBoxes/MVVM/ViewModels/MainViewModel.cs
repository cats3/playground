﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestCheckBoxes.Core;

namespace TestCheckBoxes.MVVM.ViewModels
{
    public class MainViewModel : ObservableObject
    {
        public ObservableCollection<DummyObjectViewModel> DummyObjects { get; set; }
        public int SelectedItems { get { return DummyObjects.Where(d => d.Selected == true).Count(); } private set { } }

        public MainViewModel()
        {
            DummyObjects = new ObservableCollection<DummyObjectViewModel>()
            {
                { new DummyObjectViewModel("foo") },
                { new DummyObjectViewModel("bar") },
                { new DummyObjectViewModel("abc") },
                { new DummyObjectViewModel("def") },
            };
            DummyObjects.ToList().ForEach(o => o.PropertyChanged += O_PropertyChanged);
        }

        private void O_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(DummyObjectViewModel.Selected))
                OnPropertyChanged(nameof(SelectedItems));
        }
    }
}
