﻿using TestCheckBoxes.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestCheckBoxes.MVVM.Models
{
    public class DummyObject
    {
        public string Name { get; set; }
    }
}
