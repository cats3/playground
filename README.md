# Playground

This is a playground repository for playing around with stuff before using in live projects or products.


## Contributing

Anyone can contribute to this repository.

## Individual Projects

### [excelTemplateTest]

A project to test out generic Excel report generation from a template Excel file.  This uses C3 libraries so please checkout
this repository at the same root level as the c3 repository so that the DLLs are easily found.  Naturally you need to build C3 first.
