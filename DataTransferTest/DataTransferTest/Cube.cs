﻿using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace DataTransferTest
{
    public class Cube : IDisposable
    {
        private TcpClient tcpClient = new TcpClient();
        private IPAddress address;

        private StreamWriter writer = null;
        private StreamReader reader = null;
        byte[] receiveBuffer = new byte[1024];
        public int ID { get; set; } = 0;
        public int Timeout { get; set; } = 3000;
        public Cube(string ipAddress, int id)
        {
            ID = id;
            address = IPAddress.Parse(ipAddress);
            Task t = tcpClient.ConnectAsync(address, 49153);
            t.Wait(Timeout);
            if (t.IsCompleted)
            {
                var netStream = tcpClient.GetStream();
                writer = new StreamWriter(netStream);
                reader = new StreamReader(netStream);
            }
        }

        public async Task<string> GetRigName()
        {
            var message = CubeMessage.Create(CubeMessage.GetRigName, ID).ToArray();
            await writer.BaseStream.WriteAsync(message, 0, message.Length);
            Task<int> Read = reader.BaseStream.ReadAsync(receiveBuffer, 0, receiveBuffer.Length);
            Read.Wait(Timeout);
            if (Read.IsCompleted)
                return Encoding.ASCII.GetString(receiveBuffer.ToArray(), 12, 32).ToString();
            return String.Empty;
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!disposed)
            {
               if (disposing)
                {
                    reader.Dispose();
                    writer.Dispose();
                    tcpClient.Dispose();
                }
                disposed = true;
            }
        }
    }
}
