﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace DataTransferTest
{
    class Program
    {
        static ITargetBlock<int> headBlock = null;
        CancellationTokenSource CancellationTokenSource;

        public static string response = String.Empty;

        static ITargetBlock<int> CreateNetwork()
        {
            var startCommunications = new TransformBlock<int, double>(channel =>
            {
                try
                {
                    return 1;
                }
                catch (OperationCanceledException)
                {
                    return 0;
                }
            }
            );

            var displayData = new ActionBlock<double>(data =>
            {
                Console.WriteLine(data);
            },
            new ExecutionDataflowBlockOptions
            {
                //TaskScheduler = TaskScheduler.FromCurrentSynchronizationContext()
            });

            var operationCancelled = new ActionBlock<object>(delegate
            {
                Console.WriteLine("streaming cancellled");
            },
            new ExecutionDataflowBlockOptions
            {
                //TaskScheduler = TaskScheduler.FromCurrentSynchronizationContext()
            });

            startCommunications.LinkTo(displayData);
            return startCommunications;
        }

        public static async Task Main()
        {
            string host = "172.22.4.166";
            using (Cube cube = new Cube(host, 0))
            {
                var name = await cube.GetRigName();
                Console.WriteLine($"Cube rig name is {name}");
            }

            int port = 49152;
            //string host = "192.168.3.22";
            var client = new TcpClient();
            IPAddress address = IPAddress.Parse(host);
            await GetRigName(address);


            await client.ConnectAsync(address, port);
            Console.WriteLine($"Connected to {client.Client.RemoteEndPoint}");

            await SetupChannels(client);
            var foo = await GetTotalBuffer(client);

            if (headBlock == null)
            {
                headBlock = CreateNetwork();
            }
            headBlock.Post(0);

            Console.WriteLine("Press Enter to continue...");
            Console.ReadLine();
            return;
        }

        public static async Task<int> GetTotalBuffer(TcpClient client)
        {
            int timeout = 3000;
            byte[] receiveBuffer = new byte[1024];

            var netstream = client.GetStream();
            var writer = new StreamWriter(netstream);
            var reader = new StreamReader(netstream);
            {
                List<byte> message = new List<byte>();
                message.AddRange(new byte[] { 0, 0, 0, 0 });
                message.AddRange(BitConverter.GetBytes(4));        // msglength
                message.AddRange(BitConverter.GetBytes(0x0359));
                message.AddRange(BitConverter.GetBytes((UInt16)0));

                await writer.BaseStream.WriteAsync(message.ToArray(), 0, message.Count);
                Task<int> Read = reader.BaseStream.ReadAsync(receiveBuffer, 0, 100);
                Read.Wait(timeout);

                if (Read.IsCompleted)
                {
                    return BitConverter.ToInt32(receiveBuffer, 12);
                }
                return 0;
            }

        }
        public static async Task SetupChannels(TcpClient client)
        {
            int timeout = 3000;
            byte[] receiveBuffer = new byte[1024];

            var netstream = client.GetStream();
            var writer = new StreamWriter(netstream);
            var reader = new StreamReader(netstream);
            {
                List<byte> message = new List<byte>();
                message.AddRange(new byte[] { 0, 0, 0, 0 });
                message.AddRange(BitConverter.GetBytes(9));        // msglength
                message.AddRange(BitConverter.GetBytes(0x0358));
                message.AddRange(BitConverter.GetBytes((int)1));    // listsize
                message.Add(0);

                await writer.BaseStream.WriteAsync(message.ToArray(), 0, message.Count);
                Task<int> Read = reader.BaseStream.ReadAsync(receiveBuffer, 0, 100);
                Read.Wait(timeout);
            }
        }
        private static async Task GetRigName(IPAddress address)
        {

            int port = 49152;
            int timeout = 3000;
            byte[] receiveBuffer = new byte[1024];

            using (var client = new TcpClient())
            {
                try
                {
                    await client.ConnectAsync(address, port);
                    Console.WriteLine($"Connected to {client.Client.RemoteEndPoint}");
                    var netstream = client.GetStream();
                    var writer = new StreamWriter(netstream);
                    var reader = new StreamReader(netstream);

                    List<byte> message = new List<byte>();
                    message.Add((byte)0);
                    message.AddRange(new byte[] { 0, 0, 0, 4, 0, 0, 0 });
                    message.AddRange(new byte[] { 0xbb, 0x3, 0, 0 });

                    await writer.BaseStream.WriteAsync(message.ToArray(), 0, message.Count);

                    Task<int> Read = reader.BaseStream.ReadAsync(receiveBuffer, 0, 100);
                    Read.Wait(timeout);

                    if (Read.Status == TaskStatus.RanToCompletion)
                    {
                        response = Encoding.ASCII.GetString(receiveBuffer.ToArray(), 12, 32).ToString();
                        Console.WriteLine($"Received rig_name = {response}");
                    }
                    else
                    {
                        Console.WriteLine("Read did not complete");
                    }

                    reader.Close();
                    writer.Close();
                    netstream.Close();
                }
                catch (SocketException se)
                {
                    Console.WriteLine($"socket exception: {se.Message}");
                }
                catch (ObjectDisposedException ode)
                {
                    Console.WriteLine($"object has been disposed: {ode.Message}");
                }
            }
        }

    }
}
