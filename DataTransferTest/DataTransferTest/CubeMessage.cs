﻿using System;
using System.Collections.Generic;

namespace DataTransferTest
{
    public static class CubeMessage
    {
        private static readonly UInt16 padding = 0;
        public static readonly UInt16 GetRigName = 0x03bb;

        public static byte[] Create(UInt16 messageID, int destination, byte[] payload = null)
        {
            List<byte> message = new List<byte> { (byte)destination, 0, 0, 0 };
            var length = 4;
            if (payload != default)
                length += payload.Length;
            message.AddRange(BitConverter.GetBytes(length));
            message.AddRange(BitConverter.GetBytes(messageID));
            message.AddRange(BitConverter.GetBytes(padding));
            if (payload?.Length > 0)
                message.AddRange(payload);
            return message.ToArray();
        }
    }
}
