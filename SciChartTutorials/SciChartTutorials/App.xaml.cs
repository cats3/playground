﻿using SciChart.Charting.Visuals;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;

namespace SciChartTutorials
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
            // Set this code once in App.xaml.cs or application startup
            SciChartSurface.SetRuntimeLicenseKey("Uh7nU+bwfjdS7sswrLF9VAT2IXynXtIEB18nqL7HDaDCaJyRgIkBOihRvB6K19yt2bV0TU9xTMnkavBof+iSAlnJjJnANaPs5nSUSPS/cPKVdehZUzRp8wJjhHlbz50veiwappVQCzsTE/j5Ft0Qk+XLyadcs67vGB1HQSGeSSxliNXOb0vt6c2cQXslUwIz6cmghkLSEVFwxMaHigSRUoq2wbQtvw7/pjNsYJOd4qkzObvyv1bgXSvMis1gy/Lo+kEKNH/L7bFnFhu/WWK4aUWysvuhSqlt8Z21803XWS0VYEkG5Q9j3i+zb+iKE3HFRIIn82a3ymG7FN6s8lcRYFGDqVp0J2l/8czfE42FPRRkGztNztIS1DzW/Tkbz6IHTZP2wTz/8HesrWxmWA1Tj6VppvOPC1m4cyFiGLRPlhS/6ByzHRznJQIsYV0nOE9mIHKLOHW/v77l1s+mYYaIfmddYPh39d0Dm2GtwPJJvMM=");
        }

    }
}
