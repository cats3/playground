#include "pch.h"
#include "CppUnitTest.h"
#include "../PlayWithParameters/parameter.h"
#include "../PlayWithParameters/IntegerParameter.h"
#include "../PlayWithParameters/GenericParameter.h"
#include <iostream>
#include <stdint.h>

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
	TEST_CLASS(UnitTests)
	{
	public:
		TEST_METHOD(ParameterInitialisationTest)
		{
			string rootName = "Root";
			Parameter param;
			Assert::IsFalse(param.isGood());
			param(nullptr, rootName);
			Assert::AreEqual(param.getName(), rootName);
			Assert::IsTrue(param.isGood());
		}

		TEST_METHOD(ParameterHierarchyTest)
		{
			Parameter root;
			root(nullptr, "root");
			GenericParameter<uint32_t> child;
			child(&root, "child");
			Assert::AreEqual(1, root.getChildrenCount());
			GenericParameter<float> child2;
			child2(&root, "child2");
			Assert::AreEqual(2, root.getChildrenCount());
		}

		TEST_METHOD(IntegerParameterTest)
		{
			const int32_t minValue = 0;
			const int32_t maxValue = 100;
			const int32_t defaultValue = 40;
			
			Parameter root;
			root(nullptr, "root");
			IntegerParameter p;
			p(&root, "integerChild", ParameterFlags::None, nullptr,  minValue, maxValue, "units", defaultValue);
			Assert::AreEqual(maxValue, p.getMax());
			Assert::AreEqual(minValue, p.getMin());
			Assert::AreEqual(1, root.getChildrenCount());

			Assert::AreEqual(defaultValue, p.getValue());
			p = 20;
			Assert::AreEqual(20, p.getValue());
			p = maxValue + 1;
			Assert::AreEqual(maxValue, p.getValue());
			p = minValue -1;
			Assert::AreEqual(minValue, p.getValue());

			p.setValue(50);
			Assert::AreEqual(50, p.getValue());
		}

		TEST_METHOD(IntegerReadOnlyTest)
		{
			class SuperClass : public Parameter {
			public:
				int readOnlyEvents = 0;
				void raiseSetReadOnlyEvent(Parameter* source)
				{
					readOnlyEvents++;
				}
			};

			SuperClass root;
			root(nullptr, "root");
			Assert::AreEqual(0, root.readOnlyEvents);
			IntegerParameter p;
			p(&root, "integerChild", ParameterFlags::ReadOnly, nullptr, 0, 100, "units", 10);
			p = 20;
			Assert::AreEqual(1, root.readOnlyEvents);
			Assert::AreEqual(10, p.getValue());
		}


		TEST_METHOD(GenericTestInstance)
		{
			GenericParameter<int> p;
			Assert::IsFalse(p.isGood());
			p(nullptr, "dummy");
			Assert::IsTrue(p.isGood());

			GenericParameter<float> p2;
			Assert::IsFalse(p2.isGood());
			p2(nullptr, "dummy");
			Assert::IsTrue(p.isGood());

			Parameter root;
			root(nullptr, "root");

			int foo;
			GenericParameter<int> p3;
			p3(&root, "dummy", ParameterFlags::None, &foo, 0, 100, "units", 10);
			Assert::IsTrue(p3.isGood());
			Assert::AreEqual(10, p3.getValue());
			Assert::AreEqual(0, p3.getMin());
			Assert::AreEqual(100, p3.getMax());

			p3 = 50;
			Assert::AreEqual(50, p3.getValue());
			p3 = 200;
			Assert::AreEqual(100, p3.getValue());

			float bar;
			GenericParameter<float> p4;
			p4(&root, "dummy", ParameterFlags::None, &bar, 0.0f, 2.05f, "units", 0.5f);
			Assert::IsTrue(p4.isGood());
			Assert::AreEqual(0.5f, p4.getValue(), 0.00001f);
			Assert::AreEqual(0.0f, p4.getMin());
			Assert::AreEqual(2.05f, p4.getMax());

			p4 = 1.0f;
			Assert::AreEqual(1.0f, p4.getValue());
			p4 = 200;
			Assert::AreEqual(2.05f, p4.getValue());
		}

		TEST_METHOD(TenPercentOverrange)
		{
			GenericParameter<float> p;
			p(nullptr, "dummy", ParameterFlags::AllowOverrange, nullptr, 0.0f, 1.0f, "units", 0.0f);
			p = 2.0f;
			Assert::AreEqual(1.1f, p.getValue());
		}

	};
}
