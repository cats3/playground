// pch.cpp: source file corresponding to the pre-compiled header

#include "pch.h"

// When you are using pre-compiled headers, this source file is necessary for compilation to succeed.
#include "../PlayWithParameters/dwell.cpp"
#include "../PlayWithParameters/parameter.cpp"
#include "../PlayWithParameters/IntegerParameter.cpp"
#include "../PlayWithParameters/GenericParameter.h"
#include "../PlayWithParameters/Generator.cpp"
#include "../PlayWithParameters/GeneratorImplementation.cpp"