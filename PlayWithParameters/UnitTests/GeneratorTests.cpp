#include "pch.h"
#include "CppUnitTest.h"

#include <iostream>
#include <stdint.h>
#include "../PlayWithParameters/Generator.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace UnitTests
{
	TEST_CLASS(GeneratorUnitTests)
	{
	public:
		
		TEST_METHOD(CreateGenerator)
		{
			GeneratorImplementation g;
			Generator gen;
			gen(nullptr, "Generator", g);
			Assert::AreEqual(0, g.cyclesComplete);
			Assert::AreEqual(0, gen.CyclesCompleted.getValue());
		}

		TEST_METHOD(Execute)
		{
			GeneratorImplementation g;
			Generator gen;
			gen(nullptr, "Generator", g);

			g.Execute(0);
			Assert::AreEqual(1, g.cyclesComplete);
			Assert::AreEqual(1, gen.CyclesCompleted.getValue());

		}

	};
}
