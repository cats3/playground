﻿using System;
using Google.Protobuf;
using NetMQ;
using NetMQ.Sockets;

namespace ProtocolBuffers
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("I am the ProtocolBuffer Server!");

            Parameter p = new()
            {
                // Name = "MyParameter",
                Value = 123,
                Uid = 456
            };

            using (var server = new ResponseSocket())
            {
                server.Bind("tcp://*:5556");
                string msg = server.ReceiveFrameString();
                Console.WriteLine("From Client: {0}", msg);
                server.SendFrame(p.ToByteArray());
            }
        }
    }
}
