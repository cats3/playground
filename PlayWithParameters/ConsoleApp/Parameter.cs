﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp
{
    public class Parameter<T>
    {
        public string Name { get; set; } = "Parameter";
        public T Minimum { get; set; }
        public T Maximum { get; set; }

        public Parameter(string name, T minimum, T maximum)
        {
            Name = name;
            Minimum = minimum;
            Maximum = maximum;
        }
    }
}
