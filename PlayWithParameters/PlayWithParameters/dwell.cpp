#include "dwell.h"

void Dwell::Execute() {
	if (handle!=nullptr)
		*handle += 1;
	elapsed++;
}

void Dwell::SetHandle(int* handle)
{
	this->handle = handle;
}

