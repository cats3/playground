#pragma once

#include <stdint.h>
#include <string>

#define PARAMETER_MAGIC_VALUE	0x5ac0ffee

using namespace std;

enum class ParameterFlags : uint32_t {
	None = 0x00,
	ReadOnly = 0x01,
	AllowOverrange = 0x02,
};

inline ParameterFlags operator | (ParameterFlags l, ParameterFlags r)
{
	using T = std::underlying_type_t<ParameterFlags>;
	return static_cast<ParameterFlags>(static_cast<T>(l) | static_cast<T>(r));
}
inline ParameterFlags operator & (ParameterFlags l, ParameterFlags r)
{
	using T = std::underlying_type_t<ParameterFlags>;
	return static_cast<ParameterFlags>(static_cast<T>(l) & static_cast<T>(r));
}


class Parameter {
public:
	int getChildrenCount();
	bool isGood();
	bool isReadOnly() { return static_cast<bool>(flags & ParameterFlags::ReadOnly); }
	bool allowOverrange() { return static_cast<bool>(flags & ParameterFlags::AllowOverrange); }
	void operator()(Parameter* parent, string name);
	string getName();
	Parameter* getNext() { return next; }

	virtual void raiseSetNANEvent(Parameter* source);
	virtual void raiseSetReadOnlyEvent(Parameter* source);
	virtual void raiseSetOverrangeEvent(Parameter* source);

protected:
	uint32_t magic = 0;
	string name;
	void adoptChild(Parameter* child);

	// flags
	ParameterFlags flags;

	// Hierarchy
	Parameter* parent;
	Parameter* tail;
	Parameter* next;
	Parameter* sublist;
};

