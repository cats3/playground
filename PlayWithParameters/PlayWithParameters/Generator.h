#pragma once
#include <string>

#include "parameter.h"
#include "GenericParameter.h"
#include "GeneratorImplementation.h"

class Generator : public Parameter
{
public:
	void operator()(Parameter* parent, string name, GeneratorImplementation& implementation);
	GenericParameter<int> CyclesCompleted;
private:
	GeneratorImplementation* impl;
};

