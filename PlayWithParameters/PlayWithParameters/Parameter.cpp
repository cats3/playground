#include "parameter.h"

using namespace std;

bool Parameter::isGood()
{
	return (magic == PARAMETER_MAGIC_VALUE);
}

void Parameter::operator()(Parameter* parent, string name)
{
	this->parent = parent;
	this->name = name;

	magic = PARAMETER_MAGIC_VALUE;
	if (parent != nullptr)
		parent->adoptChild(this);
}

void Parameter::raiseSetNANEvent(Parameter* source)
{
	if (parent != nullptr)
		parent->raiseSetNANEvent(source);
}

void Parameter::raiseSetReadOnlyEvent(Parameter* source)
{
	if (parent != nullptr)
		parent->raiseSetReadOnlyEvent(source);
}

void Parameter::raiseSetOverrangeEvent(Parameter* source)
{
	if (parent != nullptr)
		parent->raiseSetOverrangeEvent(source);
}
string Parameter::getName()
{
	return name;
}

void Parameter::adoptChild(Parameter* child)
{
	if (sublist != nullptr)
		tail->next = child;
	else
		sublist = child;

	tail = child;
	child->next = nullptr;
}

int Parameter::getChildrenCount()
{
	Parameter* p = sublist;
	int count = 0;
	while (p)
	{
		p = p->getNext();
		count++;
	}
	return count;
}
