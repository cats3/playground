#pragma once
class __declspec(dllexport) Dwell
{
protected:
	int* handle;

public:
	int dwellTime;
	int elapsed;

	Dwell() {
		dwellTime = 0;
		elapsed = 0;
		handle = nullptr;
	}
	void Execute();
	void SetHandle(int* handle);
};

