#pragma once
#include "parameter.h"
#include <stdint.h>
#include <string>

using namespace std;

template <typename T>
class GenericParameter : Parameter
{
public:
	bool isGood() { return Parameter::isGood(); };
	void operator()(Parameter* parent, string name) { Parameter::operator()(parent, name); };
	void operator()(Parameter* parent, string name, ParameterFlags flags, T* source, T min, T max, string units, T defaultValue);
	void operator=(T value);
	T getValue();
	T getMin() { return min; }
	T getMax() { return max; }
private:
	template <typename T>
	void clampValue(T& v)
	{
		if (v < min)
			v = min;
		if (v > max)
			v = max;
	}
	void clampValue(float& v)
	{
		float minValue = allowOverrange() ? 1.1f * min : min;
		float maxValue = allowOverrange() ? 1.1f * max : max;

		if (v < minValue) v = minValue;
		if (v > maxValue) v = maxValue;
	}

	T* source;
	T min;
	T max;
	T defaultValue;
	T localValue;
	T violationValue;
};

template <typename T>
void GenericParameter<T>::operator()(Parameter* parent, string name, ParameterFlags flags, T* source, T min, T max, string units, T defaultValue)
{
	Parameter::operator()(parent, name);
	this->min = min;
	this->max = max;
	this->defaultValue = defaultValue;
	this->flags = flags;
	this->source = source;

	operator=(defaultValue);
}

template <typename T>
void GenericParameter<T>::operator=(T v)
{
	clampValue(v);
	localValue = v;
	if (source != nullptr)
		*source = localValue;
}

template <typename T>
T GenericParameter<T>::getValue()
{
	if (source != nullptr)
	{
		return *source;
	}
	else
	{
		return localValue;
	}
}
