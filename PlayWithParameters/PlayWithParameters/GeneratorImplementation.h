#pragma once
class GeneratorImplementation
{
public:
	GeneratorImplementation()
	{
		cyclesComplete = 0;
	}

	void Execute(int channel);
	int cyclesComplete;
	float output;
};

