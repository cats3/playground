#include "IntegerParameter.h"

void IntegerParameter::operator()(Parameter* parent, string name, ParameterFlags flags, int32_t* source, int32_t min, int32_t max, string units, int32_t defaultValue)
{
	Parameter::operator()(parent, name);
	this->min = min;
	this->max = max;
	this->defaultValue = defaultValue;
	this->flags = flags;

	localValue = defaultValue;
}

void IntegerParameter::operator=(int32_t v)
{
	if (isReadOnly())
	{
		raiseSetReadOnlyEvent(this);
		return;
	}
	if (v < min)
		v = min;
	if (v > max)
		v = max;
	localValue = v;
}

int32_t IntegerParameter::operator()()
{
	return localValue;
}

void IntegerParameter::setValue(int32_t v)
{
	operator=(v);
}

