#pragma once
#include "parameter.h"
class IntegerParameter : public Parameter
{
public:
	virtual int32_t getMax() { return max; }
	virtual int32_t getMin() { return min; }
	virtual int32_t getValue() { return operator()(); };
	virtual void setValue(int32_t v);

	void operator=(int32_t v);
	int32_t operator()();
	void operator()(Parameter* parent, string name, ParameterFlags flags, int32_t* source, int32_t min, int32_t max, string units, int32_t defaultValue);
protected:

	int32_t* source;
	int32_t min;
	int32_t max;
	int32_t defaultValue;
	int32_t localValue;
};

