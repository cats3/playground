#include "Generator.h"

void Generator::operator()(Parameter* par, string name, GeneratorImplementation& implementation)
{
	Parameter::operator()(par, name);
	impl = &implementation;

	CyclesCompleted(this, "CyclesCompleted", ParameterFlags::ReadOnly, &impl->cyclesComplete, 0, INT_MAX, "cycles", 0);
}