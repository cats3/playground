// ZeroMQCpp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <string>
#include <random>
#include <iostream>
#include <chrono>

#include <zmq.hpp>

int main()
{
	using namespace std::literals;

	std::random_device rd;

	std::default_random_engine en(rd());
	std::normal_distribution<float> dist(-0.005, 0.005);

	zmq::context_t ctx{ 1 };
	zmq::message_t request;

	// open a REP (reply) socket and connect
	zmq::socket_t sock(ctx, zmq::socket_type::rep);
	sock.bind("tcp://*:5555");

	const int samples = 4000;
	const double freq = 8;
	int arr_index = 0;
	size_t bytes_sent;
	zmq::send_result_t send_result;

	float arr[samples];
	std::vector<float> txbuf(samples);
	for (auto j = 0; j < samples; j++)
	{
		arr[j] = 10.0f * static_cast<float>(sin((freq * static_cast<double>(j) / static_cast<double>(samples)) * std::_Pi * 2));// +dist(en));
	}
	std::chrono::time_point<std::chrono::steady_clock> t1 = std::chrono::steady_clock::now();
	for (;;)
	{
		try {
			sock.recv(request, zmq::recv_flags::none);
			std::chrono::time_point<std::chrono::steady_clock> t2 = std::chrono::steady_clock::now();
			int diff = (t2 - t1) / 250us;
			std::cout << "received " << request.to_string() << " after " << (t2 - t1) / 1ms << "ms." << std::endl;
			t1 = t2;

			bytes_sent = 0;
			txbuf.clear();
			if (diff > samples) diff = samples-1;
			if ((arr_index + diff) > samples)
			{
				bytes_sent = samples - arr_index;
				txbuf.insert(txbuf.end(), arr + arr_index, arr + samples);
				arr_index = 0;
				diff -= bytes_sent;
			}
			txbuf.insert(txbuf.end(), arr + arr_index, arr + arr_index + diff);
			send_result = sock.send(zmq::buffer(txbuf, diff * 4), zmq::send_flags::none);
			arr_index += diff;
			std::cout << "sent " << send_result.value() << " bytes" << std::endl;
		}
		catch (zmq::error_t t)
		{
			std::cout << "exception" << std::endl;
		}
	}
}

