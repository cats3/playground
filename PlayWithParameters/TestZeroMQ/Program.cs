﻿
using NetMQ;
using NetMQ.Sockets;

Console.WriteLine("Hello, World!");

using (var client = new RequestSocket())
{
    Console.WriteLine("IP Address?");
    string? ipaddr = Console.ReadLine();
    if (ipaddr == null)
        ipaddr = "localhost";
    client.Connect($"tcp://{ipaddr}:5555");

    for (var i = 0; i < 100; i++)
    {
        Thread.Sleep(20);
        client.SendFrame("GIMME");
        byte[] foo = client.ReceiveFrameBytes();
        for (var b = 0; b < foo.Count(); b += sizeof(float))
        {
            float bar = BitConverter.ToSingle(foo, b);
            //Console.WriteLine($"{count++},{bar}");
        }
    }
    Console.ReadKey();
}
