﻿// See https://aka.ms/new-console-template for more information
using NetMQ;
using NetMQ.Sockets;
using System.Runtime.InteropServices;

Console.WriteLine("C# ZeroMQ Server Test");

using (var server = new ResponseSocket())
{
    server.Bind("tcp://*:5555");
    int count = 0;
    for (var i = 0; i < 10; i++)
    {
        byte[] foo = server.ReceiveFrameBytes();
        for(var b = 0; b < foo.Count(); b+=sizeof(float))
        {
            float bar = BitConverter.ToSingle(foo, b);
            Console.WriteLine($"{count++},{bar}");
        }
        server.SendFrame("OK");
    }
    Console.ReadLine();
}
