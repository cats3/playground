﻿using System;
using Google.Protobuf;
using NetMQ;
using NetMQ.Sockets;

namespace ProtocolBuffers
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");

            using (var client = new RequestSocket())
            {
                client.Connect("tcp://localhost:5556");
                client.SendFrame("Hello");
                byte[] msg = client.ReceiveFrameBytes();
                Parameter p = new();
                p.MergeFrom(msg);
                Console.WriteLine(p.ToString());
    
                Console.ReadKey();
            }
        }
    }
}

