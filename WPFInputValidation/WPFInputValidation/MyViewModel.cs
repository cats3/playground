﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace WPFInputValidation
{
    public class MyViewModel : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        private double _myDoubleValue;

        private Dictionary<string, List<ValidationRule>> ValidationRules { get; }
        private Dictionary<string, List<string>> Errors { get; }

        public MyViewModel()
        {

            Errors = new Dictionary<string, List<string>>();
            ValidationRules = new Dictionary<string, List<ValidationRule>>();

            ValidationRules.Add(nameof(MyDoubleValue), new List<ValidationRule>() { new MyValidationRule { Min = -1, Max = 2 } } );
        }
        public double MyDoubleValue
        {
            get { return _myDoubleValue; }
            set
            {
                _myDoubleValue = value;
                OnPropertyChanged();
            }
        }

        private bool Validate<T>(T value, [CallerMemberName] string propertyName = "")
        {
            Errors.Remove(propertyName);
            OnErrorsChanged(propertyName);

            if (ValidationRules.TryGetValue(propertyName, out List<ValidationRule> rules))
            {
                rules.Select(validationRule => validationRule.Validate(value, CultureInfo.CurrentCulture))
                    .Where(result => !result.IsValid)
                    .ToList()
                    .ForEach(invalidResult => AddError(propertyName, invalidResult.ErrorContent as string));
                return !PropertyHasErrors(propertyName);
            }

            return true;
        }

        private bool PropertyHasErrors(string propertyName) => Errors.TryGetValue(propertyName, out List<string> propertyErrors) && propertyErrors.Any();

        private void AddError(string propertyName, string errorMessage)
        {
            if (!Errors.TryGetValue(propertyName, out List<string> errors))
            {
                errors = new List<string>();
                Errors[propertyName] = errors;
            }

            if (!errors.Contains(errorMessage))
            {
                errors.Add(errorMessage);
            }
            OnErrorsChanged(propertyName);
        }

        public bool HasErrors => Errors.Any();

        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        public event PropertyChangedEventHandler PropertyChanged;

        public IEnumerable GetErrors(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
                return new List<string>();
            return Errors.TryGetValue(propertyName, out List<string> errors) ? errors : new List<string>();
        }

        private void OnErrorsChanged([CallerMemberName] string propertyName = "")
        {
            ErrorsChanged?.Invoke(this, new DataErrorsChangedEventArgs(propertyName));
        }

        private void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
