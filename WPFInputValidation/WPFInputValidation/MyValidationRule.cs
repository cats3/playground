﻿using System.Globalization;
using System.Windows.Controls;

namespace WPFInputValidation
{
    public class MyValidationRule : ValidationRule
    {
        public double Min { get; set; }
        public double Max { get; set; }

        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            double v;
            if (!(value is double))
            {
                if (!double.TryParse((string)value, out v))
                {
                    return new ValidationResult(false, "Invalid input, enter a number");
                }
            }
            else
            {
                v = (double)value;
            }

            if (v < Min)
                return new ValidationResult(false, $"Value must be greater than {Min}");
            if (v > Max)
                return new ValidationResult(false, $"Value must be less than {Max}");

            return ValidationResult.ValidResult;                
        }
    }
}
