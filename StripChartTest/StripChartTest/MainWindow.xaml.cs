﻿using SciChart.Charting.Visuals;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace StripChartTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            // Set this code once in App.xaml.cs or application startup
            SciChartSurface.SetRuntimeLicenseKey("TGoN9l/UVcNQkzTzgjG05GekLw4Hoq0k5kc3LauLKd3yhfH0eOjTMSqwCYNQvv1wXYjzw8llWkSmQJtI/59F0jJC6Caasegu4whnVEszLYc1SYZj4Etklu4GgaRgEZB2dfM8gRHLEZwo/ntMLyD2c4agYbAB7AcAik+vhxkt2CpY0nWyamXPptT8sML6TXd2CDjMiG74RgU3/FZ+LHHlVszfXceoROcQQDWpe33EyoqRa+pWUboAGlnSqiuAgP0T5RNkGN076ACrIVOP3/E16Y4no6Nh3+A+gnX7PDELFz46TIUKTIt8CuFh/scybAVPmiEOLHXzEbX/agg8Y4T09fEn2nlYZF/DTOW4XEiQ96xOQz3QA3Zh23dtOc33R2UcDjNMF73tY/VoAaFQWByt6fbqkqGyacxdad4dqkLnBklvF9Y6Tx/EyxUjNqjRRX1dcKIsp7m3nkJeeI6PSuoBuXHVQxfbwdfMqoQPBPpB3qY=");
            InitializeComponent();
        }
    }
}
