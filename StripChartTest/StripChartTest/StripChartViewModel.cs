﻿using SciChart.Charting.Model.DataSeries;
using StripChartTest.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;
using System.Threading.Tasks;
using SciChart.Data.Model;
using System.Windows.Input;
using Microsoft.Xaml.Behaviors.Core;
using NetMQ.Sockets;
using NetMQ;

namespace StripChartTest
{
    public class StripChartViewModel : ObservableObject
    {
        private RequestSocket? _client;

        private Timer? _timer;
        private readonly object _timerLock = new object();
        private IUniformXyDataSeries<double> _dataSeries;

        private DoubleRange? _xVisibleRange;
        private DoubleRange? _yVisibleRange;
        private double _totalTime;

        public DoubleRange XVisibleRange { get => _xVisibleRange; set => SetField(ref _xVisibleRange, value); }
        public DoubleRange YVisibleRange { get => _yVisibleRange; set => SetField(ref _yVisibleRange, value); }

        private const double WindowSize = 5d;
        private const int SampleRate = 4000;
        private const double TimerInterval = 20;

        public StripChartViewModel()
        {
            _client = new RequestSocket();
            var fifoCapacity = WindowSize * SampleRate * 1.1;
            _dataSeries = new UniformXyDataSeries<double>(0d, 1d / SampleRate) { FifoCapacity = (int)fifoCapacity };

            XVisibleRange = new DoubleRange(0d, WindowSize);
            YVisibleRange = new DoubleRange(-12, 12);

            StartCommand = new ActionCommand(OnExampleEnter);
            StopCommand = new ActionCommand(OnExampleExit);
        }

        public ICommand StartCommand { get; }
        public ICommand StopCommand { get; }

        public IUniformXyDataSeries<double> StripChartDataSeries
        {
            get => _dataSeries;
            set => SetField(ref _dataSeries, value);
        }

        // These methods are just used to do tidy up when switching between examples
        public void OnExampleExit()
        {
            if (_timer != null)
            {
                _timer.Stop();
                _timer.Elapsed -= TimerElapsed;
                _timer.Dispose();
            }
            if (_client != null)
            {
                _client.Close();
                _client.Dispose();
            }
        }

        public void OnExampleEnter()
        {
            if (_client != null )_client.Connect($"tcp://localhost:5555");

            _timer = new Timer(TimerInterval) { AutoReset = true };
            _timer.Elapsed += TimerElapsed;
            _timer.Start();
        }
        private void TimerElapsed(object? sender, ElapsedEventArgs e)
        {
            lock (_timerLock)
            {
                if (_client != null)
                {
                    _client.SendFrame("GIMME");
                    byte[] foo = _client.ReceiveFrameBytes();
                    for (var b = 0; b < foo.Length; b += sizeof(float))
                    {
                        float bar = BitConverter.ToSingle(foo, b);
                        AppendPoint(bar);
                    }
                }
            }
        }

        private void AppendPoint(double point)
        {
            _dataSeries.Append(point);
            ComputeXAxisRange(_totalTime);
            _totalTime += 1d / SampleRate;
        }

        private void ComputeXAxisRange(double time)
        {
            if (time >= XVisibleRange.Max)
            {
                // Calculates a visible range. When the trace touches the right edge of the chart
                // (governed by WindowSize), shift the entire range 50% so that the trace is in the 
                // middle of the chart 
                double fractionSize = WindowSize * 0.05;
                double newMin = fractionSize * Math.Floor((time - fractionSize) / fractionSize);
                double newMax = newMin + WindowSize;

                XVisibleRange = new DoubleRange(newMin, newMax);
            }
        }
    }
}
