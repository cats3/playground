﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TestMemoryLeak
{
    class Program
    {
        static MyClass foo = new MyClass();
        static void Main(string[] args)
        {
            for (int i = 0; i < 50; i++)
            {
                Console.ReadKey();
                var xmltext = foo.GetXML();
                Console.WriteLine(xmltext);
            }
            Console.ReadKey();
        }

    }

    public class MyClass
    {
        private readonly Hashtable hashtable = new Hashtable();
        XmlSerializer xml = new XmlSerializer(typeof(MyClass));

        public MyClass()
        {
            hashtable.Add("intSerializer", xml);
        }

        public int foo = 123;
        public string GetXML()
        {
            XmlSerializer xml = new XmlSerializer(typeof(MyClass));
            hashtable["intSerializer"] = xml;
            using (var memstream = new MemoryStream())
            {
                xml.Serialize(memstream, this);
                memstream.Position = 0;
                //var sr = new StreamReader(memstream);
                //var returnString = sr.ReadToEnd();
                var returnString = new StreamReader(memstream).ReadToEnd();
                memstream.Dispose();
                return returnString;
            }
        }
    }
}
