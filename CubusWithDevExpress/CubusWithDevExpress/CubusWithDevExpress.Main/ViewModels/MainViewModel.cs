﻿using CubusWithDevExpress.Modules.ViewModels;
using DevExpress.Mvvm.POCO;
using System.Collections.ObjectModel;
using System.Linq;

namespace CubusWithDevExpress.Main.ViewModels
{
    public class MainViewModel
    {
        public static MainViewModel Create()
        {
            return ViewModelSource.Create(() => new MainViewModel());
        }

        public SequencesViewModel SequencesViewModel { get; set; }
        public ObservableCollection<RecentItem> RecentFiles { get; set; }
        public MainViewModel()
        {
            //SequencesViewModel = new SequencesViewModel();
            //((ISupportParentViewModel)SequencesViewModel).ParentViewModel = this;

            RecentFiles = new ObservableCollection<RecentItem>();
            Enumerable.Range(1, 4)
                .Select(x => new RecentItem() { Number = x, FileName = "CyclicPro Test " + x.ToString() })
                .ToList()
                .ForEach(x => RecentFiles.Add(x));
        }
    }

    public class RecentItem {
        public string FileName { get; set; }
        public int Number { get; set; }
    }

}
