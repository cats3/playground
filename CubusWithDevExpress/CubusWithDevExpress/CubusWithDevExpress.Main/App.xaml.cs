﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.ModuleInjection;
using DevExpress.Mvvm.UI;
using DevExpress.Xpf.Core;
using CubusWithDevExpress.Common;
using CubusWithDevExpress.Main.Properties;
using CubusWithDevExpress.Main.ViewModels;
using CubusWithDevExpress.Main.Views;
using CubusWithDevExpress.Modules.Models;
using CubusWithDevExpress.Modules.Stores;
using CubusWithDevExpress.Modules.ViewModels;
using CubusWithDevExpress.Modules.Views;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows;
using AppModules = CubusWithDevExpress.Common.Modules;
using CubusWithDevExpress.Modules.Services;

//using Microsoft.Extensions.DependencyInjection;
//using Microsoft.Extensions.Hosting;
using DevExpress.Entity.Model;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace CubusWithDevExpress.Main
{
    public partial class App : Application
    {
        protected SplashScreenManager splashManager;

        public App()
        {
            ApplicationThemeHelper.UpdateApplicationThemeName();
            splashManager = SplashScreenManager.CreateFluent();
            splashManager.ViewModel.Title = "Cubus 3";
            splashManager.ViewModel.Subtitle = "Demonstration v0.0.1";
            splashManager.ViewModel.Copyright = "Copyright (c) 2022 - CATS3 Limited";
            splashManager.ViewModel.Logo = new System.Uri("/Images/logo.png", System.UriKind.Relative);
            splashManager.ShowOnStartup();
        }
        protected override void OnExit(ExitEventArgs e)
        {
            ApplicationThemeHelper.SaveApplicationThemeName();
            base.OnExit(e);
        }
        void OnApplicationStartup(object sender, StartupEventArgs e)
        {
            Bootstrapper.Run();
        }
    }
    public class Bootstrapper
    {
        private readonly IHost _host;

        public static Bootstrapper Default { get; protected set; }
        public static void Run()
        {
            Default = new Bootstrapper();
            Default.RunCore();
        }
        protected Bootstrapper() {
            _host = Host.CreateDefaultBuilder()
                .ConfigureServices((context, services) =>
                {
                    services.AddSingleton<ITestSequenceService, TestSequenceStore>();
                }).Build();
        }

        const string StateVersion = "1.0";
        public virtual void RunCore()
        {
            ConfigureTypeLocators();
            RegisterModules();
            ConfigureStores();
            if (!RestoreState())
                InjectModules();
            ConfigureNavigation();
            ShowMainWindow();
        }

        private void ConfigureStores()
        {
            ITestSequenceService testSequenceService = _host.Services.GetService<ITestSequenceService>();
            testSequenceService.AddItem(new RampItem());
            testSequenceService.AddItem(new DwellItem() { DwellSeconds = 2.34m });
        }

        protected IModuleManager Manager { get { return ModuleManager.DefaultManager; } }

        protected virtual void ConfigureTypeLocators()
        {
            var mainAssembly = typeof(MainViewModel).Assembly;
            var modulesAssembly = typeof(ModuleViewModel).Assembly;
            var assemblies = new[] { mainAssembly, modulesAssembly};
            ViewModelLocator.Default = new ViewModelLocator(assemblies);
            ViewLocator.Default = new ViewLocator(assemblies);
        }
        protected virtual void RegisterModules()
        {
            Manager.GetRegion(Regions.Documents).VisualSerializationMode = VisualSerializationMode.PerKey;
            Manager.Register(Regions.MainWindow, new Module(AppModules.Main, MainViewModel.Create, typeof(MainView)));
            Manager.Register(Regions.Navigation, new Module(AppModules.Module1, () => new NavigationItem("Module1")));
            Manager.Register(Regions.Navigation, new Module(AppModules.Module2, () => new NavigationItem("Module2")));

            Manager.Register(Regions.Navigation, new Module(AppModules.DigitalDisplays, () => new NavigationItem("Digital Displays")));
            Manager.Register(Regions.Navigation, new Module(AppModules.DigitalDisplays2, () => new NavigationItem("Digital Displays")));

            Manager.Register(Regions.Documents, new Module(AppModules.Module1, () => ModuleViewModel.Create("Module1"), typeof(ModuleView)));
            Manager.Register(Regions.Documents, new Module(AppModules.Module2, () => ModuleViewModel.Create("Module2"), typeof(ModuleView)));

            Manager.Register(Regions.Documents, new Module(AppModules.DigitalDisplays, () => DigitalDisplaysViewModel.Create("Digital Display"), typeof(DigitalDisplaysView)));
            Manager.Register(Regions.Documents, new Module(AppModules.DigitalDisplays2, () => DigitalDisplaysViewModel.Create("Digital Display"), typeof(DigitalDisplaysView)));

            //Manager.Register(Regions.Editor, new Module(AppModules.TestSequenceModule, () => SequencesViewModel.Create(_host.Services.GetService<ITestSequenceService>()), typeof(SequencesView)));

        }
        protected virtual bool RestoreState()
        {
#if !DEBUG
            if (Settings.Default.StateVersion != StateVersion) return false;
            return Manager.Restore(Settings.Default.LogicalState, Settings.Default.VisualState);
#else
            return false;
#endif
        }
        protected virtual void InjectModules()
        {
            Manager.Inject(Regions.MainWindow, AppModules.Main);
            Manager.Inject(Regions.Navigation, AppModules.Module1);
            Manager.Inject(Regions.Navigation, AppModules.Module2);
            Manager.Inject(Regions.Navigation, AppModules.DigitalDisplays);
            Manager.Inject(Regions.Navigation, AppModules.DigitalDisplays2);
            //Manager.Inject(Regions.Editor, AppModules.TestSequenceModule);
        }
        protected virtual void ConfigureNavigation()
        {
            Manager.GetEvents(Regions.Navigation).Navigation += OnNavigation;
            Manager.GetEvents(Regions.Documents).Navigation += OnDocumentsNavigation;
        }
        protected virtual void ShowMainWindow()
        {
            App.Current.MainWindow = new MainWindow();
            App.Current.MainWindow.Show();
            App.Current.MainWindow.Closing += OnClosing;
        }
        void OnNavigation(object sender, NavigationEventArgs e)
        {
            if (e.NewViewModelKey == null) return;
            Manager.InjectOrNavigate(Regions.Documents, e.NewViewModelKey);
        }
        void OnDocumentsNavigation(object sender, NavigationEventArgs e)
        {
            Manager.Navigate(Regions.Navigation, e.NewViewModelKey);
        }
        void OnClosing(object sender, CancelEventArgs e)
        {
            string logicalState;
            string visualState;
            Manager.Save(out logicalState, out visualState);
            Settings.Default.StateVersion = StateVersion;
            Settings.Default.LogicalState = logicalState;
            Settings.Default.VisualState = visualState;
            Settings.Default.Save();
        }
    }
}