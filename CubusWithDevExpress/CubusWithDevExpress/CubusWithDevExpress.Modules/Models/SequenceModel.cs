﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CubusWithDevExpress.Modules.Models
{
    public class Sequence : SequenceItemBase
    {
        public int Repeats { get; set; }
        public ObservableCollection<ISequenceItem> Items { get; set; }

        public Sequence(string name, int repeats, IEnumerable<ISequenceItem> items) : base(name)
        {
            Repeats = repeats;
            Items = new ObservableCollection<ISequenceItem>(items);
        }
    }
    public interface ISequenceItem
    {
        string Name { get; set; }
        Guid Id { get; set; }
    }
    public class SequenceItemBase : ISequenceItem
    {
        public string Name { get; set; }
        public Guid Id { get; set; }

        public SequenceItemBase(string name)
        {
            Name = name;
            Id = Guid.NewGuid();
        }
    }

    public class RampItem : SequenceItemBase
    {
        public RampItem() : base("Ramp")
        {
        }
    }

    public class DwellItem : SequenceItemBase
    {
        public decimal DwellSeconds { get; set; } = 1.0M;
        public DwellItem() : base("Dwell")
        {
        }
    }

    public class CyclicItem : SequenceItemBase
    {
        public int Cycles { get; set; } = 10;
        public CyclicItem() : base("Cyclic")
        {
        }
    }
}
