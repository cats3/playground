﻿using CubusWithDevExpress.Modules.Models;
using CubusWithDevExpress.Modules.Services;
using System;
using System.Windows.Input;

namespace CubusWithDevExpress.Modules.Commands
{
    public class AddDwellCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;

        private ITestSequenceService _service;

        public AddDwellCommand(ITestSequenceService service)
        {
            _service = service;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _service?.AddItem(new DwellItem());
        }
    }
}
