﻿using CubusWithDevExpress.Modules.Models;
using DevExpress.Mvvm;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace CubusWithDevExpress.Modules.ViewModels
{
    public class SequenceItemViewModel : ViewModelBase
    {
        private readonly ISequenceItem _item;

        public SequenceItemViewModel(ISequenceItem item)
        {
            _item = item;
            Name = item.Name;
        }

        public string Name { get; set; }

        public ViewModelBase SelectedViewModel { get; set; }
    }
}
