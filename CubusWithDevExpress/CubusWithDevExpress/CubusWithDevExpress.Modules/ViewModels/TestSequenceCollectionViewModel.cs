﻿using CubusWithDevExpress.Modules.Models;
using CubusWithDevExpress.Modules.Services;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using System.Collections.ObjectModel;

namespace CubusWithDevExpress.Modules.ViewModels
{
    public class TestSequenceCollectionViewModel
    {
        private ITestSequenceService _service;
        public ObservableCollection<ISequenceItem> Sequences { get; set; }

        public TestSequenceCollectionViewModel()
        {
            _service = this.GetRequiredService<ITestSequenceService>();

            _service.AddItem(new DwellItem() { DwellSeconds = 1.2345m });
            _service.AddItem(new RampItem() { });
            Sequences = new ObservableCollection<ISequenceItem>(_service.MainSequence);
        }

        public void AddRamp()
        {
            _service.AddItem(new RampItem());
        }
        public void AddDwell()
        {
            _service.AddItem(new DwellItem());
        }
        public void AddCyclic()
        {
            _service.AddItem(new CyclicItem());
        }

        public void ShowDetail(ISequenceItem item)
        {
            if (item == null)
                return;

            IDocumentManagerService service = this.GetRequiredService<IDocumentManagerService>();
            IDocument document = service.FindDocument(parameter: item, parentViewModel: this);

            if (document == null)
            {
                document = service.CreateDocument(documentType: "SequenceItemDetailView", parameter: item, parentViewModel: this);
                document.Title = item.Name;
            }
            document.Show();
        }
    }
}
