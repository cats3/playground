﻿using DevExpress.Mvvm.POCO;
using System.Collections.ObjectModel;
using CubusWithDevExpress.Modules.Models;
using CubusWithDevExpress.Modules.Services;
using System.Windows.Input;
using CubusWithDevExpress.Modules.Commands;
using System.Linq;

namespace CubusWithDevExpress.Modules.ViewModels
{
    public class SequencesViewModel
    {
        private readonly ITestSequenceService _service;

        public ObservableCollection<ISequenceItem> Sequences { get; private set; }

        public ICommand AddRampCommand { get; private set; }
        public ICommand AddDwellCommand { get; private set; }
        public virtual ISequenceItem SelectedItem { get; set; }

        public static SequencesViewModel Create(ITestSequenceService service)
        {
            return ViewModelSource.Create(() => new SequencesViewModel(service));
        }
        protected SequencesViewModel() { }
        protected SequencesViewModel(ITestSequenceService service)
        {
            _service = service;
            AddRampCommand = new AddRampCommand(_service);
            AddDwellCommand = new AddDwellCommand(_service);
            _service.SequenceUpdated += Service_SequenceUpdated;
            UpdateSequences();
        }
        private void UpdateSequences()
        {
            Sequences = new ObservableCollection<ISequenceItem>(_service?.MainSequence);
            SelectedItem = Sequences.First();
        }
        private void Service_SequenceUpdated()
        {
            UpdateSequences();
        }
    }
}
