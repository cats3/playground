﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using CubusWithDevExpress.Common;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace CubusWithDevExpress.Modules.ViewModels
{
    public class DigitalDisplaysViewModel : IDocumentModule
    {
        public string Caption {get; private set; }

        public virtual bool IsActive { get; set; }

        public ObservableCollection<DisplayDataItem> Items { get; private set; }

        public static DigitalDisplaysViewModel Create(string caption)
        {
            return ViewModelSource.Create(() => new DigitalDisplaysViewModel()
            {
                Caption = caption,
            });
        }

        public DigitalDisplaysViewModel()
        {
            Items = new ObservableCollection<DisplayDataItem>();
            var rand = new Random();
            Enumerable.Range(0, rand.Next(3,8))
                .Select(x => new DisplayDataItem() { Reading =  (-0.5d + rand.NextDouble()) * 1000d, Name = "Transducer " + x.ToString() })
                .ToList()
                .ForEach(x => Items.Add(x));
        }
    }
    public class DisplayDataItem
    {
        public double Reading { get; set; }
        public string Name { get; set; }
    }
}
