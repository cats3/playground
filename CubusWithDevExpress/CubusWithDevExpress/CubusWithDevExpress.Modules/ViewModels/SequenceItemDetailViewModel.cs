﻿using CubusWithDevExpress.Modules.Models;
using DevExpress.Mvvm;

namespace CubusWithDevExpress.Modules.ViewModels
{
    public class SequenceItemDetailViewModel : ViewModelBase
    {
        public string Name
        {
            get { return GetValue<string>(); }
            set { SetValue(value); }
        }
        public SequenceItemDetailViewModel()
        {
            Name = "Foobar";
        }

        public ViewModelBase SelectedViewModel
        {
            get { return GetValue<ViewModelBase>(); }
            set
            {
                SetValue(value);
            }
        }

        public ISequenceItem SelectedItem
        {
            get { return GetValue<ISequenceItem>(); }
            set { SetValue(value); }
        }

        protected override void OnParameterChanged(object parameter)
        {
            base.OnParameterChanged(parameter);
            if (parameter is ISequenceItem item)
            {
                Name = item.Name;
                SelectedItem = item;
                if (item is DwellItem)
                    SelectedViewModel = new DwellItemViewModel();
                else if (item is RampItem)
                    SelectedViewModel = new RampItemViewModel();
                else
                    SelectedViewModel = null;
            }
        }

    }
}
