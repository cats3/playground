﻿using CubusWithDevExpress.Modules.Models;
using CubusWithDevExpress.Modules.Services;
using DevExpress.Mvvm;
using DevExpress.Mvvm.POCO;
using System;

namespace CubusWithDevExpress.Modules.ViewModels
{
    public class DwellItemViewModel : ViewModelBase
    {
        private readonly ITestSequenceService _service;
        DwellItem _dwellItem;
        public DwellItemViewModel()
        {
            ITestSequenceService service = this.GetRequiredService<ITestSequenceService>();
            _service = service;
        }

        public decimal DwellSeconds
        {
            get { return GetValue<decimal>(); }
            set
            {
                if (SetValue(value))
                {
                    _dwellItem.DwellSeconds = value;
                    _service?.UpdateItem(_dwellItem);
                }
            }
        }

        protected override void OnParameterChanged(object parameter)
        {
            base.OnParameterChanged(parameter);
            if (parameter is DwellItem dwell)
            {
                _dwellItem = dwell;
                DwellSeconds = dwell.DwellSeconds;
            }
        }
    }
}
