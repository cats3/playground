﻿using DevExpress.Xpf.Grid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DevExpress.Mvvm.UI.Interactivity;
using DevExpress.Xpf.Core;
using CubusWithDevExpress.Modules.Views;
using System.Windows;

namespace CubusWithDevExpress.Modules.Behaviors
{
    public class TestSequenceBehavior : Behavior<TreeViewControl>
    {
        public SequenceItemDetailView DetailView
        {
            get { return (SequenceItemDetailView)GetValue(DetailViewProperty); }
            set { SetValue(DetailViewProperty, value); }
        }

        // Using a DependencyProperty as the backing store for DetailView.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty DetailViewProperty =
            DependencyProperty.Register("DetailView", typeof(SequenceItemDetailView), typeof(TestSequenceBehavior), new PropertyMetadata(null));

    }
}
