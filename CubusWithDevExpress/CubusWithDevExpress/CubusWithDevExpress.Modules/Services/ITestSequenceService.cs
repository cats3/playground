﻿using CubusWithDevExpress.Modules.Models;
using System;
using System.Collections.Generic;

namespace CubusWithDevExpress.Modules.Services
{
    public interface ITestSequenceService
    {
        IEnumerable<Sequence> MainSequence { get; }
        public event Action SequenceUpdated;
        void AddItem(ISequenceItem item);
        void UpdateItem(ISequenceItem item);
        void DeleteItem(ISequenceItem item);
    }
}
