﻿using CubusWithDevExpress.Modules.Models;
using DevExpress.Data.Extensions;
using DevExpress.Mvvm.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CubusWithDevExpress.Modules.Services
{
    public class TestSequenceService : ServiceBase, ITestSequenceService
    {
        private static TestSequenceService instance;
        private List<Sequence> _sequences;
        private Sequence selectedSequence;
        public IEnumerable<Sequence> MainSequence => _sequences;

        public event Action SequenceUpdated;
        public TestSequenceService()
        {
            Debug.WriteLine("*");
            if (instance == null)
            {
                instance = this;
                _sequences = new List<Sequence>() { new Sequence("Main Sequence", 1, Array.Empty<ISequenceItem>()) };
                selectedSequence = _sequences.First();
            }
        }
        public void AddItem(ISequenceItem item)
        {
            var sameItems = instance.selectedSequence.Items.Where(x => x.GetType() == item.GetType()).ToList();
            var newItemIndex = sameItems.Count + 1;
            item.Name = $"{item.Name} {newItemIndex}";
            instance.selectedSequence.Items.Add(item);
            instance.SequenceUpdated?.Invoke();
        }
        public void UpdateItem(ISequenceItem item)
        {
            int currentIndex = instance.selectedSequence.Items.FindIndex(x => x.Id.Equals(item.Id));
            if (currentIndex == -1)
                instance.selectedSequence.Items.Add(item);
            else
                instance.selectedSequence.Items[currentIndex] = item;

            instance.SequenceUpdated?.Invoke();
        }
        public void DeleteItem(ISequenceItem item)
        {
            throw new NotImplementedException();
        }

    }
}
