﻿using System.Windows.Controls;

namespace CubusWithDevExpress.Modules.Views
{
    /// <summary>
    /// Interaction logic for SequenceItemDetailView.xaml
    /// </summary>
    public partial class SequenceItemDetailView : UserControl
    {
        public SequenceItemDetailView()
        {
            InitializeComponent();
        }
    }
}
