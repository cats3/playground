﻿using CubusWithDevExpress.Modules.Models;
using CubusWithDevExpress.Modules.Services;
using DevExpress.Data.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace CubusWithDevExpress.Modules.Stores
{
    public class TestSequenceStore : ITestSequenceService
    {
        private List<Sequence> _sequences;
        private Sequence selectedSequence;
        public IEnumerable<Sequence> MainSequence => _sequences;

        public event Action SequenceUpdated;
        public TestSequenceStore()
        {
            _sequences = new List<Sequence>() { new Sequence("Main Sequence", 1, Array.Empty<ISequenceItem>()) };
            selectedSequence = _sequences.First();
        }
        public void AddItem(ISequenceItem item)
        {
            var sameItems = selectedSequence.Items.Where(x => x.GetType() == item.GetType()).ToList();
            var newItemIndex = sameItems.Count + 1;
            item.Name = $"{item.Name} {newItemIndex}";
            selectedSequence.Items.Add(item);
            SequenceUpdated?.Invoke();
        }
        public void UpdateItem(ISequenceItem item)
        {
            int currentIndex = selectedSequence.Items.FindIndex(x => x.Id.Equals(item.Id));
            if (currentIndex == -1)
                selectedSequence.Items.Add(item);
            else
                selectedSequence.Items[currentIndex] = item;

            SequenceUpdated?.Invoke();
        }

        public void DeleteItem(ISequenceItem item)
        {
            throw new NotImplementedException();
        }
    }


}
