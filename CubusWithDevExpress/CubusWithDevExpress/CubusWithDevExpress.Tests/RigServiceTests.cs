﻿using CubusWithDevExpress.TestElements.Models;
using CubusWithDevExpress.TestElements.Services;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubusWithDevExpress.Tests
{
    public class RigServiceTests
    {
        public IRigService service;
        [Test]
        public async Task RigServiceCreation()
        {
            service = new RigService();
            Assert.IsNotNull(service);
            Assert.That(service.Channels.Count(), Is.EqualTo(0));
            Assert.That(service.Transducers.Count(), Is.EqualTo(0));

            var loadtx = new Transducer("load", "kN");
            var disptx = new Transducer("displacement", "mm");
            await service.AddTransducer(loadtx);
            Assert.That(service.Transducers, Has.Exactly(1).Items);
            await service.AddTransducer(disptx);
            Assert.That(service.Transducers, Has.Exactly(2).Items);

            var channel = new ControlChannel();
            Assert.That(channel.Name, Is.EqualTo("Channel"));
            channel.AddFeedbackTransducer(disptx);
            Assert.That(channel.FeedbackTransducers, Has.Exactly(1).Items);
            channel.AddFeedbackTransducer(disptx);
            Assert.That(channel.FeedbackTransducers, Has.Exactly(1).Items);

            await service.AddChannel(channel);
            Assert.That(service.Channels, Has.Exactly(1).Items);
        }
    }
}
