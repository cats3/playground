﻿using CubusWithDevExpress.TestElements.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubusWithDevExpress.Tests
{
    public class ElementTests
    {
        private readonly double tolerance = 0.001d;
        public ITestElement element;
        [SetUp]
        public void SetUp()
        {

        }
        [Test]
        public void DwellElementCreation()
        {
            element = new DwellElement();
            var e = element as DwellElement;
            Assert.That(e.Duration, Is.EqualTo(1.0d).Within(tolerance));
        }

        [Test]
        public void RampElementCreation()
        {
            element = new RampElement();
            var e = element as RampElement;
            Assert.That(e.IsConstantRate, Is.False);
            Assert.That(e.Shape, Is.EqualTo(RampShape.Linear));
            Assert.That(e.Duration, Is.EqualTo(1.0d).Within(tolerance));

            using (StringWriter s = new StringWriter())
            {
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(RampElement));
                x.Serialize(s, element);
                var f = s.ToString();
            }
        }
    }
}
