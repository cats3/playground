﻿namespace CubusWithDevExpress.Common
{
    public static class Modules
    {
        public static string Main { get { return "Main"; } }
        public static string Module1 { get { return "Module1"; } }
        public static string Module2 { get { return "Module2"; } }

        public static string TestSequenceModule { get { return "TestSequenceModule"; } }

        public static string DigitalDisplays { get { return "DigitalDisplays"; } }
        public static string DigitalDisplays2 { get { return "DigitalDisplays2"; } }
    }
}
