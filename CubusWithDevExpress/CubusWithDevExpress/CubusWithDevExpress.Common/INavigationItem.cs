﻿namespace CubusWithDevExpress.Common
{
    public interface INavigationItem
    {
        string Caption { get; }
    }
}
