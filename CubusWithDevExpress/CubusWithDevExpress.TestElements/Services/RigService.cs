﻿using CubusWithDevExpress.TestElements.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubusWithDevExpress.TestElements.Services
{
    public class RigService : IRigService
    {
        List<IControlChannel> _channels;
        List<ITransducer> _transducers;

        public RigService()
        {
            _channels = new List<IControlChannel>();
            _transducers = new List<ITransducer>();
        }

        public IEnumerable<ITransducer> Transducers => _transducers;

        public IEnumerable<IControlChannel> Channels => _channels;

        public async Task AddChannel(IControlChannel channel)
        {
            await Task.Run(() =>
            {
                if (!_channels.Contains(channel))
                    _channels.Add(channel);
            });
        }

        public async Task AddTransducer(ITransducer transducer)
        {
            await Task.Run(() => _transducers.Add(transducer));
        }
    }
}
