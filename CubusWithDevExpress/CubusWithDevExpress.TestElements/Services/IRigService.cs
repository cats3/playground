﻿using CubusWithDevExpress.TestElements.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubusWithDevExpress.TestElements.Services
{
    public interface IRigService
    {
        Task AddTransducer(ITransducer transducer);
        Task AddChannel(IControlChannel channel);
        IEnumerable<ITransducer> Transducers { get; }
        IEnumerable<IControlChannel> Channels { get; }

    }
}
