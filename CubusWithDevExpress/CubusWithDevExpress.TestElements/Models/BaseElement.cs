﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubusWithDevExpress.TestElements.Models
{
    public abstract class BaseElement : ITestElement
    {
        public string Name { get; set; }
        public Guid Id { get; set; }
        public abstract IEnumerable<IElementChannel> Channels { get; }

        public BaseElement(string name)
        {
            Name = name;
            Id = Guid.NewGuid();
        }

    }
}
