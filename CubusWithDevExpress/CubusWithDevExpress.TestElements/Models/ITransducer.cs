﻿namespace CubusWithDevExpress.TestElements.Models
{
    public interface ITransducer
    {
        string Name { get; }
        Guid Id { get; }
        string Description { get; }
        string Unit { get; }
    }
}