﻿namespace CubusWithDevExpress.TestElements.Models
{
    public class DwellElement : BaseElement
    {
        public double Duration { get; set; }

        public override IEnumerable<IElementChannel> Channels => throw new NotImplementedException();

        public DwellElement() : base("Dwell")
        {
            Duration = 1.0d;
        }
    }
}
