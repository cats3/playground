﻿namespace CubusWithDevExpress.TestElements.Models
{
    public class ControlChannelBase : IControlChannel
    {
        protected List<ITransducer> _feedbackTransducers;

        public Guid Id { get; }
        public string Name { get; set; }
        public string Description { get; set; }
        public IEnumerable<ITransducer> FeedbackTransducers => _feedbackTransducers;

        public ControlChannelBase(string name)
        {
            Name = name;
            Description = String.Empty;
            _feedbackTransducers = new List<ITransducer>();
        }

        public void AddFeedbackTransducer(ITransducer feedbackTransducer)
        {
            if (!_feedbackTransducers.Contains(feedbackTransducer))
                _feedbackTransducers.Add(feedbackTransducer);
        }

        public void RemoveFeedbackTransducer(ITransducer transducer)
        {
            _feedbackTransducers.RemoveAll(e => e.Id == transducer.Id);
        }
    }
}