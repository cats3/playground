﻿namespace CubusWithDevExpress.TestElements.Models
{
    public interface IControlChannel
    {
        Guid Id { get; }
        string Name { get; set; }
        string Description { get; set; }
        IEnumerable<ITransducer> FeedbackTransducers { get; }
        void AddFeedbackTransducer(ITransducer feedbackTransducer);
        void RemoveFeedbackTransducer(ITransducer transducer);
    }
}
