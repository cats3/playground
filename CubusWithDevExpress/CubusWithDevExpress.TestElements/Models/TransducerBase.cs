﻿namespace CubusWithDevExpress.TestElements.Models
{
    public abstract class TransducerBase : ITransducer
    {
        public string Name { get; set; }
        public Guid Id { get; }
        public string Description { get; set; }
        public abstract string Unit { get; set; }
        protected TransducerBase(string name)
        {
            Name = name;
            Id = Guid.NewGuid();
            Description = String.Empty;
        }
    }
}