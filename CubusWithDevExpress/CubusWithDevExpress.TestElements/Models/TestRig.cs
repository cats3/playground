﻿namespace CubusWithDevExpress.TestElements.Models
{
    public class TestRig
    {
        public IEnumerable<IControlChannel> Channels { get; set; }
        public IEnumerable<ITransducer> Transducers { get; set; }
    }
}
