﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubusWithDevExpress.TestElements.Models
{
    public interface ITestElement
    {
        string Name { get; set; }
        Guid Id { get; set; }
        abstract IEnumerable<IElementChannel> Channels { get; }
    }
}
