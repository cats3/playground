﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubusWithDevExpress.TestElements.Models
{
    public class Transducer : TransducerBase
    {
        public Transducer(string name, string unit) : base(name)
        {
            Unit = unit;
        }

        public override string Unit { get; set; }
    }
}
