﻿namespace CubusWithDevExpress.TestElements.Models
{
    public class RampElement : BaseElement
    {
        public List<RampElementChannel> _channels;

        public double Duration { get; set; }
        public RampShape Shape { get; set; }
        public bool IsConstantRate { get; set; }

        public override IEnumerable<RampElementChannel> Channels => _channels;

        public RampElement() : base("Ramp")
        {
            Shape = RampShape.Linear;
            IsConstantRate = false;
            Duration = 1.0d;

            _channels=new List<RampElementChannel>();
            _channels.Add(new RampElementChannel() { Target = 3.0d });
        }

    }
}
