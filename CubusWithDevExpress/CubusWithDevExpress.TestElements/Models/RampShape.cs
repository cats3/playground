﻿namespace CubusWithDevExpress.TestElements.Models
{
    public enum RampShape
    {
        Linear, Haversine, CustomLinear, CustomSpline, Square,
    }
}