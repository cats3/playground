﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CubusWithDevExpress.TestElements.Models
{
    public class RampElementChannel : IElementChannel
    {
        // public IControlMode ControlMode { get => throw new NotImplementedException(); set => throw new NotImplementedException(); }

        public Guid Id { get; }

        public double Target { get; set; }

        public RampElementChannel()
        {
            Id = Guid.NewGuid();
        }
    }
}
