﻿namespace CubusWithDevExpress.TestElements.Models
{
    public interface IControlMode
    {
        string Name { get; }
        string Units { get; }
    }
}
