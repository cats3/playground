using System.Windows;
using SciChart.Charting.Visuals;
using SciChart.Examples.ExternalDependencies.Controls.ExceptionView;

namespace SciChartExport
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App()
        {
		     DispatcherUnhandledException += App_DispatcherUnhandledException;

			 InitializeComponent();

            // TODO: Put your SciChart License Key here if needed
            // Set this code once in App.xaml.cs or application startup
            SciChartSurface.SetRuntimeLicenseKey("MF/RzFoiKikfdzaNSX2pAvXo3gtL9HcHZfGKm/mDo/L8Q3ZNBlOcNih/BiMdxGFn5ZPhJRMmKm8hjGTXCBfwgXyPM20Mpz0DLpjbvWeCFIVgd1RCM6ClB25HS5THn0kfAGg0OBc5XWeS2BzKvlG20bdsRLx/oXXI5P0DWC1TXTbRPMN9tOaiMhn6hZIcQpb/G54zj+f+BZnALHbFSrPZgRnd1nD+9ejJkuCbpgwnAG44aWmdyklFl+RQXSbUTK6uZ4gkVbov8kRNX7HdCi2pU9wgjUgunvrqIT2iNf5HEPdrKEwbh3aaPeWLiAfd1UlXRL2H1cACQTrDjqGNKnOZXNXQ+yW9v99dwqTalBZ/pfROqZL+hTPVTY3QCD5Jg7KDQS5q0GerV+l2lvHfTcL+a9YRkq7QgYyNoIYzQGl0xpcUoh598TTpfsG3VVQLFrHLgb2r0kEG1Zb6r9g0OWYs/iIbzgao0a/wfgBSgSmsHWE=");
            // SciChartSurface.SetRuntimeLicenseKey(@"{YOUR SCICHART WPF v6 LICENSE KEY}");
        }

		private void App_DispatcherUnhandledException(object sender,
            System.Windows.Threading.DispatcherUnhandledExceptionEventArgs e)
        {            
            var exceptionView = new ExceptionView(e.Exception)
            {
                WindowStartupLocation = WindowStartupLocation.CenterScreen,
            };
            exceptionView.ShowDialog();

            e.Handled = true;
        }
    }
}
