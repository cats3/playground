# BDDfy Tests

## Introduction

In a simialr nature to integration tests, BDDfy tests the overall end-to-end behaviour of a mocked service. This allows us to write test cases that mock the routes through the solution (i.e. expected behaviour) for various test cases scenario.

[Link to Docs to be found here.](https://teststackbddfy.readthedocs.io/en/latest/)

## Basic Example (from Docs)

BDDfy tests allow for a Given => When => Then structure to tests, which translate roughly to the following meanings:

### Given:
The steps that should be assumed before the tests.
### When:
The characteristic of the type of behaviour we wish to test. I.e. "When X, Then Y."
### Then:
The expected response for the sceanario.
### And:
Additional Given, When, Then steps which sapply in each test case.


## Example in plain English

``` 
Given the account balance is $100
  And the card is valid
  And the machine contains enough money
When the Account Holder requests $20
Then the ATM should dispense $20
  And the account balance should be $80
 And the card should be returned 
 ```

This example mimics the logic of the test, and is only to allow for useful thought about the nature of the bahviour we are trying to test. This being said, there should be a clear concise naming convention, which can be seen below in the example code.

## Example in code

A typical test should look like the following:

``` 
[TestMethod]
public void AccountHasSufficientfund()
{
    new AccountHasSufficientFund()
        .Given(s => s.GivenTheAccountBalanceIs(100), "Given the account balance is $100")
            .And(s => s.AndTheCardIsValid())
            .And(s => s.AndTheMachineContainsEnoughMoney())
        .When(s => s.WhenTheAccountHolderRequests(20),
    "When the account holder requests $20")
        .Then(s => s.ThenTheAtmShouldDispense(20), "Then the ATM should dispense $20")
            .And(s => s.AndTheAccountBalanceShouldBe(80),
        "And the account balance should be $80")
            .And(s => s.AndTheCardShouldBeReturned())
        .BDDfy();
}
```

## Infrastructure

A typical test requires a test class containing the methods which are used in the above `[TestMethod]` scenario.
```
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace BDDfy.Samples.Atm
{
    [TestClass]
    public class AccountHasSufficientFund
    {
        private Card _card;
        private Atm _atm;

        public void GivenTheAccountBalanceIs(int balance)
        {
            _card = new Card(true, balance);
        }

        public void AndTheCardIsValid()
        {
        }

        public void AndTheMachineContainsEnoughMoney()
        {
            _atm = new Atm(100);
        }

        public void WhenTheAccountHolderRequests(int moneyRequest)
        {
            _atm.RequestMoney(_card, moneyRequest);
        }

        public void ThenTheAtmShouldDispense(int dispensedMoney)
        {
            Assert.AreEqual(dispensedMoney, _atm.DispenseValue);
        }

        public void AndTheAccountBalanceShouldBe(int balance)
        {
            Assert.AreEqual(balance, _card.AccountBalance);
        }

        public void AndTheCardShouldBeReturned()
        {
            Assert.IsFalse(_atm.CardIsRetained);
        }
    }
} 
```

## Method naming conventions 

BDDfy uses reflection to scan your classes for steps. The following is the list of method name conventions we should follow for consistency and readability:

- Method names ending with Context is considered a setup method but doesn't get shown in the reports
- Method name equaling Setup is a setup method but doesn't get shown in in the reports (By default, BDDfy also generates an HTML report called 'BDDfy.Html' in your project's output folder)
- Method names starting with Given is a setup step that gets shown in the reports
- Method names starting with AndGiven are considered setup steps running after 'Given' steps which is reported.
- Method name starting with When is considered a state transition step and is reported
- Method names starting with AndWhen and are considered state transition steps running after 'When' steps and is reported
- Method names starting with Then is an asserting step and is reported
- Method names starting with And and AndThen are considered an asserting steps running after 'Then' step and is reported
- Method name starting with TearDown is considered as tear down method that is always run at the end but doesn't get shown in the reports.


## Mocking

Similar to regular unit tests, BDDfy mocks uneccessary POCOs and Services in a very similar way. Essentially the assertions and Given, When, Then steps allow us to chain together expected behaviour into a scenario where we have:

- A clear understanding of the nature of what the good path and bad paths we are testing for are. Essentially, we have more readable test that also chains together snippets of expected paths.

## Documentation Example 

As a demonstration, we will create a scenario test case that is included in the above documentation. This is also included [here](https://teststackbddfy.readthedocs.io/en/latest/).

The scenario we will be testing is written in plain english here:
```
Scenario 3: Card has been disabled
Given the card is disabled
When the Account Holder requests $20
Then the ATM should retain the card
  And the ATM should say the card has been retained
```

We will need to install the TestStack.BDDfy nuget package.

We create a `[TestClass]` called `CardHasBeenDisabled.cs` that implements a few methods using the Given => When => Then structure of BDDfy tests. This can be seen below (unimplement for now):

``` 
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestStack.BDDfy;

namespace BDDfyExample
{
    [TestClass]
    public class CardHasBeenDisabled
    {
        void GivenTheCardIsDisabled()
        {
            throw new NotImplementedException();
        }

        void WhenTheAccountHolderREquestsMoney() { }

        void ThenTheAtmShoudlRetainTheCard() { }

        void AndTheAtmShouldSayTheCardHasBeenDisabled() { }

        [TestMethod]
        public void Execute()
        {
            this.BDDfy();
        }
    }
}
```

In our example there are two projects - a simple BDDfyExample project which implements some logic based upon a hypothetical ATM; the other is the respective test project.

Ignoring the logic in the base base class for now we are interested in the BDDfyExample.Tests project where we have now implemented the `CardHasBeenDisabled` test class.

The following code will be discussed in more detail, below: 
```
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TestStack.BDDfy;
using BDDfyExample;

namespace BDDfyExample.Tests
{
    [TestClass]
    public class CardHasBeenDisabled
    {
        private Card _card;
        Atm _subject;

        void GivenTheCardIsDisabled()
        {
            _card = new Card(false, 100);
            _subject = new Atm(100);
        }

        void WhenTheAccountHolderRequestsMoney()
        {
            _subject.RequestMoney(_card, 20);
        }

        void ThenTheAtmShouldRetainTheCard()
        {
            Assert.IsTrue(_subject.CardIsRetained);
        }

        void AndTheAtmShouldSayTheCardHasBeenRetained()
        {
            Assert.AreEqual(DisplayMessage.CardIsRetained, _subject.Message);
        }

        [TestMethod]
        public void Execute()
        {
            this.BDDfy();
        }
    }
}
```

In the `GivenTheCardIsDisabled()` method we register the Given steps, creating a new card which has `enabled = false` although it has the correct ammount of funds on it (e.g. `100`) and setup an ATM which could dispense those funds given the card is activated. 

In the `WhenTheAccountHolderRequestsMoney()` When method we define what steps we are trying to test, i.e. `_subject.RequestMoney(_card, 20);` which passes in the `_card` object and requests `20`.

Next, in the `ThenTheAtmShouldRetainTheCard()` and `AndTheAtmShouldSayTheCardHasBeenRetained()` methods we complete our assertions that assert the card is retained and the nature of the display message returned.

Finally, we have the `[TestMethod]` which executes the BDDfy steps.

This concludes our very basic example for the way to setup and run a BDDfy test, however, like in unit tests we could mock objects and add more in-depth detail to the nature of these tests. In a future project we should be able to see this in the Cubus3 test projects and get a better understanding for more detailed implementations of this testing tool.

