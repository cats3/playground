﻿
using Google.Apis.Auth.OAuth2;
using Google.Apis.Drive.v3;
using Google.Apis.Upload;
using MimeKit;

namespace DriveAPITest
{
    internal class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            Console.WriteLine("Drive API Test:");
            Console.WriteLine("===============");
            try
            {
                new Program().Run().Wait();
            }
            catch (AggregateException ex)
            {
                foreach (var e in ex.InnerExceptions)
                {
                    Console.WriteLine("ERROR: " + e.Message);
                }
            }
        }

        private async Task Run()
        {
            DriveService service = GetDriveService();

            string? folderId = await GetFolderIdOrCreateIfNotExistAsync(service, "foo2", "1-U5BVaBQpgk1j4f5gayakoR9AMHsVyW-", "0AMz-O1t1PomlUk9PVA");

            if (folderId != null)
            {
                Progress<long> progress = new();
                progress.ProgressChanged += Progress_ProgressChanged;
                
                await UploadFileAsync(service, "TestFile.txt", "test.txt", folderId, progress);

                progress.ProgressChanged -= Progress_ProgressChanged;
            }
        }

        private void Progress_ProgressChanged(object? sender, long e)
        {
            Console.WriteLine($"uploaded {e}%");
        }

        private static async Task<string?> GetFolderIdOrCreateIfNotExistAsync(DriveService service, string folderName, string parentId, string driveId)
        {
            string? folderId = await FirstFolderIdOrDefaultAsync(service, folderName, parentId, driveId);

            if (folderId == null)
                folderId = await CreateNewFolderAsync(service, folderName, parentId);

            return folderId;
        }

        private static async Task<string?> FirstFolderIdOrDefaultAsync(DriveService service, string folderName, string parentId, string driveId)
        {
            bool keepSearching = true;
            string? pageToken = null;
            Google.Apis.Drive.v3.Data.File? resultFolder;
            do
            {
                var search = service.Files.List();
                search.Q = "mimeType='application/vnd.google-apps.folder'";
                search.Spaces = "drive";
                search.Corpora = "drive";
                search.SupportsAllDrives = true;
                search.DriveId = driveId;
                search.PageToken = pageToken;
                search.Fields = "nextPageToken, files(id, name, parents)";
                search.IncludeItemsFromAllDrives = true;

                var files = await search.ExecuteAsync();
                resultFolder = files?.Files.FirstOrDefault(f => f.Parents.Contains(parentId) && String.Equals(f.Name, folderName));

                pageToken = files?.NextPageToken;
                keepSearching = pageToken != null && resultFolder == null;
            } while (keepSearching);

            return resultFolder?.Id;
        }

        private static async Task<string?> CreateNewFolderAsync(DriveService service, string folderName, string parentId)
        {
            var newFolderMetaData = GetFileMetaData(folderName, parentId, "application/vnd.google-apps.folder");

            FilesResource.CreateRequest folderRequest = service.Files.Create(newFolderMetaData);
            folderRequest.Fields = "id";
            folderRequest.SupportsAllDrives = true;

            var result = await folderRequest.ExecuteAsync();
            return result?.Id;
        }

        private async Task<string?> UploadFileAsync(DriveService service, string localPath, string gdriveFilename, string gdriveFolder, IProgress<long> progress)
        {
            var mimeType = MimeTypes.GetMimeType(Path.GetFileName(localPath));
            var newFileMetaData = GetFileMetaData(gdriveFilename, gdriveFolder, mimeType);

            FilesResource.CreateMediaUpload request;
            IUploadProgress result;

            using (var stream = new FileStream(localPath, FileMode.Open))
            {
                request = service.Files.Create(newFileMetaData, stream, mimeType);
                request.Fields = "id";
                request.SupportsAllDrives = true;
                request.ProgressChanged += (p) => { progress.Report(p.BytesSent * 100 / stream.Length); };
                result = await request.UploadAsync();
            }

            if (result.Status != UploadStatus.Completed)
                throw result.Exception;

            var fileUploaded = request.ResponseBody;

            return fileUploaded.Id;
        }

        private static Google.Apis.Drive.v3.Data.File GetFileMetaData(string gdriveFilename, string gdriveFolder, string mimeType)
        {
            return new Google.Apis.Drive.v3.Data.File()
            {
                Name = gdriveFilename,
                MimeType = mimeType,
                Parents = new List<string>()
                    {
                        gdriveFolder
                    }
            };
        }

        private static DriveService GetDriveService()
        {
            GoogleCredential credential;
            using (var stream = new FileStream("client_secrets.json", FileMode.Open, FileAccess.Read))
            {
                credential = GoogleCredential.FromStream(stream)
                        .CreateScoped(DriveService.Scope.Drive, DriveService.Scope.DriveFile);
            }

            // Create the service.
            var service = new DriveService(new DriveService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "drive uploader"
            });
            return service;
        }
    }
}