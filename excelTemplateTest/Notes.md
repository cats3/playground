﻿# General Notes on excelTemplateTest

This project makes and tests the excel (or any) spreadsheet report generator tool for use with C3 session files.

# How it works

The report generator tool takes a list of session files and a template spreadsheet file.  Cells within the template file are then filled in with values that can be found within the list of session files.  There are two types of cell that can be filled in and they work in specific ways.

## Field Cells

Field cells are used for properties that relate to a test element, i.e. they are common to all session files (assuming that the session files are all generated from the same type of test).  Wherever a cell is found in the template spreadsheet that contains text that starts with a specific prefix ("Field=" in the example case) then the text following the prefix is used to search for a matching test element property label.  For example, if a cell contains the text "Field=Id" then the report generator will fill in the value for property 'Id' into that cell, replacing the text "Field=Id".  If a cell containing the prefix is not matched to a property label within the file then the text " not found" is appended to the cell.

## Column Cells

Column cells are used for properties that relate to a specimen within the session file.  By using a list of session files (i.e. many speciemns) a table of results can be generated with one row per specimen.  Whereever a cell is found in the template spreadsheet that contains text that starts with a specific prefix ("Column=" in the example case) then the text following the prefix is used to search for a matching specimen property label.  The next row down in the spreadsheet is used as a template row to fill in the matching specimen property values per matching labels.  For example, if a row contains two cells with text values "Column=Width" and "Column=Height" then the report generator will search for matchin specimen properties with labels 'Width' and 'Height' and insert a new row per specimen file.  The first session file replaces the template row in the resulting spreadsheet and all formulae and formatting in the template row are copied with cell references updated accordingly.

### Renaming Column Headers

The matching column headers are renamed to remove the prefix leaving just the property label intact.  If a cell's text contains the dollar sign '$' then the text after the sign will be used as the column header instead.  For example, the column header in the template may contain "Column=Ambient$Temperature" and in this example the report generator will replace the column header with the text 'Temperature' after matching the property label 'Ambient'.

# Limitations / Known Issues

The formulae cell reference logic works for simple formulae but hasn't been tested against all possibilities.  One suspected issue is where a cell may contain text that looks like a cell refernce, e.g. "Hello World C3" and if the row where this cell is contained is moved by the addition of rows during the report generation then the substring 'C3' will be modified because it looks like a cell reference.  That is to say that the regex matcher is not clever enough to know the difference between cell references and strings (yet).

The calculation chain element is cleared at the end of the report generation.  Accoring to the ISO specification for open spreadsheet formats states that this element is optional and in testing had not been found to cause issues.

For each added row the print area for the sheet is increased by a row.  An assumption is made that this is correct and desired behaviour but may not be.

Only the first sheet within a workbook is used for the template.  Better functionality would be to repeat the matching process for each sheet within a workbook however this opens up more logic to handle cross-sheet referenced cells and formulae.

