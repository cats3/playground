﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using C3.Data;

namespace excelTemplateTest
{
    public static class ExcelReport
    {
        /// <summary>
        /// Fill in fields in Excel file 'fileName' that start with 'fieldPrefix' given Element Parameter data in 'sessionFile'.
        /// Any prefixed fileds that are not matched with parameters are highlighted with 'not found'.
        /// </summary>
        /// <param name="fileName">Excel file to fill</param>
        /// <param name="sessionFile">Session file</param>
        /// <param name="fieldPrefix">field prefix string</param>
        public static void FillFieldSeries(string fileName, SessionFile sessionFile, string fieldPrefix)
        {
            using (SpreadsheetDocument d = SpreadsheetDocument.Open(fileName, true))
            {
                SharedStringTable sharedStringTable = d.WorkbookPart.SharedStringTablePart.SharedStringTable;
                foreach (SharedStringItem sharedStringItem in sharedStringTable.Elements<SharedStringItem>().Where(s => s.InnerText.StartsWith(fieldPrefix)))
                {
                    var newString = sessionFile.ElementProperties.Where(p => fieldPrefix + p.Name == sharedStringItem.InnerText).Select(s => s.Value).FirstOrDefault();
                    if (newString != null)
                        sharedStringItem.Text = new Text(newString.ToString());
                    else
                        sharedStringItem.Text = new Text(sharedStringItem.InnerText + " not found");
                }
                d.Close();
            }
        }

        /// <summary>
        /// Create a row in the given spreadeheet per specimen file beneath a row that contains column headings that match
        /// specimen parameters plus a prefix string
        /// </summary>
        /// <param name="fileName">Excel spreadsheet file</param>
        /// <param name="sessionFiles">Session files with parameters to match</param>
        /// <param name="columnPrefix">Column prefix string to match</param>
        public static void FillColumnSeries(string fileName, List<SessionFile> sessionFiles, string columnPrefix)
        {
            var hi = GetHeaderIndicies(fileName, columnPrefix);
            using (SpreadsheetDocument d = SpreadsheetDocument.Open(fileName, true))
            {
                WorksheetPart worksheetPart = d.WorkbookPart.WorksheetParts.First();
                SheetData sheetData = worksheetPart.Worksheet.GetFirstChild<SheetData>();
                SharedStringTable sharedStringTable = d.WorkbookPart.SharedStringTablePart.SharedStringTable;
                IEnumerable<Row> rows = worksheetPart.Worksheet.Descendants<Row>().Where(r => r.Elements<Cell>().Any(c => c.DataType == "s"));
                foreach (Row r in rows)
                {
                    IEnumerable<Cell> cells = r.Elements<Cell>().Where(c => c.DataType == "s" && hi.Contains(c.InnerText));
                    if (cells.Count() > 0)
                    {
                        // r is a row with at least one cell that contains 'columnPrefix' text
                        // cells is a list of cells that need replacing with real text
                        uint newRowIndex = r.RowIndex;
                        int count = 0;
                        Row newRow = null;
                        foreach (var sessionFile in sessionFiles)
                        {
                            int matching = 0;
                            newRowIndex++;
                            if (count > 0)
                                newRow = (Row)r.NextSibling<Row>().Clone();
                            else
                                newRow = r.NextSibling<Row>();
                            foreach (var cell in cells)
                            {
                                int index = int.Parse(cell.InnerText);
                                Cell updateCell = newRow.Elements<Cell>().Where(c => ColumnName(c) == ColumnName(cell)).FirstOrDefault();
                                // TODO: ugh?  Best way of doing this?  I just want the text without the prefix
                                string matchingString = sharedStringTable.ElementAt(index).InnerText.Split(columnPrefix).Last();
                                matchingString = matchingString.Split('$').First();   // remove any $ there (suggest alternate name for column heading)
                                // get the approriate specimen parameter matching 'matchingString'
                                var foo = sessionFile.Specimen.Properties.Where(p => p.Label == matchingString).FirstOrDefault();
                                if (foo != null)
                                {
                                    matching++;
                                    updateCell.CellValue = new CellValue(foo.Value);
                                    updateCell.DataType = new EnumValue<CellValues>(CellValues.Number);
                                }
                            }
                            if (matching > 0)
                            {
                                if (count > 0)
                                {
                                    InsertRow(newRowIndex, worksheetPart, newRow, false);
                                    // TODO: update the print area (FLAKY, DODGY CODE WARNING)
                                    UpdatePrintArea(d.WorkbookPart);
                                }
                            }
                            count++;
                        }
                    }
                }
                RenameColumnHeadings(sharedStringTable, columnPrefix);
                d.WorkbookPart.Workbook.CalculationProperties.FullCalculationOnLoad = true;
                d.WorkbookPart.Workbook.CalculationProperties.ForceFullCalculation = true;
                // TODO: Nasty but it works in Excel, Google Sheets and LibreOffice (I've checked!)
                d.WorkbookPart.DeletePart(d.WorkbookPart.CalculationChainPart);
                d.Close();
            }
        }

        public static void RenameColumnHeadings(SharedStringTable sharedStringTable, string columnPrefix)
        {
            foreach (var st in sharedStringTable.Elements<SharedStringItem>().Where(s => s.InnerText.StartsWith(columnPrefix)))
            {
                string newString = "";
                if (st.InnerText.Contains('$'))
                {
                    newString = st.InnerText.Split('$', StringSplitOptions.None).Last();
                }
                else
                {
                    newString = st.InnerText.Split(columnPrefix, StringSplitOptions.None).Last();
                }
                st.Text = new Text(newString);
            }
        }
        /// <summary>
        /// Inserts a new row at the desired index. If one already exists, then it is
        /// returned. If an insertRow is provided, then it is inserted into the desired
        /// rowIndex
        /// </summary>
        /// <param name="rowIndex">Row Index</param>
        /// <param name="worksheetPart">Worksheet Part</param>
        /// <param name="insertRow">Row to insert</param>
        /// <param name="isLastRow">Optional parameter - True, you can guarantee that this row is the last row (not replacing an existing last row) in the sheet to insert; false it is not</param>
        /// <returns>Inserted Row</returns>
        public static Row InsertRow(uint rowIndex, WorksheetPart worksheetPart, Row insertRow, bool isNewLastRow = false)
        {
            Worksheet worksheet = worksheetPart.Worksheet;
            SheetData sheetData = worksheet.GetFirstChild<SheetData>();

            Row retRow = !isNewLastRow ? sheetData.Elements<Row>().FirstOrDefault(r => r.RowIndex == rowIndex) : null;

            // If the worksheet does not contain a row with the specified row index, insert one.
            if (retRow != null)
            {
                // if retRow is not null and we are inserting a new row, then move all existing rows down.
                if (insertRow != null)
                {
                    UpdateRowIndexes(worksheetPart, rowIndex, false);
                    UpdateMergedCellReferences(worksheetPart, rowIndex, false);
                    UpdateHyperlinkReferences(worksheetPart, rowIndex, false);

                    // actually insert the new row into the sheet
                    retRow = sheetData.InsertBefore(insertRow, retRow);  // at this point, retRow still points to the row that had the insert rowIndex

                    string curIndex = retRow.RowIndex.ToString();
                    string newIndex = rowIndex.ToString();

                    foreach (Cell cell in retRow.Elements<Cell>())
                    {
                        // Update the references for the row's cells.
                        cell.CellReference = new StringValue(cell.CellReference.Value.Replace(curIndex, newIndex));
                        if (cell.CellFormula != null)
                        {
                            // cell has a formula
                            string newFormula = FormulaHelper.UpdateFormulaCellReferences(cell.CellFormula.Text, (int)(rowIndex - int.Parse(curIndex)));
                            cell.CellFormula = new CellFormula(newFormula);
                        }
                    }

                    // Update the row index.
                    retRow.RowIndex = rowIndex;

                    // update the dimensions
                    SheetDimension sheetDimension = worksheetPart.Worksheet.SheetDimension;
                    string newDimensions = FormulaHelper.UpdateRangeEndRow(sheetDimension.Reference.InnerText);
                    sheetDimension.Reference.InnerText = newDimensions;
                }
            }
            else
            {
                // Row doesn't exist yet, shifting not needed.
                // Rows must be in sequential order according to RowIndex. Determine where to insert the new row.
                Row refRow = !isNewLastRow ? sheetData.Elements<Row>().FirstOrDefault(row => row.RowIndex > rowIndex) : null;

                // use the insert row if it exists
                retRow = insertRow ?? new Row() { RowIndex = rowIndex };

                IEnumerable<Cell> cellsInRow = retRow.Elements<Cell>();

                if (cellsInRow.Any())
                {
                    string curIndex = retRow.RowIndex.ToString();
                    string newIndex = rowIndex.ToString();

                    foreach (Cell cell in cellsInRow)
                    {
                        // Update the references for the row's cells.
                        cell.CellReference = new StringValue(cell.CellReference.Value.Replace(curIndex, newIndex));
                    }

                    // Update the row index.
                    retRow.RowIndex = rowIndex;
                }

                sheetData.InsertBefore(retRow, refRow);
            }

            return retRow;
        }

        internal static string ColumnName(Cell cell)
        {
            Regex regex = new Regex("[A-Za-z]+");
            string colref = regex.Match(cell.CellReference).Value;
            return colref;
        }

        /// <summary>
        /// Returns a list of indicies of shared string table strings that being with 'prefix'
        /// within the spreadsheet file 'fileName'
        /// </summary>
        /// <param name="fileName">spreadsheet to work through</param>
        /// <param name="prefix">prefix string</param>
        /// <returns></returns>
        public static List<string> GetHeaderIndicies(string fileName, string prefix)
        {
            var ret = new List<string>();
            using (SpreadsheetDocument d = SpreadsheetDocument.Open(fileName, false))
            {
                SharedStringTable sharedStringTable = d.WorkbookPart.SharedStringTablePart.SharedStringTable;
                int i = 0;
                foreach (SharedStringItem sharedStringItem in sharedStringTable.Elements<SharedStringItem>())
                {
                    string text = sharedStringItem.InnerText;
                    if (text.StartsWith(prefix))
                        ret.Add(i.ToString());
                    i++;
                }
            }
            return ret;
        }

        /// <summary>
        /// Updates all of the Row indexes and the child Cells' CellReferences whenever
        /// a row is inserted or deleted.
        /// </summary>
        /// <param name="worksheetPart">Worksheet Part</param>
        /// <param name="rowIndex">Row Index being inserted or deleted</param>
        /// <param name="isDeletedRow">True if row was deleted, otherwise false</param>
        private static void UpdateRowIndexes(WorksheetPart worksheetPart, uint rowIndex, bool isDeletedRow)
        {
            // Get all the rows in the worksheet with equal or higher row index values than the one being inserted/deleted for reindexing.
            IEnumerable<Row> rows = worksheetPart.Worksheet.Descendants<Row>().Where(r => r.RowIndex.Value >= rowIndex);

            foreach (Row row in rows)
            {
                uint newIndex = (isDeletedRow ? row.RowIndex - 1 : row.RowIndex + 1);
                string curRowIndex = row.RowIndex.ToString();
                string newRowIndex = newIndex.ToString();
                bool isAdjacentRow = (rowIndex == row.RowIndex);
                foreach (Cell cell in row.Elements<Cell>())
                {
                    // Update the references for the rows cells.
                    cell.CellReference = new StringValue(cell.CellReference.Value.Replace(curRowIndex, newRowIndex));
                    if (cell.CellFormula != null)
                    {
                        // cell has a formula
                        string newFormula = FormulaHelper.UpdateFormulaCellReferences(cell.CellFormula.Text, (int)(newIndex - int.Parse(curRowIndex)),isAdjacentRow);
                        cell.CellFormula = new CellFormula(newFormula);
                    }
                }

                // Update the row index.
                row.RowIndex = newIndex;
            }
        }

        /// <summary>
        /// Updates the MergedCelss reference whenever a new row is inserted or deleted. It will simply take the
        /// row index and either increment or decrement the cell row index in the merged cell reference based on
        /// if the row was inserted or deleted.
        /// </summary>
        /// <param name="worksheetPart">Worksheet Part</param>
        /// <param name="rowIndex">Row Index being inserted or deleted</param>
        /// <param name="isDeletedRow">True if row was deleted, otherwise false</param>
        private static void UpdateMergedCellReferences(WorksheetPart worksheetPart, uint rowIndex, bool isDeletedRow)
        {
            if (worksheetPart.Worksheet.Elements<MergeCells>().Count() > 0)
            {
                MergeCells mergeCells = worksheetPart.Worksheet.Elements<MergeCells>().FirstOrDefault();

                if (mergeCells != null)
                {
                    // Grab all the merged cells that have a merge cell row index reference equal to or greater than the row index passed in
                    List<MergeCell> mergeCellsList = mergeCells.Elements<MergeCell>().Where(r => r.Reference.HasValue)
                                                                                     .Where(r => GetRowIndex(r.Reference.Value.Split(':').ElementAt(0)) >= rowIndex ||
                                                                                                 GetRowIndex(r.Reference.Value.Split(':').ElementAt(1)) >= rowIndex).ToList();

                    // Need to remove all merged cells that have a matching rowIndex when the row is deleted
                    if (isDeletedRow)
                    {
                        List<MergeCell> mergeCellsToDelete = mergeCellsList.Where(r => GetRowIndex(r.Reference.Value.Split(':').ElementAt(0)) == rowIndex ||
                                                                                       GetRowIndex(r.Reference.Value.Split(':').ElementAt(1)) == rowIndex).ToList();

                        // Delete all the matching merged cells
                        foreach (MergeCell cellToDelete in mergeCellsToDelete)
                        {
                            cellToDelete.Remove();
                        }

                        // Update the list to contain all merged cells greater than the deleted row index
                        mergeCellsList = mergeCells.Elements<MergeCell>().Where(r => r.Reference.HasValue)
                                                                         .Where(r => GetRowIndex(r.Reference.Value.Split(':').ElementAt(0)) > rowIndex ||
                                                                                     GetRowIndex(r.Reference.Value.Split(':').ElementAt(1)) > rowIndex).ToList();
                    }

                    // Either increment or decrement the row index on the merged cell reference
                    foreach (MergeCell mergeCell in mergeCellsList)
                    {
                        string[] cellReference = mergeCell.Reference.Value.Split(':');

                        if (GetRowIndex(cellReference.ElementAt(0)) >= rowIndex)
                        {
                            string columnName = GetColumnName(cellReference.ElementAt(0));
                            cellReference[0] = isDeletedRow ? columnName + (GetRowIndex(cellReference.ElementAt(0)) - 1).ToString() : IncrementCellReference(cellReference.ElementAt(0), CellReferencePartEnum.Row);
                        }

                        if (GetRowIndex(cellReference.ElementAt(1)) >= rowIndex)
                        {
                            string columnName = GetColumnName(cellReference.ElementAt(1));
                            cellReference[1] = isDeletedRow ? columnName + (GetRowIndex(cellReference.ElementAt(1)) - 1).ToString() : IncrementCellReference(cellReference.ElementAt(1), CellReferencePartEnum.Row);
                        }

                        mergeCell.Reference = new StringValue(cellReference[0] + ":" + cellReference[1]);
                    }
                }
            }
        }

        /// <summary>
        /// Updates all hyperlinks in the worksheet when a row is inserted or deleted.
        /// </summary>
        /// <param name="worksheetPart">Worksheet Part</param>
        /// <param name="rowIndex">Row Index being inserted or deleted</param>
        /// <param name="isDeletedRow">True if row was deleted, otherwise false</param>
        private static void UpdateHyperlinkReferences(WorksheetPart worksheetPart, uint rowIndex, bool isDeletedRow)
        {
            Hyperlinks hyperlinks = worksheetPart.Worksheet.Elements<Hyperlinks>().FirstOrDefault();

            if (hyperlinks != null)
            {
                Match hyperlinkRowIndexMatch;
                uint hyperlinkRowIndex;

                foreach (Hyperlink hyperlink in hyperlinks.Elements<Hyperlink>())
                {
                    hyperlinkRowIndexMatch = Regex.Match(hyperlink.Reference.Value, "[0-9]+");
                    if (hyperlinkRowIndexMatch.Success && uint.TryParse(hyperlinkRowIndexMatch.Value, out hyperlinkRowIndex) && hyperlinkRowIndex >= rowIndex)
                    {
                        // if being deleted, hyperlink needs to be removed or moved up
                        if (isDeletedRow)
                        {
                            // if hyperlink is on the row being removed, remove it
                            if (hyperlinkRowIndex == rowIndex)
                            {
                                hyperlink.Remove();
                            }
                            // else hyperlink needs to be moved up a row
                            else
                            {
                                hyperlink.Reference.Value = hyperlink.Reference.Value.Replace(hyperlinkRowIndexMatch.Value, (hyperlinkRowIndex - 1).ToString());

                            }
                        }
                        // else row is being inserted, move hyperlink down
                        else
                        {
                            hyperlink.Reference.Value = hyperlink.Reference.Value.Replace(hyperlinkRowIndexMatch.Value, (hyperlinkRowIndex + 1).ToString());
                        }
                    }
                }

                // Remove the hyperlinks collection if none remain
                if (hyperlinks.Elements<Hyperlink>().Count() == 0)
                {
                    hyperlinks.Remove();
                }
            }
        }

        /// <summary>
        /// Given a cell name, parses the specified cell to get the row index.
        /// </summary>
        /// <param name="cellReference">Address of the cell (ie. B2)</param>
        /// <returns>Row Index (ie. 2)</returns>
        public static uint GetRowIndex(string cellReference)
        {
            // Create a regular expression to match the row index portion the cell name.
            Regex regex = new Regex(@"\d+");
            Match match = regex.Match(cellReference);

            return uint.Parse(match.Value);
        }

        /// <summary>
        /// Increments the reference of a given cell.  This reference comes from the CellReference property
        /// on a Cell.
        /// </summary>
        /// <param name="reference">reference string</param>
        /// <param name="cellRefPart">indicates what is to be incremented</param>
        /// <returns></returns>
        public static string IncrementCellReference(string reference, CellReferencePartEnum cellRefPart)
        {
            string newReference = reference;

            if (cellRefPart != CellReferencePartEnum.None && !String.IsNullOrEmpty(reference))
            {
                string[] parts = Regex.Split(reference, "([A-Z]+)");

                if (cellRefPart == CellReferencePartEnum.Column || cellRefPart == CellReferencePartEnum.Both)
                {
                    List<char> col = parts[1].ToCharArray().ToList();
                    bool needsIncrement = true;
                    int index = col.Count - 1;

                    do
                    {
                        // increment the last letter
                        col[index] = Letters[Letters.IndexOf(col[index]) + 1];

                        // if it is the last letter, then we need to roll it over to 'A'
                        if (col[index] == Letters[Letters.Count - 1])
                        {
                            col[index] = Letters[0];
                        }
                        else
                        {
                            needsIncrement = false;
                        }

                    } while (needsIncrement && --index >= 0);

                    // If true, then we need to add another letter to the mix. Initial value was something like "ZZ"
                    if (needsIncrement)
                    {
                        col.Add(Letters[0]);
                    }

                    parts[1] = new String(col.ToArray());
                }

                if (cellRefPart == CellReferencePartEnum.Row || cellRefPart == CellReferencePartEnum.Both)
                {
                    // Increment the row number. A reference is invalid without this componenet, so we assume it will always be present.
                    parts[2] = (int.Parse(parts[2]) + 1).ToString();
                }

                newReference = parts[1] + parts[2];
            }

            return newReference;
        }

        /// <summary>
        /// Nasty fudge of a function that only updates the end row of the print area
        /// TODO: revisit this as there's more to consider
        /// </summary>
        /// <param name="workbookPart">The workbook part that contains the print area to update</param>
        /// <param name="rowDelta">Number of rows to change by</param>
        public static void UpdatePrintArea(WorkbookPart workbookPart, int rowDelta = 1)
        {
            var definedNames = workbookPart.Workbook.Elements<DefinedNames>().First();
            DefinedName definedName = definedNames.Elements<DefinedName>().Where(n => n.Name == "_xlnm.Print_Area").First();
            string printArea = definedName.InnerText;
            definedName.Text = FormulaHelper.UpdateRangeEndRow(printArea, rowDelta, true);
        }

        /// <summary>
        /// Given a cell name, parses the specified cell to get the column name.
        /// </summary>
        /// <param name="cellReference">Address of the cell (ie. B2)</param>
        /// <returns>Column name (ie. A2)</returns>
        private static string GetColumnName(string cellName)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellName);

            return match.Value;
        }
        public enum CellReferencePartEnum
        {
            None,
            Column,
            Row,
            Both
        }
        private static List<char> Letters = new List<char>() { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', ' ' };

    }
}
