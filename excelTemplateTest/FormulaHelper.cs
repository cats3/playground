﻿using System;
using System.Text.RegularExpressions;

namespace excelTemplateTest
{
    /// <summary>
    /// A class to help manipulate formulae in OpenXML spreadsheets
    /// </summary>
    public static class FormulaHelper
    {
        /// <summary>
        /// Update the formula cell references for a given formula and a row difference
        /// Only those cell references where rows can change are updated
        /// </summary>
        /// <param name="formulaString">Formula string to update</param>
        /// <param name="rowDelta">amount to adjust valid current row references by</param>
        /// <returns>updated formula string</returns>
        public static string UpdateFormulaCellReferences(string formulaString, int rowDelta, bool onlyRangeEnds=false)
        {
            if (formulaString == null)
                return null;
            // we only want to adjust single cell references and ends of ranges
            // pattern for finding cells where the row ref can change (don't care about columns)
            string r = "([$]*[A-Z]+[1-9][0-9]*)";
            if (onlyRangeEnds == true)
                r = @"(?![$]*[A-Z]+\d*:)" + r;
            
            CellReferenceUpdater updater = new CellReferenceUpdater(rowDelta);
            return Regex.Replace(formulaString, r, updater.Update);
        }
        
        /// <summary>
        /// Get the row number from a cell reference (e.g. AA123 returns 123)
        /// </summary>
        /// <param name="cellReference"></param>
        /// <returns>row part of the cell reference</returns>
        public static int GetRowIndex(string cellReference)
        {
            // Create a regular expression to match the row index portion the cell name.
            Regex regex = new Regex(@"\d+");
            Match match = regex.Match(cellReference);
            return int.Parse(match.Value);
        }

        public static string UpdateRangeEndRow(string rangeReference, int rowDelta=1, bool updateFixed=false)
        {
            if (rangeReference == null)
                return null;
            string findFixed = updateFixed ? "[$]*" : "";
            string r = $"(?<=[:])[$]*[A-Z]+{findFixed}[1-9][0-9]*";
            CellReferenceUpdater updater = new CellReferenceUpdater(rowDelta);
            return Regex.Replace(rangeReference, r, updater.Update);
        }
    }

    /// <summary>
    /// Helper class with MatchEvalualator delegate function for updating row references
    /// by a specific delta
    /// </summary>
    public class CellReferenceUpdater
    { 
        public int rowDelta = 0;

        public CellReferenceUpdater(int rowDelta)
        {
            this.rowDelta = rowDelta;
        }

        public string Update(Match m)
        {
            int rowIndex = FormulaHelper.GetRowIndex(m.Value);
            int newRowIndex = Math.Max(1,rowIndex + rowDelta);
            return m.Value.Replace(rowIndex.ToString(), newRowIndex.ToString());
        }
    }
}
