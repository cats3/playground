﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using C3.Data;
using C3.Data.Session.Base;
using C3.Test.Specimens;
using C3.Test.Enums;

namespace excelTemplateTest
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Testing ExcelReport function!");

            // there should be a set of .csf files in the .\test folder!
            List<SessionFile> sessionFiles = new List<SessionFile>();
            foreach (var f in Directory.EnumerateFiles(Directory.GetCurrentDirectory() + "\\test"))
            {
                if (Path.GetExtension(f) == ".csf")
                {
                    Console.WriteLine($"Adding test file {f}");
                    var sessionFile = SessionFile.Load(f, false);
                    InsertDummyCompressiveStrainValues(sessionFile);
                    sessionFiles.Add(sessionFile);
                }
            }
            Console.WriteLine($"Found {sessionFiles.Count} file(s)");

            string reportFilename = Path.ChangeExtension(Path.GetRandomFileName(), "xlsx");
            File.Copy("test\\test.xlsx", reportFilename, true);
            ExcelReport.FillFieldSeries(reportFilename, sessionFiles[0], "Field=");
            ExcelReport.FillColumnSeries(reportFilename, sessionFiles, "Column=");

            Console.WriteLine("Report generation complete!");

            Console.ReadKey();
        }

        public static void InsertDummyCompressiveStrainValues(SessionFile sessionFile)
        {
            // for the session file given, add dummy compressive strain values if there aren't any
            // compressive strain values are percentages
            // there are five per test
            // they are specimen properties
            SpecimenPropertyCollection specimenProperties = sessionFile.Specimen.Properties;
            if (!specimenProperties.Any(a=>a.Label.Contains("CompressiveStrain")))
            {
                Random rand = new Random();
                for(var i = 1; i <= 5; i++)
                {
                    SpecimenProperty prop = new SpecimenProperty(eSpecimenPropertyType.Number);
                    prop.Label = $"CompressiveStrain{i}";
                    double dummyValue = (double)rand.Next(850, 950) / 10;
                    prop.Value = dummyValue.ToString();
                    specimenProperties.Add(prop);
                }
            }
        }
    }
}
