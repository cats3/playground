﻿using C3.Common.Injection;
using Cubus.Displays.Services;
using DisplayCanvas;
using DisplayCanvas.Services;
using DisplayCanvas.UI.Canvas;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DisplayCanvasDemo
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();


            Services.Register(CanvasService, typeof(IManagementService));
            Services.Register(serv, typeof(IDisplayService));

            serv.GetDisplays().ForEach(t => CanvasService.LoadedDisplayTypes.Add(Activator.CreateInstance(t) as Display));

            groupModel = new CanvasGroup();
            groupModel.AddCanvas();
            groupModel.AddCanvas();
            groupModel.AddCanvas();
            groupModel.AddCanvas();
            this.DataContext = CanvasService;
            cc.Displays = CanvasService.LoadedDisplayTypes;
            
           

           // cc.SetSelectedIndex(0);
        }

        CanvasManagementService CanvasService = new CanvasManagementService();
        DisplayInjectionService serv = new DisplayInjectionService();
        //DisplayCanvas.Canvas canvasModel = new DisplayCanvas.Canvas();
        CanvasGroup groupModel = null;

        SplitterCanvasModel getDemoCanvas()
        {
            var canvasModel = new SplitterCanvasModel();
            canvasModel.CanvasName = "New Canvas ";
            return canvasModel;
        }

        TestDisplayModel getModel()
        {
            var dm = new TestDisplayModel();
            dm.Parameters.Add(new DisplayParameter<string>() { Name = "Name", Value = "Insert Canvas Name Here" });
            dm.Parameters.Add(new DisplayParameter<double>() { Name = "Maximum", Value = 100.0 });
            dm.Parameters.Add(new DisplayParameter<double>() { Name = "Minimum", Value = 100.0 });
            dm.Parameters.Add(new DisplayParameter<double>() { Name = "Power Level", Value = 9001.0 });
            


            dm._internal_PrepareUIElements();

            return dm;
        }

        bool LOADMODE = true;

        private void btnTemp_Click(object sender, RoutedEventArgs e)
        {
            if (!LOADMODE)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.DefaultExt = ".xml";

                if (sfd.ShowDialog().Value)
                {
                    CanvasService.SaveState(sfd.FileName);
                }

            }
            else
            {
                OpenFileDialog ofd = new OpenFileDialog();
                if(ofd.ShowDialog().Value)
                {
                    CanvasService.LoadStateFromFile(ofd.FileName);
                }
            }

        }
    }

   
}
