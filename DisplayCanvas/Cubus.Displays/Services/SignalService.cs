﻿using C3System;
using C3.Common.WPF.Input;
using DisplayCanvas;
using DisplayCanvas.Interfaces;
using Interfaces.MachineInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using C3.Common.Models;
using C3.Common;

namespace Cubus.Displays.Services
{
    public class SignalService : ISignalService
    {
        public SignalService()
        {            
            _tools = CubusTOOLS.CubusTool;
            _tools.GetSignals(false).ForEach(s => Signals.Add(new DisplaySignal(s)));
            StartUpdating();
        }

        CubusTOOLS _tools;

        public List<IDisplaySignal> Signals { get; set; } = new List<IDisplaySignal>();

        public void StartUpdating()
        {
            if (running)
                return;

            ManagedTask.Run("Signal Service Updater", () =>
            {
                running = true;

                while (running)
                {
                    Signals.ForEach(s => s.Update());
                    Task.Delay(new TimeSpan(0, 0, 0, 0, RefreshPeriod)).Wait();
                }
            });
        }
        
        public void StopUpdating()
        {
            running = false;
        }

        public void Shutdown()
        {
            StopUpdating();
            
        }

        public List<IDisplaySignal> GetSignals()
        {
            return Signals;
        }

        bool running = false;

        /// <summary>
        /// Refresh Period in Milliseconds
        /// </summary>
        public int RefreshPeriod { get; set; } = 300;
    }

    public class DisplaySignal : BaseModel,IDisplaySignal
    {
        public double Value
        { get => _value; set { _value = value; OnPropertyChanged(); } }
        private double _value = 0.0;


        public string SignalName
        { get => _name; set { _name = value; OnPropertyChanged(); } }
        private string _name = "";

        public string Units
        { get => _units; set { _units = value; OnPropertyChanged(); } }
        private string _units = "";
        private IMSignal _signal;

        public DisplaySignal(IMSignal signal)
        {
            Signal = signal;
        }

        public void Update()
        {
            Signal.SignalValue(out double val);
            Value = val;
        }

        public IMSignal Signal
        {
            get => _signal;
            set
            {
                _signal = value;

                _signal.GetSignalName(out string name);
                SignalName = name;

                Units = (_signal as IMTransducer).Units;

            }
        }

    }
}
