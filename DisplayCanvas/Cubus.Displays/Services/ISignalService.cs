﻿using DisplayCanvas;
using DisplayCanvas.Interfaces;
using System.Collections.Generic;

namespace Cubus.Displays.Services
{
    public interface ISignalService : IDisplayService
    {
        int RefreshPeriod { get; set; }
        List<IDisplaySignal> Signals { get; set; }

        void StartUpdating();
        void StopUpdating();
    }
}