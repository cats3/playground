﻿using DisplayCanvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cubus.Displays.Services
{
    public class DisplayInjectionService : IDisplayService
    {
        public List<Type> GetDisplays()
        {
            //SPS: Do Reflection here
            return predefinedDisplays;
        }



        private static List<Type> predefinedDisplays
        {
            get => new List<Type>()
            {
                typeof(TestDisplayModel),
            };

        }

        public void Shutdown()
        {
            //throw new NotImplementedException();
        }
    }

}
