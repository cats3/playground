﻿using Abt.Controls.SciChart;
using Abt.Controls.SciChart.Model.DataSeries;
using Abt.Controls.SciChart.Visuals.Axes;
using Abt.Controls.SciChart.Visuals.RenderableSeries;
using Cubus.Core.UI;
using Cubus.Displays.Services;
using Cubus.Displays.XY.Viewmodels;
using Cubus.Displays.XY.Views;
using DisplayCanvas;
using DisplayCanvas.Interfaces;
using Interfaces.MachineInterfaces;
using StringsHelper;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;
using sysThread = System.Threading;

namespace Cubus.Displays.XY
{
    public class XYDisplay : Display
    {
        public XYDisplay(IMOscilloscopeService oscilliscope, ISignalService signalRepository)
        {
            this.Oscilliscope = oscilliscope;
            this.SignalRepo = signalRepository;
        }

        public override string DisplayName => "XYTab";

        public override string DisplayDescription => "Displays 2 signals a chart";

        public IMOscilloscopeService Oscilliscope { get; private set; }
        public ISignalService SignalRepo { get; private set; }

        public override void PrepareUIElements()
        {
            var viewModel = new XYDisplayViewModel(this,this.Oscilliscope, this.SignalRepo);

            this.View = new XYDisplayView() { DataContext = viewModel };

            this.ConfigureView = new XYDisplayConfigureView() { DataContext = viewModel };
        }

        private List<ToolbarAction> LoadActions()
        {
            var LoadData = new ToolbarAction();
            var SaveData = new ToolbarAction();
            var ClearData = new ToolbarAction();

            return new List<ToolbarAction>();
        }

        public override void RequestAction(eAction action)
        {
            switch (action)
            {
                case eAction.Play:
                    break;
                case eAction.Pause:
                    break;
                case eAction.Reset:
                    break;
                default:
                    break;
            }
        }
    }

}
