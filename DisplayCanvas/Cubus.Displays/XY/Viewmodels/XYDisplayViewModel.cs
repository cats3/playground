﻿using Abt.Controls.SciChart;
using Abt.Controls.SciChart.Model.DataSeries;
using Abt.Controls.SciChart.Visuals.Axes;
using Abt.Controls.SciChart.Visuals.RenderableSeries;
using C3.Common.Models;
using C3.Common.WPF.Input;
using Cubus.Displays.Services;
using DisplayCanvas;
using DisplayCanvas.Interfaces;
using Interfaces.MachineInterfaces;
using StringsHelper;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Input;
using System.Windows.Media;


namespace Cubus.Displays.XY.Viewmodels
{
    public class XYDisplayViewModel : BaseModel
    {
        public XYDisplayViewModel()
        {
        }

        public XYDisplayViewModel(XYDisplay display, IMOscilloscopeService oscilliscope, ISignalService signalRepository)
        {
            this.Oscilliscope = oscilliscope;

            this.SignalRepository = signalRepository;

            #region Inti chart 
            this.XYRenderableSeries.Add(this.XYRenderSeries);

            this.XYRenderSeries.DataSeries = XYSeriesData;

            this.DrawingInformation.FifoThreshold = FifoSize;
            #endregion

            this.Display = display;
        }

        #region Services
        private ISignalService _signalRepository;
        public ISignalService SignalRepository
        {
            get => _signalRepository; set
            {
                _signalRepository = value;
                var signals = this.SignalRepository.Signals;               
                if(signals.Count > 1)
                    this.Signals.AddRange(signals);
                    this.XSignal = Signals.FirstOrDefault();
                    this.YSignal = Signals.Skip(1).FirstOrDefault();
            }
        }

        private IMOscilloscopeService _oscilliscope;
        public IMOscilloscopeService Oscilliscope { get => _oscilliscope; set => _oscilliscope = value; }
        #endregion

        #region XAxis
        public string XAxisTitle { get; set; } = "XAxis Title".Translate();

        public DoubleRange XAxisVisibleRange { get; set; }

        public AutoRange XAxisAutoRange { get; set; } = AutoRange.Always;

        public bool AllowXAxisDrag { get; set; } = true;

        public NumericAxis XAxis { get; } = new NumericAxis() { Name = "XAxis", AxisTitle = "XAxis".Translate(), AutoRange = AutoRange.Always, GrowBy = new DoubleRange() { Max = 0.01, Min = 0.01 } };
        #endregion

        #region YAxis
        public string YAxisTitle { get; set; } = "YAxisTitle".Translate();

        public AutoRange YAxisAutoRange { get; set; } = AutoRange.Always;

        public bool AllowYAxisDrag { get; set; } = true;

        public IRenderableSeries YRenderableSeries = new FastLineRenderableSeries();

        public DoubleRange YAxisVisibleRange { get; set; }

        public NumericAxis YAxis { get; } = new NumericAxis() { Name = "YAxis", AxisTitle = "YAxis".Translate(), AxisAlignment = AxisAlignment.Left, AutoRange = AutoRange.Always, GrowBy = new DoubleRange() { Max = 0.01, Min = 0.01 } };
        #endregion

        #region Series Data
        public XyDataSeries<double, double> XYSeriesData
        {
            get;
            set;
        } = new XyDataSeries<double, double>() { SeriesName = "X", FifoCapacity = FifoSize };

        public IRenderableSeries XYRenderSeries = new FastLineRenderableSeries() { };

        public ObservableCollection<IRenderableSeries> XYRenderableSeries { get; } = new ObservableCollection<IRenderableSeries>();
        #endregion

        #region Configure
        public List<IDisplaySignal> Signals { get; set; } = new List<IDisplaySignal>();

        private IDisplaySignal xSignal;

        public IDisplaySignal XSignal
        {
            get => xSignal;
            set
            {
                base.SetField(ref xSignal, value);
                this.SignalChanged(this.XAxis, this.xSignal);
            }
        }

        public void ClearSeriesData()
        {
            this.XYSeriesData.Clear();
        }

        private IDisplaySignal ySignal;

        public IDisplaySignal YSignal
        {
            get => this.ySignal;
            set
            {
                base.SetField(ref ySignal, value);

                this.SignalChanged(this.YAxis, this.ySignal);
            }
        }

        private void SignalChanged(IAxis axis, IDisplaySignal signal)
        {
            axis.AxisTitle = $"{signal.SignalName}[{signal.Units}]";

            this.GetColourForSignal(signal);

            this.XYSeriesData.SeriesName = GetSeriesName(xSignal, ySignal);

            this.ChartTitle = GetChartTitle(xSignal, ySignal);
        }

        public double _timeWindow = 1;

        public double TimeWindow
        {
            get { return _timeWindow; }
            set
            {
                base.SetField(ref this._timeWindow, value);

                this.SetFiFOValue();
            }
        }

        public String SelectedTimeBase
        {
            get { return this._selectedTimeBase.GetEnumDescription(); }
            set
            {
                TimeBase t = Enum.GetValues(typeof(TimeBase))
                .Cast<TimeBase>()
                .FirstOrDefault(v => v.GetEnumDescription() == value);

                base.SetField(ref _selectedTimeBase, t);
                this.SetFiFOValue();
            }
        }

        public List<string> TimeBaseOptions
        {
            get => Enum.GetValues(typeof(TimeBase)).Cast<TimeBase>().Select(e => e.GetEnumDescription()).ToList();
        }
        #endregion

        #region View
        public string _chartTitle;

        public string ChartTitle { get { return this._chartTitle; } set { base.SetField(ref this._chartTitle, value); } }

        public ICommand InvertXAxis
        {
            get
            {
                return new RelayCommand(action =>
                {
                    this.XAxis.FlipCoordinates = !this.XAxis.FlipCoordinates;
                });
            }

        }

        public ICommand InvertYAxis
        {

            get
            {
                return new RelayCommand(action =>
                {
                    this.YAxis.FlipCoordinates = !this.YAxis.FlipCoordinates;

                });
            }

        }

        public ICommand ShowGrid
        {

            get
            {
                return new RelayCommand(action =>
                {
                    this.YAxis.DrawMajorGridLines = !this.YAxis.DrawMajorGridLines;
                    this.XAxis.DrawMajorGridLines = !this.XAxis.DrawMajorGridLines;

                    this.YAxis.DrawMinorGridLines = !this.YAxis.DrawMinorGridLines;
                    this.XAxis.DrawMinorGridLines = !this.XAxis.DrawMinorGridLines;

                });
            }

        }

        public ICommand SwapAxis
        {

            get
            {
                return new RelayCommand(action =>
                {
                    ApplyAlignment(this.XAxis);
                    ApplyAlignment(this.YAxis);
                });
            }

        }

        #endregion

        #region Data
        public bool DataTriggerEnabled { get; set; }
        public bool RepeatDataCapture { get; set; }

        public bool SaveOnCapture { get; set; }

        public int DelayDataCaptureCycles { get; set; }

        public int CaptureDataCycles { get; set; }

        public ICommand ClearData
        {
            get
            {
                return new RelayCommand(action =>
                {
                    this.ClearSeriesData();
                });
            }

        }

        private bool _isDataCaptureEnabled = true;

        public bool IsDataCaptureEnabled
        {
            get { return _isDataCaptureEnabled; }
            set
            {
                SetField(ref _isDataCaptureEnabled, value);
            }
        }
        #endregion

        #region Scaling
        private bool _isManualScaling;
        public bool IsManualScaling
        {
            get { return _isManualScaling; }
            set { SetField(ref _isManualScaling, value); }
        }

        public ICommand AutoScaling
        {

            get
            {
                return new RelayCommand(action =>
                {
                    this.XAxis.AutoRange = AutoRange.Always;
                    this.YAxis.AutoRange = AutoRange.Always;
                    this.ScalingVisibile = this.IsManualScaling = false;
                });
            }
        }
        public ICommand ManualScaling
        {
            get
            {
                return new RelayCommand(action =>
                {
                    this.XAxis.AutoRange = AutoRange.Never;
                    this.YAxis.AutoRange = AutoRange.Never;
                    this.ScalingVisibile = this.IsManualScaling = true;
                });
            }

        }
        public ICommand ShowScaling
        {
            get
            {
                return new RelayCommand(action =>
                {
                    ScalingVisibile = !ScalingVisibile;
                });
            }
        }

        public bool _scalingVisibility;
        public bool ScalingVisibile
        {
            get { return _scalingVisibility; }
            set
            {
                SetField(ref _scalingVisibility, value);
            }
        }
        public int _scalingDecimalPlaces;
        private TimeBase _selectedTimeBase = TimeBase.Seconds;

        public int ScalingDecimalPlaces
        {
            get { return _scalingDecimalPlaces; }
            set
            {
                SetField(ref _scalingDecimalPlaces, value);
            }
        }

        #endregion

        #region Tools
        private static void ApplyAlignment(IAxis axis)
        {
            switch (axis.AxisAlignment)
            {
                case AxisAlignment.Right:
                    axis.AxisAlignment = AxisAlignment.Bottom;
                    break;
                case AxisAlignment.Bottom:
                    axis.AxisAlignment = AxisAlignment.Right;
                    break;
                case AxisAlignment.Top:
                    axis.AxisAlignment = AxisAlignment.Left;
                    break;
                case AxisAlignment.Left:
                    axis.AxisAlignment = AxisAlignment.Top;
                    break;
            }
        }

        private static void RotateClockwise(IAxis axis)
        {
            switch (axis.AxisAlignment)
            {
                case AxisAlignment.Right:
                    axis.AxisAlignment = AxisAlignment.Bottom;
                    break;
                case AxisAlignment.Bottom:
                    axis.AxisAlignment = AxisAlignment.Left;
                    break;
                case AxisAlignment.Top:
                    axis.AxisAlignment = AxisAlignment.Right;
                    break;
                case AxisAlignment.Left:
                    axis.AxisAlignment = AxisAlignment.Top;
                    break;
            }
        }

        private const int FifoSize = 1000;

        public void SetFiFOValue()
        {
            double t = 0;

            double SamplesPerSec = this.Oscilliscope.SampleFrequency;

            t = GetTimeFromInputAndBase();
            //I dont think changeing the draw time maters if we set sci chart to only show (timeWindow(s) * (samples/s)) 
            //this._drawDataTimer.Interval = Convert.ToInt32(TimeSpan.FromSeconds(t).TotalMilliseconds);
            //this.XYSeriesData.FifoCapacity =  (int)Math.Ceiling(t * SamplesPerSec);

            this.XYSeriesData.FifoCapacity = Convert.ToInt32(SamplesPerSec * t);

            this.DrawingInformation.FifoThreshold = this.XYSeriesData.FifoCapacity ?? 0;
        }

        private double GetTimeFromInputAndBase()
        {
            double t = 0.0;

            switch (_selectedTimeBase)
            {
                case TimeBase.MillSeconds:
                    t = TimeSpan.FromMilliseconds(TimeWindow).TotalSeconds;
                    break;
                case TimeBase.Seconds:
                    t = TimeWindow;
                    break;
                case TimeBase.Minutes:
                    t = TimeSpan.FromMinutes(TimeWindow).TotalSeconds;
                    break;
                default:
                    break;
            }

            return t;
        }

        #endregion

        #region PlotDrawing
        private int _drawDataInterval = 15;

        private Timer _drawDataTimer;

        public void StartDrawTimer()
        {
            if (this._drawDataTimer == null)
            {
                this._drawDataTimer = new Timer();

                this._drawDataTimer.Tick += this._drawTimer_Tick;

                this._drawDataTimer.Interval = this._drawDataInterval;
            }
            this._drawDataTimer.Start();
        }

        private void StopDrawTimer()
        {
            this._drawDataTimer?.Stop();
        }

        private async void _drawTimer_Tick(object sender, EventArgs e)
        {
            try
            {
                var samples = Convert.ToInt32(this.Oscilliscope.SampleFrequency);

                double[,] data = new double[samples, this.Signals.Count];

                int count = 0;

                var xIndex = this.XSignal.Signal.SystemSignalIndex;

                var yIndex = this.YSignal.Signal.SystemSignalIndex;

                Task.Run(() =>
                {
                    this.Oscilliscope.GetData(ref data, out count);
                }).GetAwaiter().OnCompleted(() => {                    
                    // Lock the data-series and clear / re-add new data
                    using (this.XYSeriesData.SuspendUpdates())
                    {
                        this.XYSeriesData.Clear();

                        this.XYSeriesData.AcceptsUnsortedData = true;

                        for (int s = 0; s < count; s++)
                        {

                            this.XYSeriesData.Append(data[s, xIndex], data[s, yIndex]);
                        }
                    }
                    SetDiagnosticDrawInformation();
                });
            }
            catch { }
        }

        private void SetDiagnosticDrawInformation()
        {
            this.DrawingInformation.AddDrawTime(DateTime.Now);

            this.DrawingInformation.CurrentPointCount = this.XYSeriesData.Count;
        }

        #endregion

        #region Scope
        public void StartScope()
        {
            this.Oscilliscope.ScopeStart();

            this.StartDrawTimer();
        }

        public void StopScope()
        {
            this.Oscilliscope.ScopeStop();

            this.StopDrawTimer();
        }
        #endregion

        #region Chart Interaction
        private bool _cursorEnabled;
        public bool CursorEnabled
        {
            get => _cursorEnabled;
            set => SetField(ref _cursorEnabled, value);
        }
        public ICommand ShowCursor
        {
            get
            {
                return new RelayCommand(action =>
                {
                    CursorEnabled = !CursorEnabled;
                });
            }

        }
        #endregion

        #region Display Specific
        public SciChartDrawInformation DrawingInformation { get; set; } = new SciChartDrawInformation();
        public XYDisplay Display { get; private set; }

        private List<ToolbarAction> LoadActions()
        {
            var returnList = new List<ToolbarAction>();

            var ClearData = new ToolbarAction(x => { });

            var StartScope = new ToolbarAction(x => { });

            var StopScope = new ToolbarAction(x => { });

            returnList.Add(ClearData);

            returnList.Add(StartScope);

            returnList.Add(StopScope);

            return new List<ToolbarAction>();
        }
        #endregion

        #region GetMethods - Axis Title Chart Title
        private string GetChartTitle(IDisplaySignal xSignal, IDisplaySignal ySignal)
        {
            return $"{xSignal?.SignalName}[{xSignal?.Units}] {"vs".Translate()} {ySignal?.SignalName}[{ySignal?.Units}]";
        }

        private string GetSeriesName(IDisplaySignal xSignal, IDisplaySignal ySignal)
        {
            return $"{xSignal?.SignalName} vs {ySignal?.SignalName}";
        }

        private void GetColourForSignal(IDisplaySignal signal)
        {
            //Frig For Demo
            this.XYRenderSeries.SeriesColor = signal.SignalName.Contains("Disp") ? Color.FromRgb(255, 0, 0) : Color.FromRgb(0, 0, 255);
        }
        #endregion


    }
}
