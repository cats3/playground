﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cubus.Core.UI;
using C3.Common.Models;

namespace Cubus.Displays.XY.Viewmodels
{
    public class SciChartDrawInformation : BaseModel
    {

        private int _fifoThreshold;
        public int FifoThreshold { get => _fifoThreshold; set => SetField(ref _fifoThreshold, value); }

        private double _currentPointCOunt;
        public double CurrentPointCount { get => _currentPointCOunt; set => SetField(ref _currentPointCOunt, value); }

        private DateTime _previousDrawTime;
        private double avergeCount = 100;
        private double currentAverge;
        public void AddDrawTime(DateTime time)
        {
            if (_previousDrawTime == null)
            {
                this._previousDrawTime = time;
                return;
            }

            TimeSpan diff = time.Subtract(_previousDrawTime);

            this.TimeToCompleteDraw = Math.Round(Convert.ToDouble(diff.Milliseconds), 2);

            this.AvergeDrawTime = Math.Round(approxRollingAverage(diff.Milliseconds), 2);

            this._previousDrawTime = time;
        }

        double approxRollingAverage(double new_sample)
        {

            currentAverge -= currentAverge / avergeCount;
            currentAverge += new_sample / avergeCount;

            return currentAverge;
        }

        private double _timeToComplete;

        public double TimeToCompleteDraw { get => this._timeToComplete; set { SetField(ref _timeToComplete, value); } }

        private double _avergeDrawTime;

        public double AvergeDrawTime { get => this._avergeDrawTime; set { SetField(ref _avergeDrawTime, value); } }


        IEnumerable<TimeSpan> CalculateDeltas(IEnumerable<DateTime> times)
        {
            var time_deltas = new List<TimeSpan>();

            DateTime prev = times.First();
            foreach (var t in times.Skip(1))
            {
                time_deltas.Add(t - prev);
                prev = t;
            }

            return time_deltas;
        }
    }

}
