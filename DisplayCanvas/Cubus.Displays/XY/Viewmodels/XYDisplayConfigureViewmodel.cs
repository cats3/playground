﻿using C3.Common.Models;
using C3.Common.WPF.Input;
using Cubus.Displays.Services;
using Interfaces.MachineInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cubus.Displays.XY.Viewmodels
{
    public class XYDisplayConfigureViewModel : BaseModel
    {
        public XYDisplayConfigureViewModel()
        {


            this.XSignal = XSignals.FirstOrDefault();
        }

        #region Configure
        public List<DisplaySignal> YSignals { get; set; } = new List<DisplaySignal>();
        public List<DisplaySignal> XSignals { get; set; } = new List<DisplaySignal>();

        private DisplaySignal xSignal;
        public DisplaySignal XSignal
        {
            get => xSignal;
            set { SetField(ref xSignal, value); }
        }

        private DisplaySignal ySignal;
        public DisplaySignal YSignal
        {
            get => ySignal;
            set { SetField(ref ySignal, value); }
        }

        public int TimeWindow { get; set; } = 5;
        #endregion

        #region View
        public bool InvertXAxis { get; set; }
        public bool InvertYAxis { get; set; }
        public bool ShowGrid { get; set; }
        public bool SwapAxis { get; set; }
        #endregion

        #region Data capture
        public bool DataTriggerEnabled { get; set; }
        public bool RepeatDataCapture { get; set; }
        public bool SaveOnCapture { get; set; }

        public int DelayDataCaptureCycles { get; set; }
        public int CaptureDataCycles { get; set; }

        #endregion

        #region Scaling
        public bool AutoScaling { get; set; }
        public bool ManualScaling { get; set; }
        public bool ShowScaling { get; set; }
        #endregion

    }
}
