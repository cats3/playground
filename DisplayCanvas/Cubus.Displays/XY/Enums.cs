﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cubus.Displays.XY
{
    public enum TimeBase
    {
        [Description("[ms]")]
        MillSeconds = 0,
        [Description("[s]")]
        Seconds = 1,
        [Description("[min]")]
        Minutes = 2,
        [Description("[cycles]")]
        Cycles = 3
    }   
}
