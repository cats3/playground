﻿using Cubus.Displays.Services;
using Cubus.Displays.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cubus.Displays.ModelViews
{
    /// <summary>
    /// Interaction logic for DigitalDisplayListView.xaml
    /// </summary>
    public partial class DigitalDisplayListView : UserControl
    {
        public DigitalDisplayListView()
        {
            InitializeComponent();
            this.DataContextChanged += DigitalDisplayListView_DataContextChanged;

        }

        public DigitalDisplayList Model;

       

        

        private void DigitalDisplayListView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (Model == null)
            {
                Model = e.NewValue as DigitalDisplayList;

                Model.PropertyChanged += Model_PropertyChanged;

            }

           


        }

        private void Model_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Model.EnabledSignals):
                    if (grd.Children.Count < Model.EnabledSignals.Count) //ADDED
                    {
                        Model.EnabledSignals.Where(s => grd.Children.OfType<DigitalDisplay>().Any(d => (d as DigitalDisplay).Signal == s)).ToList().ForEach(signal =>
                        {
                            if (Model.Orientation == Orientation.Horizontal)
                            {
                                var col = grd.ColumnDefinitions.Count;
                                grd.ColumnDefinitions.Add(new ColumnDefinition());
                                var add = new DigitalDisplay() { DataContext = signal };
                                Grid.SetColumn(add, col);
                                grd.Children.Add(add);
                            }
                            else
                            {
                                var row = grd.RowDefinitions.Count;
                                grd.RowDefinitions.Add(new RowDefinition());
                                var add = new DigitalDisplay() { DataContext = signal };
                                Grid.SetRow(add, row);
                                grd.Children.Add(add);
                            }
                        });
                    }
                    else if (grd.Children.Count > Model.EnabledSignals.Count) //REMOVED
                    {
                        grd.Children.OfType<DigitalDisplay>().Where(d => !Model.EnabledSignals.Contains(d.Signal)).ToList().ForEach(c =>
                        { grd.Children.Remove(c); } );

                        var cnt = 0;

                        grd.Children.OfType<DigitalDisplay>().ToList().ForEach(dd => //SPS: May cause weird reordering.
                        {
                            if (Model.Orientation == Orientation.Horizontal)
                            {
                                Grid.SetColumn(dd, cnt++); 
                            }
                            else
                            {
                                Grid.SetRow(dd, cnt++);
                            }
                        });

                    }
                    break;
                default:
                    break;
            }
        }
    }
}
