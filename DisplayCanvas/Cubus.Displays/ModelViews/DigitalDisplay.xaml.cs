﻿using Cubus.Displays.Services;
using Cubus.Displays.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cubus.Displays.ModelViews
{
    /// <summary>
    /// Interaction logic for DigitalDisplay.xaml
    /// </summary>
    public partial class DigitalDisplay : UserControl
    {
        public DigitalDisplay()
        {
            InitializeComponent();
            this.DataContextChanged += DigitalDisplay_DataContextChanged;
        }

        public DisplaySignal Signal { get; set; }



        private void DigitalDisplay_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (Signal == null)
            {
                Signal = e.NewValue as DisplaySignal;

                Signal.PropertyChanged += Signal_PropertyChanged;
            }
        }

        private void Signal_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case nameof(Signal.Value):
                    if (!Owner.Paused)
                    {
                        //SPS: Update value manually.
                    }
                    break;
                default:
                    break;
            }
        }

        public DigitalDisplayList Owner
        {
            get; set;
        }

       

    }
}
