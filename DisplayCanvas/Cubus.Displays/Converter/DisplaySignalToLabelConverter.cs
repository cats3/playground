﻿using DisplayCanvas.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Data;

namespace Cubus.Displays.Converter
{
    public class DisplaySignalToLabelConverter : IValueConverter
    {
        public DisplaySignalToLabelConverter()
        {

        }
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if(value is IDisplaySignal)
            {
                var sig = value as IDisplaySignal;

                return $"{sig.SignalName} [{sig.Units}]";
            }

            return "";
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
