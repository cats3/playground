﻿using C3.Common.WPF.Input;
using Cubus.Displays.ModelViews;
using Cubus.Displays.Services;
using DisplayCanvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace Cubus.Displays.ViewModels
{
    public class DigitalDisplayList : Display
    {
        public DigitalDisplayList()
        {
            Service = C3.Common.Injection.Services.GetService<SignalService>() as SignalService;
            Service.StartUpdating();
            View = new DigitalDisplayListView() { DataContext = this };
        }

        public bool Paused = false;

        private List<DisplaySignal> _enabled = new List<DisplaySignal>();

        public List<DisplaySignal> EnabledSignals { get => _enabled; set { _enabled = value; OnPropertyChanged(); } }


        public Orientation Orientation
        { get; set; } = Orientation.Vertical;


        public SignalService Service { get; internal set; }

        public override List<IDisplayParameter> Parameters
        { get => base.Parameters; set => base.Parameters = value; }


        public override string DisplayName => throw new NotImplementedException();

        public override string DisplayDescription => throw new NotImplementedException();


        public override void RequestAction(eAction action)
        {
            switch (action)
            {
                case eAction.Play:
                    Paused = false;
                    break;
                case eAction.Pause:
                    Paused = true;
                    break;
                case eAction.Reset:
                    break;
                default:
                    break;
            }
        }

        public override void PrepareUIElements()
        {
            // Content = null;
            // SettingsControl = null;
        }
    }



}
