﻿using DisplayCanvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisplayCanvas
{
    public class TestDisplayModel : Display
    {
        public override string DisplayName => "Dummy Display";

        public override string DisplayDescription => "";

        //public override List<ToolbarAction> ToolBarActions => throw new NotImplementedException();

      

        public override void PrepareUIElements()
        {
            CustomActions.Add(new ToolbarAction(o => { }, null, "Test Button"));
            //Content = null;
            //SettingsControl = null;
        }

        public override void RequestAction(eAction action)
        {
            //throw new NotImplementedException();
        }
    }
}
