﻿using DisplayCanvas.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml.Serialization;

namespace DisplayCanvas.Services
{
    public class CanvasManagementService : CanvasBaseViewModel, IManagementService
    {
        public CanvasManagementService()
        {
            CurrentState = new CanvasSession();
}
        public CanvasSession CurrentState
        { get => GetValue<CanvasSession>(); internal set => SetValue(value); } 
        public List<Display> LoadedDisplayTypes { get; set; } = new List<Display>();

        public void LoadStateFromFile(string path)
        {
            SaveState();
            CurrentState.Groups.ForEach(g => g.CloseWindow());

            CurrentState = CanvasSession.LoadFrom(path);

            CurrentState.Groups.Where(g => g.IsWindowed).ToList().ForEach(g => g.CreateNewWindow());
        }

        public FrameworkElement MainHost
        {
            get;set;
        }



        public void SaveState(string path = null)
        {
            if (path != null)
                CurrentState.Path = path;
            CurrentState?.Save();
        }

        public void Shutdown()
        {
            SaveState();
        }

       




    }

    public interface IManagementService : IDisplayService
    {

    }

    [Serializable]
    public class CanvasSession : SettingsData, ISaveable
    {
        public CanvasSession()
        {
            Groups = new List<CanvasGroup>();
        }
        [XmlIgnore]
        public SettingsData Settings { get; set; } = new SettingsData();

        public List<CanvasGroup> Groups
        {
            get => GetValue<List<CanvasGroup>>();
            set => SetValue(value);
        }

        public string ElementName => GetType().FullName;

        public T GetValue<T>([CallerMemberName] string propertyName = "")
        {
            if (!Settings.Entries.Any(p => p.EntryName == propertyName))
                Settings.AddEntry(default(T), propertyName);

            return (T)Settings[propertyName].Instance;
        }

        /// <summary>
        /// Sets the SettingsData entry for the Member in question, and calles OnPropertyChanged().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="propertyName"></param>
        public void SetValue<T>(T value, [CallerMemberName] string propertyName = "")
        {
            if (!Settings.Entries.Any(p => p.EntryName == propertyName))
                Settings.AddEntry(value, propertyName);
            else
                Settings[propertyName].Instance = value;

        }

        public new static CanvasSession LoadFrom(string path)
        {
            return CanvasSession.LoadFromXML(File.ReadAllText(path));
        }

        public new static CanvasSession LoadFromXML(string xmlstring)
        {

            //            List<Type> extraTypes = new List<Type>(CanvasManager.LoadedDisplayTypes.Select(d => d.GetType())){ typeof(SplitGridModel),
            //typeof(DisplayHostBaseViewModel), typeof(SplitterCanvasModel), typeof(ObservableCollection<SplitterCanvasModel>), typeof(CanvasGroup) };

            List<Type> extraTypes = new List<Type>(((CanvasManagementService)C3.Common.Injection.Services.GetService<IManagementService>())
            .LoadedDisplayTypes.Select(d => d.GetType()));


            MemoryStream memstream = new MemoryStream();
            var byts = Encoding.ASCII.GetBytes(xmlstring);
            memstream.Write(byts, 0, byts.Length);
            memstream.Position = 0;
            XmlSerializer xml = new XmlSerializer(typeof(CanvasSession), extraTypes.ToArray());//, extraTypes.ToArray());

            CanvasSession sd = (CanvasSession)xml.Deserialize(memstream);



            // sd.Entries = sd.SerialisableEntries.Cast<ISettingsEntry>().ToList();

            return sd;
        }

        public override string GetXML()
        {


            List<Type> extraTypes = new List<Type>(((CanvasManagementService)C3.Common.Injection.Services.GetService<IManagementService>())
                .LoadedDisplayTypes.Select(d => d.GetType()));
          
            

            XmlSerializer xml = new XmlSerializer(typeof(CanvasSession), extraTypes.ToArray());//, extraTypes.ToArray());


            MemoryStream memstream = new MemoryStream();

            xml.Serialize(memstream, this);

            memstream.Position = 0;
            return new StreamReader(memstream).ReadToEnd();
        }
    }


}
