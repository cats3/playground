﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisplayCanvas.Interfaces
{
    public interface IDisplaySignalRepository
    {
        List<IDisplaySignal> GetSignals();
    }
}
