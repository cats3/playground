﻿using Interfaces.MachineInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisplayCanvas.Interfaces
{
    public interface IDisplaySignal
    {
        string SignalName { get; set; }

        double Value { get; set; }

        string Units { get; set; }

        IMSignal Signal { get; }

        void Update();
    }
}
