﻿using DisplayCanvas.Interfaces;
using DisplayCanvas.UI.Canvas;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Windows;
using System.Xml;

namespace DisplayCanvas
{
    public interface IDisplay : IDisplayable, IToolbarParticipant
    {
        string DisplayName { get; }

        string DisplayDescription { get; }

        

        UIElement View { get; set; }
        //int CurrentColumn { get; set; }
        //int CurrentRow { get; set; }
        //eExpandMode ExpandMode { get; }
        string Label { get; set; }
        List<IDisplayParameter> Parameters { get; set; }
        UIElement ConfigureView { get; set; }

        List<ToolbarAction> CustomActions { get; }

        //event EventHandler<eExpandMode> ExpandModeChanged;

        SettingsData Settings
        { get; set; }

        void _internal_PrepareUIElements();
    }

}