﻿namespace DisplayCanvas
{
    public interface ISettings
    {
        string GetXML();
    }


    public interface ISettingsEntry
    {
        string EntryName { get; set; }
        object Instance { get; set; }

        string TypeName { get; set; }

    }



}