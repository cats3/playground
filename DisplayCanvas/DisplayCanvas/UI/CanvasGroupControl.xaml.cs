﻿using DisplayCanvas.UI.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DisplayCanvas
{
    /// <summary>
    /// Interaction logic for CanvasGroupControl.xaml
    /// </summary>
    public partial class CanvasGroupControl : UserControl
    {
        public CanvasGroupControl()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            (DataContext as CanvasGroup)?.RemoveCanvas(((FrameworkElement)sender).DataContext as SplitterCanvasModel);   
        }

       

        public void SetSelectedIndex(int idx)
        {
            tabCtrl.SelectedIndex = idx;
        }
    }
}
