﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DisplayCanvas.UI
{
    /// <summary>
    /// Interaction logic for DisplayCanvasMain.xaml
    /// </summary>
    public partial class DisplayCanvasMain : UserControl
    {
        public DisplayCanvasMain()
        {
            InitializeComponent();
        }


        public List<Display> Displays
        {
            set => toolbox.Displays = value;
        }
    }
}
