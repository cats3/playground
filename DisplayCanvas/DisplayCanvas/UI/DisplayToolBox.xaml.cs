﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DisplayCanvas.UI
{
    /// <summary>
    /// Interaction logic for DisplayToolBox.xaml
    /// </summary>
    public partial class DisplayToolBox : UserControl, INotifyPropertyChanged
    {
        public static DependencyProperty DisplaysProperty =
         DependencyProperty.Register("Displays", typeof(IEnumerable), typeof(DisplayToolBox),
         new PropertyMetadata());


        public DisplayToolBox()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        public List<Display> Displays
        {
            get => (List<Display>)GetValue(DisplaysProperty);
            set { SetValue(DisplaysProperty, value); OnPropertyChanged(); }
        }

        public void OnPropertyChanged([CallerMemberName] string propertyName = "") => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));

        public event PropertyChangedEventHandler PropertyChanged;

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            SplitterCanvas.MouseTool = eTool.DropDisplay;
            SplitterCanvas.DroppedType = (sender as ToggleButton).DataContext.GetType();
        }

        private void ToggleButton_Unchecked(object sender, RoutedEventArgs e)
        {
            SplitterCanvas.MouseTool = eTool.Pointer;
        }
    }
}
