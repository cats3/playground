﻿using C3.Common.WPF.Input;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DisplayCanvas
{
    /// <summary>
    /// Interaction logic for DisplayParameterControl.xaml
    /// </summary>
    public partial class DisplayParameterControl : UserControl, INotifyPropertyChanged
    {
        public DisplayParameterControl()
        {
            InitializeComponent();
            this.DataContext = this;
        }

        public IDisplayParameter Parameter { get => myParameter; set
            {
                myParameter = value;
                beuatifulMethod();
                onPropChange();
            } }
        private IDisplayParameter myParameter = null;

        public event PropertyChangedEventHandler PropertyChanged;

        public string ParameterName
        { get => myParameter?.Name?? "Not Set";
          
        }

        public string ValueAsText
        {
            get => myParameter?.Value.ToString() ?? "Not Set";
            set
            {
                myParameter.Value = value;
            }
        }


        public List<string> ValueAsList
        { get => myParameter.DisplayType == eDisplayType.List ? ((System.Collections.IEnumerable)myParameter?.Value)?.Cast<string>()?.ToList() ?? new List<string>() { "Not Set" } : new List<string>(); }

        private void beuatifulMethod()
        {
            switch (myParameter.DisplayType)
            {
                case eDisplayType.Numeric:
                case eDisplayType.Text:
                    bdrList.Visibility = Visibility.Collapsed;
                    bdrTextNumeric.Visibility = Visibility.Visible;
                    onPropChange(nameof(ValueAsText));
                    onPropChange(nameof(ParameterName));
                    break;
                case eDisplayType.List:
                    bdrList.Visibility = Visibility.Visible;
                    bdrTextNumeric.Visibility = Visibility.Collapsed;
                    onPropChange(nameof(ValueAsList));
                    onPropChange(nameof(ParameterName));
                    break;
                case eDisplayType.Unknown:
                    bdrList.Visibility = Visibility.Collapsed;
                    bdrTextNumeric.Visibility = Visibility.Visible;
                    break;
                default:
                    break;
            }

        }
        private void onPropChange([CallerMemberName]string propName = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propName));

        }

        private void tbName_TextChanged(object sender, TextChangedEventArgs e)
        {
            myParameter.Value = tbName.Text;
        }

        private void cbList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //myParameter.SetValue(tbName.Text); //SPS: Implement Selected item on Parameters.
        }
    }
}
