﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DisplayCanvas
{
    /// <summary>
    /// Interaction logic for DisplayControl.xaml
    /// </summary>
    public partial class DisplayControl : UserControl, INotifyPropertyChanged
    {
        public static DependencyProperty HostedDisplayProperty =
          DependencyProperty.Register("HostedDisplay", typeof(Display), typeof(DisplayControl),
          new PropertyMetadata());

        public DisplayControl()
        {
            InitializeComponent();
        }

        public Display HostedDisplay
        { get => GetValue(HostedDisplayProperty)as Display;
            set
            {
                SetValue(HostedDisplayProperty, value);
                PropChange();

            }
        }
        public bool MouseIsOverDragArea { get => tbName.IsMouseDirectlyOver; }
        public bool Dragging { get; internal set; }
        public bool Resizing { get; internal set; }
        public bool IsOverlayDisplayControl  { get; set; } = false;

        private void toggleSettings_Checked(object sender, RoutedEventArgs e)
        {
            bdrSettingsOverlay.Visibility = Visibility.Visible;
        }

        private void toggleSettings_Unchecked(object sender, RoutedEventArgs e)
        {
            bdrSettingsOverlay.Visibility = Visibility.Collapsed;
        }

        private void tbName_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Dragging = true;
        }

        private void UserControl_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Resizing = true;
            e.Handled = false;
        }

        private void btnExpand_Click(object sender, RoutedEventArgs e)
        {
            ExpandedViewRequested?.Invoke(this, this.HostedDisplay);
        }

        public static event EventHandler<Display> ExpandedViewRequested;
        public event PropertyChangedEventHandler PropertyChanged;

        private void PropChange([CallerMemberName] string propname = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propname));
        }
    }
}
