﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DisplayCanvas.UI.Canvas
{
    /// <summary>
    /// Interaction logic for SplitGridView.xaml
    /// </summary>
    public partial class SplitGridView : UserControl
    {
        public static DependencyProperty
            ItemsSourceProperty =
         DependencyProperty.Register("ItemSource", typeof(IEnumerable), typeof(SplitGridView),
         new PropertyMetadata(new List<object>(), OnItemsSourcePropertyChanged)),

             ItemTemplateProperty =
         DependencyProperty.Register("ItemTemplate", typeof(DataTemplate), typeof(SplitGridView),
         new PropertyMetadata()),

            ItemTemplateSelectorProperty =
         DependencyProperty.Register("ItemTemplateSelector", typeof(DataTemplateSelector), typeof(SplitGridView),
         new PropertyMetadata());

        public IEnumerable ItemSource
        {
            get => GetValue(ItemsSourceProperty) as IEnumerable;
            set => SetValue(ItemsSourceProperty, value);
        }

        public DataTemplate ItemTemplate
        {
            get => GetValue(ItemTemplateProperty) as DataTemplate;
            set => SetValue(ItemTemplateProperty, value);
        }

        public DataTemplateSelector ItemTemplateSelector
        {
            get => GetValue(ItemTemplateSelectorProperty) as DataTemplateSelector;
            set => SetValue(ItemTemplateSelectorProperty, value);
        }

        private static void OnItemsSourcePropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var gridView = d as SplitGridView;

            var list = e.NewValue as IEnumerable;
            gridView?.UpdateItemsSource(list);
        }

        public SplitGridView()
        {
            InitializeComponent();
        }

        public bool IsMain
        { get; set; } = false;

        ~SplitGridView()
        {
            //var notifyCollection = ItemSource as INotifyCollectionChanged;

            //if (notifyCollection != null)
            //{
            //    notifyCollection.CollectionChanged -= NotifyCollection_CollectionChanged;
            //}
        }

        protected void UpdateItemsSource(IEnumerable list)
        {
            if (list == null)
                list = new List<IDisplayable>();


            if (ItemSource is INotifyCollectionChanged notifyCollection)
            {
                notifyCollection.CollectionChanged -= NotifyCollection_CollectionChanged;
            }

            notifyCollection = list as INotifyCollectionChanged;
            // subscribe to the new
            if(notifyCollection != null)
            {
                notifyCollection.CollectionChanged += NotifyCollection_CollectionChanged;
            }


            // Setup items in grid
            SetupItems(list);


        }

        private void SetupItems(IEnumerable list)
        {
            //Clear Grid
            root.Children
                .OfType<ContentPresenter>()
                .ToList()
                .ForEach(root.Children.Remove);

            list.OfType<object>()
                .Select(CreateContentItem)
                .ToList()
                .ForEach(c => root.Children.Add(c));
        }

        private void NotifyCollection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    for (int idx = 0; idx < e.NewItems.Count; idx++)
                    {
                        var newItem = e.NewItems[idx];

                        var presenter = CreateContentItem(newItem);
                        root.Children.Add(presenter);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    for (int idx = 0; idx < e.OldItems.Count; idx++)
                    {
                        var oldItem = e.OldItems[idx];

                        var presenter = root.Children
                            .OfType<ContentPresenter>()
                            .FirstOrDefault(c => c.Content == oldItem);

                        root.Children.Remove(presenter);
                    }
                    break;
                case NotifyCollectionChangedAction.Replace:
                    for (int idx = 0; idx < e.OldItems.Count; idx++)
                    {
                        if (idx >= e.NewItems.Count)
                            continue;

                        var oldItem = e.OldItems[idx];

                        var newItem = e.NewItems[idx];

                        var presenter = root.Children
                            .OfType<ContentPresenter>()
                            .FirstOrDefault(c => c.Content == oldItem);

                        if (presenter != null)
                        {
                            presenter.Content = newItem;
                        }
                    }
                    break;
                case NotifyCollectionChangedAction.Move:
                    break;
                case NotifyCollectionChangedAction.Reset:
                    

                    SetupItems(ItemSource);

                    break;
                default:
                    break;
            }
        }

        private ContentPresenter CreateContentItem(object item)
        {
            var presenter = new ContentPresenter()
            {
                Content = item,
                ContentTemplate = ItemTemplate,
                ContentTemplateSelector = ItemTemplateSelector
            };

            //Row column bindings
            if (item is IDisplayable gridItem)
            {
                presenter.SetBinding(Grid.RowProperty, new Binding(nameof(gridItem.Row)) { Source = gridItem });
                presenter.SetBinding(Grid.ColumnProperty, new Binding(nameof(gridItem.Column)) { Source = gridItem });
            }

            return presenter;
        }
    }
}
