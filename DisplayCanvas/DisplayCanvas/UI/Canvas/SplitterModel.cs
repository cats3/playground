﻿using C3.Common.WPF.Input;
using DisplayCanvas.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisplayCanvas.UI.Canvas
{
    public class SplitterModel : CanvasBaseViewModel
    {
        private double _opacity = 0;
        private bool _isEnabled = false;

        public double Opacity { get => GetValue<double>(); set { SetValue(value); OnPropertyChanged(); } }
        public bool IsEnabled { get => GetValue<bool>(); set { SetValue(value); OnPropertyChanged(); } }
    }

    public class HorizontalSplitterModel : SplitterModel
    { }

    public class VerticalSplitterModel : SplitterModel
    { }
}
