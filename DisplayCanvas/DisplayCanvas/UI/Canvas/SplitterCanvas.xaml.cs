﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DisplayCanvas
{
    /// <summary>
    /// Interaction logic for SplitterCanvas.xaml
    /// </summary>
    public partial class SplitterCanvas : UserControl
    {
        public SplitterCanvas()
        {
            InitializeComponent();
        }

        public static eTool MouseTool
        { get;
            set; } = eTool.Pointer;
        public static Type DroppedType { get; internal set; }

        public static Display DroppedDisplay { get; internal set; }

        //private void UserControl_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        //{
        //    sgMain.Model = e.NewValue as SplitterCanvasModel;
        //}
    }

    public enum eTool
    {
        Pointer,
        VerticalSplit,
        HorizontalSpilt,
        Delete,
        DropDisplay,
    }

}
