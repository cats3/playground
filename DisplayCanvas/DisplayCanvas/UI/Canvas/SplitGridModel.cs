﻿using C3.Common.WPF.Input;
using DisplayCanvas.Classes;
using DisplayCanvas.UI.Canvas;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Xml.Serialization;

namespace DisplayCanvas.UI.Canvas
{
    [Serializable]
    [XmlInclude(typeof(CanvasBaseViewModel))]
    public class SplitGridModel : CanvasBaseViewModel, IDisplayable
    {
        public SplitGridModel()
        {
            Children = new ObservableCollection<CanvasBaseViewModel>();
            VerticalSplitter = new VerticalSplitterModel();
            HorizontalSplitter = new HorizontalSplitterModel();
        }

        ~SplitGridModel()
        {
            //Children.CollectionChanged -= Children_CollectionChanged;
        }

        //private void Children_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        //{
        //    OnPropertyChanged(nameof(VisualChildren));
        //}

        public void FromSplitGridModel(SplitGridModel model)
        {
            this.Children.Clear();// = new ObservableCollection<BaseViewModel>(model.Children.ToList());
            model.Children.ToList().ForEach( vm =>
            {
                if(vm is SplitGridModel)
                {
                    (vm as SplitGridModel).Parent = this;
                }
                this.Children.Add(vm);
            });
            this.Orientation = model.Orientation;
            this.Row1 = model.Row1;
            this.Row2 = model.Row2;
            this.Column1 = model.Column1;
            this.Column2 = model.Column2;
            //Children = new ObservableCollection<BaseViewModel>(Children);
            OnPropertyChanged(nameof(Children));
        }

        public eSplit Orientation
        {
            get => GetValue<eSplit>();
            set
            {
                SetValue(value);
                OnPropertyChanged();
                switch (Orientation)
                {

                    case eSplit.Vertical:
                        //Row2 = new GridLength(0);
                        Column2 = 0;// new GridLength(0, GridUnitType.Star);
                        break;
                    case eSplit.Horizontal:
                        Row2 = 0; // new GridLength(0, GridUnitType.Star);
                        //Column2 = new GridLength(0);
                        break;

                    case eSplit.NotSet:
                        Row1 = 1;// new GridLength(1, GridUnitType.Star);
                        Column1 = 1;// new GridLength(1, GridUnitType.Star);
                        Row2 = 0;//new GridLength(0, GridUnitType.Star);
                        Column2 = 0;// new GridLength(0, GridUnitType.Star);

                        

                        break;

                    default:
                        break;
                }

                HorizontalSplitter.Opacity = Orientation == eSplit.Horizontal ? 1 : 0;
                HorizontalSplitter.IsEnabled = Orientation == eSplit.Horizontal;
                VerticalSplitter.Opacity = Orientation == eSplit.Vertical ? 1 : 0;
                VerticalSplitter.IsEnabled = Orientation == eSplit.Vertical;
            }
        }

        private double _row1 = 1;// new GridLength(1, GridUnitType.Star);
        private double _row2 = 0;// new GridLength(0, GridUnitType.Star);
        private double _column1 = 1;// new GridLength(1, GridUnitType.Star);
        private double _column2 = 0;// new GridLength(0, GridUnitType.Star);

       
        public VerticalSplitterModel VerticalSplitter { get => GetValue<VerticalSplitterModel>(); set => SetValue(value); }
        public HorizontalSplitterModel HorizontalSplitter { get => GetValue<HorizontalSplitterModel>(); set => SetValue(value); }


        [XmlIgnore]
        public SplitGridModel Parent
        { get; set; }


        public ObservableCollection<CanvasBaseViewModel> Children
        { get => GetValue<ObservableCollection<CanvasBaseViewModel>>(); set => SetValue(value); }


        public double Row1 { get => _row1; set { _row1 = value; OnPropertyChanged(); } }
        public double Row2 { get => _row2; set { _row2 = value; OnPropertyChanged(); } }
        public double Column1 { get => _column1; set { _column1 = value; OnPropertyChanged(); } }
        public double Column2 { get => _column2; set { _column2 = value; OnPropertyChanged(); } }

        [XmlIgnore]
        public double PanelX
        {
            get => GetValue<double>(); 
            set
            {
                if (value.Equals(GetValue<double>())) return;
                SetValue(value);
                OnPropertyChanged();
            }
        }
        [XmlIgnore]
        public double PanelY
        {
            get => GetValue<double>();
            set
            {
                if (value.Equals(GetValue<double>())) return;
                SetValue(value);
                OnPropertyChanged();
            }
        }

        private Size _gridSize;
        [XmlIgnore]
        public Size GridSize
        {
            get => GetValue<Size>();
            set
            {
                if (value.Equals(GetValue<Size>())) return;
                SetValue(value);
                OnPropertyChanged();
            }
        }


        public int Row
        {
            get => GetValue<int>();
            set
            {
                SetValue(value);
                OnPropertyChanged();
            }
        }

        public int Column
        {
            get => GetValue<int>();
            set
            {
                SetValue(value);
                OnPropertyChanged();
            }
        }

        private SplitGridModel GetNewChildGrid()
        {
            return new SplitGridModel()
            {
                Parent = this
            };
        }

        private void InsertSplit(eSplit split, Point posRelativeToThis)
        {
            if (Orientation != eSplit.NotSet)
                return;

            double fraction = 0.0;

            switch (split)
            {
                case eSplit.NotSet:
                    return;
                case eSplit.Vertical:
                    Orientation = eSplit.Vertical;
                    fraction = posRelativeToThis.X / GridSize.Width;
                    Column1 = fraction;
                    Column2 = 1 - fraction;

                    var g1v = GetNewChildGrid();
                    g1v.Column = 0;
                    Children.Add(g1v);

                    var g2v = GetNewChildGrid();
                    g2v.Column = 1;
                    Children.Add(g2v);


                    Display disp = Children.OfType<Display>().FirstOrDefault();

                    if (disp != null)
                    {
                        this.Children.Remove(disp);
                        g1v.Children.Add(disp);
                    }
                    break;
                case eSplit.Horizontal:
                    Orientation = eSplit.Horizontal;
                    fraction = posRelativeToThis.Y / GridSize.Height;
                    //SPS: Snapping fraction = Math.Round(fraction, 1);
                    Row1 = fraction;//new GridLength(fraction, GridUnitType.Star);
                    Row2 = 1 - fraction;// new GridLength(1 - fraction, GridUnitType.Star);

                    var g1h = GetNewChildGrid();
                    g1h.Row = 0;
                    Children.Add(g1h);

                    var g2h = GetNewChildGrid();
                    g2h.Row = 1;
                    Children.Add(g2h);


                    Display dispH = Children.OfType<Display>().FirstOrDefault();

                    if (dispH != null)
                    {
                        this.Children.Remove(dispH);
                        g1h.Children.Add(dispH);
                    }

                    break;
                default:
                    break;
            }
        }


        private Point GetMousePosition()
        {
            return new Point(PanelX, PanelY);
        }

        public (int row, int col) GetRowColFromMouse()
        {
            var row = 0;
            var col = 0;

            // rows

            var row1perc = Row1 / (Row1 + Row2);
            var row1Height = GridSize.Height * row1perc;
            row = PanelY > row1Height ? 1 : 0;

            var col1perc = Column1 / (Column1 + Column2);
            var col1Width = GridSize.Width * col1perc;
            col = PanelX > col1Width ? 1 : 0;

            return (row, col);

        }

        internal T GetSelectedChild<T>() where T : IDisplayable
        {
            return Children.OfType<T>().FirstOrDefault(d => Orientation == eSplit.Vertical? d.Column == GetRowColFromMouse().col : d.Row == GetRowColFromMouse().row);
        }

       
        [XmlIgnore]
        public RelayCommand MouseLeftDownCommand => new RelayCommand(o =>
        {
            switch (SplitterCanvas.MouseTool)
            {
                case eTool.Pointer:

                    return;
                case eTool.VerticalSplit:
                    if (Orientation == eSplit.NotSet)
                    {

                        InsertSplit(eSplit.Vertical, GetMousePosition());
                    }

                    break;
                case eTool.HorizontalSpilt:
                    if (Orientation == eSplit.NotSet)
                    {
                        InsertSplit(eSplit.Horizontal, GetMousePosition());
                    }

                    break;
                case eTool.Delete:

                    Display selectedDisp = null;

                    if (Children.OfType<Display>().Count() > 0 && (selectedDisp = GetSelectedChild<Display>()) != null)//Remove Display
                    {

                        Children.Remove(selectedDisp);
                        //CanvasModel.Displays.Remove(disp.HostedDisplay);
                    }
                    else if (Children.OfType<Display>().Count() < 1)//delete cell
                    {
                        if (IsMouseOverChild<SplitGridModel>())
                            return;

                        if (Orientation == eSplit.NotSet && Children.Count < 1)
                        {
                            if (Parent != null)
                            {
                                Parent?.Children.Remove(this);

                                var row = Parent.Row;
                                var col = Parent.Column;

                                //Parent.Orientation = eSplit.NotSet;
                                if (Parent.Children.OfType<SplitGridModel>().Count() > 0)
                                {
                                    Parent.FromSplitGridModel(Parent.Children.OfType<SplitGridModel>().FirstOrDefault());
                                }

                                Parent.Row = row;
                                Parent.Column = col;

                            }
                            return;
                        }
                        else if (Orientation != eSplit.NotSet)
                        {
                            (int row, int col) = GetRowColFromMouse();

                            var cg = Orientation == eSplit.Vertical? Children.OfType<SplitGridModel>().FirstOrDefault(s => s.Column == col) 
                                                                   : Children.OfType<SplitGridModel>().FirstOrDefault(s => s.Row == row);






                            if (cg == null)
                            {
                               
                                Orientation = eSplit.NotSet;
                                
                            }
                            else if (cg.Orientation == eSplit.NotSet && cg.Children.Count() < 1)
                            {
                                Children.Remove(cg);
                                Orientation = eSplit.NotSet;

                                //if (Children.OfType<SplitGridModel>().Count() > 0) //Still one remaining. Let's become it.
                                //{
                                //    var sg = Children.OfType<SplitGridModel>().First();
                                //    Children.Remove(sg);

                                //    FromSplitGridModel(sg);
                                //}

                                return;

                            }
                        }
                    }

                    break;
                case eTool.DropDisplay:
                    if (Children.OfType<Display>().Count() < 1 && Orientation == eSplit.NotSet)
                    {
                        var display = (Display)Activator.CreateInstance(SplitterCanvas.DroppedType);
                        display._internal_PrepareUIElements();
                        
                        if (display == null)
                        {
                            return;
                        }

                        //var disp = new DisplayControl() { HostedDisplay = display };

                        Children.Add(display);
                        //CanvasModel.Displays.Add(display);

                    }
                    break;
                default:
                    break;
            }
        });

        public bool IsMouseOverChild<T>() where T : IDisplayable
        {
            return GetSelectedChild<T>() != null;
        }
    }


}
