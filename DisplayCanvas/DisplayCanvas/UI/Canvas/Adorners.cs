﻿using C3.Common.WPF.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Interactivity;
using System.Windows.Media;
using System.Windows.Shapes;

namespace DisplayCanvas.UI.Canvas
{
    public class CanvasAdorner : Adorner
    {
        public CanvasAdorner(FrameworkElement adornedElement, SplitGridView associatedView)
           : base(adornedElement)
        {
            _adornerLayer = AdornerLayer.GetAdornerLayer(adornedElement);
            _adornerLayer.Add(this);
            IsHitTestVisible = false;
            _width = adornedElement.ActualWidth;
            _height = adornedElement.ActualHeight;
            AssociatedView = associatedView;
            this._adorner = AddAdorner();

            if (_adorner != null)
                this._adorner.RenderTransform = _transform;
        }

        private UIElement _adorner;
        private AdornerLayer _adornerLayer;
        private Point _mousePosition;
        private double _width;
        private double _height;
        TranslateTransform _transform = new TranslateTransform(0, 0);

        public SplitGridView AssociatedView
        { get; set; }

        public AdornerType Type { get; private set; }

        protected override int VisualChildrenCount => 1;

        public Brush ValidDropBrush { get; set; } = Brushes.CadetBlue;
        public Brush InvalidDropBrush { get; set; } = Brushes.IndianRed;
        public Point MousePosition
        { get => _mousePosition; set => _mousePosition = value; }

        private Rectangle AddAdorner()
        {
            AdornerType adornerType = AdornerType.Custom;
            switch (SplitterCanvas.MouseTool)
            {

                case eTool.VerticalSplit:
                    adornerType = AdornerType.VerticalSplit;
                    break;
                case eTool.HorizontalSpilt:
                    adornerType = AdornerType.HorizontalSplit;
                    break;
                case eTool.Delete:
                    if (!AssociatedView?.IsMain?? false)
                        adornerType = AdornerType.Deletion;
                    //Show Delete Adorner
                    break;
                case eTool.DropDisplay:
                    adornerType = AdornerType.Placement;
                    //Show Drop Adorner
                    break;
                case eTool.Pointer:
                default:
                    break;
            }

            Type = adornerType;


            Rectangle adorner = null;
            switch (adornerType)
            {
                case AdornerType.VerticalSplit:
                    adorner = SplitterAdorner(adornerType);
                    adorner.Height = _height;
                    _width = adorner.Width;

                    break;
                case AdornerType.HorizontalSplit:
                    adorner = SplitterAdorner(adornerType);
                    adorner.Width = _width;
                    _height = adorner.Height;
                    break;
                case AdornerType.Placement:
                    adorner = PlacementAdorner();
                    adorner.Height = _height;
                    adorner.Width = _width;
                    break;
                case AdornerType.Deletion:
                    adorner = DeletionAdorner();
                    adorner.Height = _height;
                    adorner.Width = _width;
                    break;
                default:
                    break;
            }

            return adorner;

        }

        public void Update(Point mousePosition)
        {
            _mousePosition = mousePosition;

            switch (Type)
            {
                case AdornerType.Custom:
                    break;
                case AdornerType.VerticalSplit:
                    // DO snapping logic here
                    _transform.X = mousePosition.X;
                    _transform.Y = 0;
                    break;
                case AdornerType.HorizontalSplit:
                    _transform.X = 0;
                    _transform.Y = mousePosition.Y;
                    break;
                case AdornerType.Placement:
                    break;
                case AdornerType.Deletion:
                    break;
                default:
                    break;
            }

            InvalidateVisual();
        }

        public void Remove()
        {
            //isRemoved = true;
            _adornerLayer.Remove(this);
        }

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            this._adorner?.Arrange(new Rect(arrangeBounds));
            return arrangeBounds;
        }

        protected override Size MeasureOverride(Size constraint)
        {
            _adorner?.Measure(new Size(_width, _height));
            return _adorner?.DesiredSize ?? new Size() ;
        }

        protected override Visual GetVisualChild(int index) => _adorner;

        public static Rectangle SplitterAdorner(AdornerType orientation)
        {
            return new Rectangle()
            {
                StrokeThickness = 0,
                Fill = Brushes.DarkGray,
                Width = orientation == AdornerType.HorizontalSplit ? 0.0 : 3,
                Height = orientation == AdornerType.HorizontalSplit ? 3 : 0.0,
                HorizontalAlignment = orientation == AdornerType.HorizontalSplit ? System.Windows.HorizontalAlignment.Stretch : System.Windows.HorizontalAlignment.Left,
                VerticalAlignment = orientation == AdornerType.HorizontalSplit ? System.Windows.VerticalAlignment.Top : System.Windows.VerticalAlignment.Stretch,
                Opacity = 0.7,
                IsHitTestVisible = false,
                Tag = orientation,
            };
        }

        public static Rectangle PlacementAdorner()
        {
            return new Rectangle()
            {
                HorizontalAlignment = System.Windows.HorizontalAlignment.Stretch,
                VerticalAlignment = System.Windows.VerticalAlignment.Stretch,
                Fill = Brushes.SkyBlue,
                Opacity = 0.7,
                IsHitTestVisible = false,
                Tag = AdornerType.Placement,
            };

        }

        public static Rectangle DeletionAdorner()
        {
            var pa = PlacementAdorner();

            pa.Fill = Brushes.Firebrick;
            pa.Tag = AdornerType.Deletion;

            return pa;
        }



    }
}
