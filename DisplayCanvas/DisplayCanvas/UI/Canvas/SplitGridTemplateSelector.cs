﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace DisplayCanvas.UI.Canvas
{
    public class SplitGridItemContainerStyleSelector : StyleSelector
    {
        public Style DefaultStyle { get; set; }
        public Style HorizontalSplitterStyle { get; set; }
        public Style VerticalSplitterStyle { get; set; }

        public override Style SelectStyle(object item, DependencyObject container)
        {
            if (item is HorizontalSplitterModel)
                return HorizontalSplitterStyle;
            else if (item is VerticalSplitterModel)
                return VerticalSplitterStyle;

            return DefaultStyle;
        }
    }

    public class SplitGridTemplateSelector : DataTemplateSelector
    {
        public DataTemplate SplitGridModelTemplate
        { get; set; }

        public DataTemplate DisplayTemplate
        { get; set; }


        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is SplitGridModel)
                return SplitGridModelTemplate;
            else if (item is Display)
                return DisplayTemplate;
            
            return base.SelectTemplate(item, container);
        }
    }
}
