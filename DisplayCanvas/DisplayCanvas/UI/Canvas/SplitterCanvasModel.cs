﻿using C3.Common.WPF.Input;
using DisplayCanvas.UI.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DisplayCanvas.UI.Canvas
{
    [Serializable]
    public class SplitterCanvasModel : DisplayHostBaseViewModel
    {
        public SplitterCanvasModel()
        {
            CanvasName = "New Canvas";
            GridModel = new SplitGridModel();
            Displays = new List<Display>();
        }

        public override List<Display> Displays
        {
            get => GetValue<List<Display>>();
            set => SetValue(value);
        }
             //   OnPropertyChanged(); } }


        public SplitGridModel GridModel
        {
            get => GetValue<SplitGridModel>();
            set => SetValue(value);
        }
        


        public override string CanvasName
        { get => GetValue<string>(); set => SetValue(value); }// = "New Canvas";


        [XmlIgnore]
        public override List<ToolbarAction> CustomActions => new List<ToolbarAction>()
        {
            new ToolbarAction(description:"Pointer Mode", action: o => { SplitterCanvas.MouseTool = eTool.Pointer; }),
            new ToolbarAction(description:"Vertical Split Mode", action: o => { SplitterCanvas.MouseTool = eTool.VerticalSplit; }),
            new ToolbarAction(description:"Horiz. Split Mode", action: o => { SplitterCanvas.MouseTool = eTool.HorizontalSpilt; }),
            new ToolbarAction(description:"Delete Mode", action: o => { SplitterCanvas.MouseTool = eTool.Delete; }),
            new ToolbarAction(description:"Drop Dummy Mode", action: o => { SplitterCanvas.MouseTool = eTool.DropDisplay; SplitterCanvas.DroppedType = typeof(TestDisplayModel); }),

        };

        [XmlIgnore]
        public override RelayCommand<Display> ShowExpandedView => new RelayCommand<Display>(d =>
        {
            OverlayDisplay = d;
            ShowOverlay = !ShowOverlay; //SPS: THIS IS A HACK
        });

        private bool _showExpanded = false;

        private Display _overlayDisplay = null;

        [XmlIgnore]
        public Display OverlayDisplay
        { get => _overlayDisplay; set { _overlayDisplay = value; OnPropertyChanged(); } }

        [XmlIgnore]
        public bool ShowOverlay
        { get => _showExpanded; set { _showExpanded = value; OnPropertyChanged(); } }

        private List<Display> _displays = new List<Display>();
        
        /// <summary>
        /// Tries to add display and position it.
        /// </summary>
        /// <param name="disp"></param>
        /// <returns></returns>
        public bool TryAddDisplay(Display disp, int row = -1, int col = -1)
        {
            return false;

            //if (row < 0 || col < 0) //Could not find a placement
            //    return false;

            //disp.Column = col;
            //disp.Row = row;
            //Displays.Add(disp);
            //OnPropertyChanged(nameof(Displays));
            //return true;
        }

        //private (int row, int col) getAvailablePlacement()
        //{
        //    for (int r = 0; r < _rows; r++)
        //        for (int c = 0; c < _cols; c++)
        //            if (!_displays.Any(d => d.CurrentRow == r && d.CurrentColumn == c))
        //                return (r, c);

        //    return (-1, -1);
        //}

       
    }
}