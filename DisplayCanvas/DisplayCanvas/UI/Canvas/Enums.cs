﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisplayCanvas.UI.Canvas
{
    public enum eSplit
    {
        NotSet = 0,
        Vertical = 1,
        Horizontal = 2,
    }

    public enum AdornerType
    {
        Custom,
        VerticalSplit,
        HorizontalSplit,
        Placement,
        Deletion,

    }

    public enum eExpandMode
    {
        Unknown,
        Normal,
        Expanded,
    }
}
