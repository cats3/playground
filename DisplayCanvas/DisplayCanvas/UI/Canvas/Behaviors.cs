﻿using C3.Common.WPF.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Interactivity;

namespace DisplayCanvas.UI.Canvas
{

    public class ObserverBehaviour : System.Windows.Interactivity.Behavior<SplitGridView>
    {
        public static readonly DependencyProperty MouseYProperty = DependencyProperty.Register(
            "MouseY", typeof(double), typeof(ObserverBehaviour), new PropertyMetadata(default(double)));

        public double MouseY
        {
            get { return (double)GetValue(MouseYProperty); }
            set { SetValue(MouseYProperty, value); }
        }

        public static readonly DependencyProperty MouseXProperty = DependencyProperty.Register(
            "MouseX", typeof(double), typeof(ObserverBehaviour), new PropertyMetadata(default(double)));

        public double MouseX
        {
            get { return (double)GetValue(MouseXProperty); }
            set { SetValue(MouseXProperty, value); }
        }

        public static readonly DependencyProperty ActualSizeProperty = DependencyProperty.Register(
           "ActualSize", typeof(Size), typeof(ObserverBehaviour), new PropertyMetadata(default(Size)));

        public Size ActualSize
        {
            get { return (Size)GetValue(ActualSizeProperty); }
            set { SetValue(ActualSizeProperty, value); }
        }

        protected override void OnAttached()
        {
            AssociatedObject.MouseMove += AssociatedObjectOnMouseMove;
            AssociatedObject.SizeChanged += AssociatedObject_SizeChanged;
            ActualSize = AssociatedObject.RenderSize;
        }

        private void AssociatedObject_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            ActualSize = e.NewSize;
        }

        private void AssociatedObjectOnMouseMove(object sender, MouseEventArgs mouseEventArgs)
        {
            var pos = mouseEventArgs.GetPosition(AssociatedObject);
            MouseX = pos.X;
            MouseY = pos.Y;

        }

        protected override void OnDetaching()
        {
            AssociatedObject.MouseMove -= AssociatedObjectOnMouseMove;
            AssociatedObject.SizeChanged -= AssociatedObject_SizeChanged;
        }
    }


    public class AdornerBehavior : Behavior<SplitGridView>
    {
        public static DependencyProperty OnMouseLeftButtonDownProperty =
           DependencyProperty.Register("OnMouseLeftButtonDown", typeof(ICommand), typeof(AdornerBehavior),
           new PropertyMetadata(new RelayCommand(o => { })));

        public static DependencyProperty SplitGridModelProperty =
          DependencyProperty.Register("SplitGridModel", typeof(SplitGridModel), typeof(AdornerBehavior),
          new PropertyMetadata());

        private CanvasAdorner _adorner; // FrameworkElement
        public ICommand OnMouseLeftButtonDown
        {
            get
            {
                return (ICommand)GetValue(OnMouseLeftButtonDownProperty);
            }
            set
            {
                SetValue(OnMouseLeftButtonDownProperty, value);
            }
        }

       
        public SplitGridModel SplitGridModel
        {
            get
            {
                return (SplitGridModel)GetValue(SplitGridModelProperty);
            }
            set
            {
                SetValue(SplitGridModelProperty, value);
            }
        }

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.MouseEnter += AssociatedObject_MouseEnter;
            this.AssociatedObject.MouseLeave += AssociatedObject_MouseLeave;
            this.AssociatedObject.PreviewMouseMove += AssociatedObject_MouseMove;
            this.AssociatedObject.PreviewMouseLeftButtonDown += AssociatedObject_PreviewMouseLeftButtonDown;
            this.AssociatedObject.AllowDrop = true;
            this.AssociatedObject.DragEnter += AssociatedObject_DragEnter;
            this.AssociatedObject.DragLeave += AssociatedObject_DragLeave;
            this.AssociatedObject.DragOver += AssociatedObject_DragOver;
            this.AssociatedObject.Drop += AssociatedObject_Drop;
        }



        private void AssociatedObject_Drop(object sender, DragEventArgs e)
        {
            if (_draggedDisplay == null || SplitGridModel.Orientation != eSplit.NotSet)
                return;

            SplitGridModel sgv = (e.Data.GetData(nameof(SplitGridModel)) as SplitGridModel);

            if (sgv != null)
            {
                sgv.Children.Remove(_draggedDisplay);
                sgv.Orientation = eSplit.NotSet;
                SplitGridModel.Children.Add(_draggedDisplay);
                e.Handled = true;
            }

            if (this._adorner != null)
                this._adorner.Remove();

            _draggedDisplay = null;
            


        }

        Display _draggedDisplay= null;

        private void AssociatedObject_DragOver(object sender, DragEventArgs e)
        {
            if (SplitGridModel == null || SplitGridModel.Orientation != eSplit.NotSet)
                return;

            var disp = (Display)e.Data.GetData(typeof(Display));

            if (disp != null)
            {
                _draggedDisplay = disp;
                e.Effects = DragDropEffects.Move;
               e.Handled = true;
            }

            
        }

        private void AssociatedObject_DragLeave(object sender, DragEventArgs e)
        {
            if (this._adorner != null)
                this._adorner.Remove();

            //e.Handled = true;
        }

        private void AssociatedObject_DragEnter(object sender, DragEventArgs e)
        {

            if (SplitGridModel == null || SplitGridModel.Orientation != eSplit.NotSet)
                return;

            var disp = (Display)e.Data.GetData(typeof(Display));

            if (disp != null)
            {
                //SplitterCanvas.MouseTool = eTool.DropDisplay;
                //SplitterCanvas.DroppedType = null;
                //SplitterCanvas.DroppedDisplay = disp;
                this._adorner = new CanvasAdorner(sender as FrameworkElement, AssociatedObject);
            }
            e.Handled = true;
        }




        private void AssociatedObject_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            SplitGridModel.GridSize = AssociatedObject.RenderSize;
            OnMouseLeftButtonDown?.Execute(e);
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.MouseEnter -= AssociatedObject_MouseEnter;
            this.AssociatedObject.MouseLeave -= AssociatedObject_MouseLeave;
            this.AssociatedObject.PreviewMouseMove -= AssociatedObject_MouseMove;
            this.AssociatedObject.PreviewMouseLeftButtonDown -= AssociatedObject_PreviewMouseLeftButtonDown;
            this.AssociatedObject.DragEnter -= AssociatedObject_DragEnter;
            this.AssociatedObject.DragLeave -= AssociatedObject_DragLeave;
            this.AssociatedObject.DragOver -= AssociatedObject_DragOver;
            this.AssociatedObject.Drop -= AssociatedObject_Drop;
        }

        private void AssociatedObject_MouseMove(object sender, MouseEventArgs e)
        {
            this._adorner?.Update(e.GetPosition(AssociatedObject));
            e.Handled = false;
        }

        private void AssociatedObject_MouseLeave(object sender, MouseEventArgs e)
        {
            this._adorner?.Remove();
            this._adorner = null;
            e.Handled = false;
        }

        private void AssociatedObject_MouseEnter(object sender, MouseEventArgs e)
        {
            if (!SplitGridModel.Children.OfType<SplitGridModel>().Any() && this._adorner == null)
                    this._adorner = new CanvasAdorner(sender as FrameworkElement, AssociatedObject);

            e.Handled = false;
        }
    }

    public class DragDropInfo
    {
        public DisplayControl Display
        {
            get; set;
        }

        public SplitGridView Parent
        {
            get; set;
        }
    }


   





    public class DragBehavior : Behavior<SplitGridView>
    {
        public static DependencyProperty SplitGridModelProperty =
         DependencyProperty.Register("SplitGridModel", typeof(SplitGridModel), typeof(DragBehavior),
         new PropertyMetadata());

        public SplitGridModel SplitGridModel
        {
            get
            {
                return (SplitGridModel)GetValue(SplitGridModelProperty);
            }
            set
            {
                SetValue(SplitGridModelProperty, value);
            }
        }


        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.MouseLeftButtonDown += AssociatedObject_MouseLeftButtonDown;
            this.AssociatedObject.MouseLeftButtonUp += AssociatedObject_MouseLeftButtonUp;
            this.AssociatedObject.MouseLeave += AssociatedObject_MouseLeave;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.MouseLeftButtonDown -= AssociatedObject_MouseLeftButtonDown;
            this.AssociatedObject.MouseLeftButtonUp -= AssociatedObject_MouseLeftButtonUp;
            this.AssociatedObject.MouseLeave -= AssociatedObject_MouseLeave;
        }


        private bool _isMouseLeftDown = false;


        private void AssociatedObject_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _isMouseLeftDown = true;
        }

        private void AssociatedObject_MouseLeave(object sender, MouseEventArgs e)
        {
            if (_isMouseLeftDown)
            {

                Display display = SplitGridModel.Children.OfType<Display>().FirstOrDefault();

                if (display != null)
                {
                    DataObject da = new DataObject();
                    da.SetData(typeof(Display), display);
                    da.SetData(nameof(SplitGridModel), SplitGridModel);
                    System.Windows.DragDrop.DoDragDrop(this.AssociatedObject, da, DragDropEffects.Move);
                }

            }
            _isMouseLeftDown = false;
            
        }

        private void AssociatedObject_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _isMouseLeftDown = false;
        }

       

       

    }



    public class TabDragBehavior : Behavior<Border>
    {
       

        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.MouseLeftButtonDown += AssociatedObject_MouseLeftButtonDown;
            this.AssociatedObject.MouseLeftButtonUp += AssociatedObject_MouseLeftButtonUp;
            this.AssociatedObject.MouseLeave += AssociatedObject_MouseLeave;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.MouseLeftButtonDown -= AssociatedObject_MouseLeftButtonDown;
            this.AssociatedObject.MouseLeftButtonUp -= AssociatedObject_MouseLeftButtonUp;
            this.AssociatedObject.MouseLeave -= AssociatedObject_MouseLeave;
        }


        private bool _isMouseLeftDown = false;


        private void AssociatedObject_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _isMouseLeftDown = true;
        }

        private void AssociatedObject_MouseLeave(object sender, MouseEventArgs e)
        {
            if (_isMouseLeftDown)
            {
                DataObject da = new DataObject();
                da.SetData(typeof(SplitterCanvasModel), AssociatedObject.DataContext);
                System.Windows.DragDrop.DoDragDrop(this.AssociatedObject, da, DragDropEffects.Move);

            }
            _isMouseLeftDown = false;

        }

        private void AssociatedObject_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            _isMouseLeftDown = false;
        }
    }


    public class TabDropBehavior : Behavior<TabControl>
    {
        public static DependencyProperty CanvasGroupModelProperty =
         DependencyProperty.Register("CanvasGroupModel", typeof(CanvasGroup), typeof(TabDropBehavior),
         new PropertyMetadata());

        public CanvasGroup CanvasGroupModel
        {
            get
            {
                return (CanvasGroup)GetValue(CanvasGroupModelProperty);
            }
            set
            {
                SetValue(CanvasGroupModelProperty, value);
            }
        }


        protected override void OnAttached()
        {
            base.OnAttached();
            this.AssociatedObject.AllowDrop = true;
            this.AssociatedObject.DragEnter += AssociatedObject_DragEnter;
            this.AssociatedObject.DragLeave += AssociatedObject_DragLeave;
            this.AssociatedObject.DragOver += AssociatedObject_DragOver;
            this.AssociatedObject.Drop += AssociatedObject_Drop;
        }

        protected override void OnDetaching()
        {
            base.OnDetaching();
            this.AssociatedObject.DragEnter -= AssociatedObject_DragEnter;
            this.AssociatedObject.DragLeave -= AssociatedObject_DragLeave;
            this.AssociatedObject.DragOver -= AssociatedObject_DragOver;
            this.AssociatedObject.Drop -= AssociatedObject_Drop;
        }


        private void AssociatedObject_Drop(object sender, DragEventArgs e)
        {
            if (_draggedCanvas == null)
                return;

            CanvasGroup oldGroup = _draggedCanvas.Group;


            if (oldGroup != null)
            {
                oldGroup.Canvases.Remove(_draggedCanvas);
                CanvasGroupModel.Canvases.Add(_draggedCanvas);
                e.Handled = true;
            }

            _draggedCanvas = null;

        }

        SplitterCanvasModel _draggedCanvas = null;

        private void AssociatedObject_DragOver(object sender, DragEventArgs e)
        {
            if (CanvasGroupModel == null)
                return;

            var canv = (SplitterCanvasModel)e.Data.GetData(typeof(SplitterCanvasModel));

            if (canv != null)
            {
                _draggedCanvas = canv;
                e.Effects = DragDropEffects.Move;
                e.Handled = true;
            }


        }

        private void AssociatedObject_DragLeave(object sender, DragEventArgs e)
        {
           
            //e.Handled = true;
        }

        private void AssociatedObject_DragEnter(object sender, DragEventArgs e)
        {

            //if (CanvasGroupModel == null)
            //    return;

            //var canv = (DisplayHostBaseViewModel)e.Data.GetData(typeof(DisplayHostBaseViewModel));


            //if (canv != null)
            //{
            //    //SplitterCanvas.MouseTool = eTool.DropDisplay;
            //    //SplitterCanvas.DroppedType = null;
            //    //SplitterCanvas.DroppedDisplay = disp;
            //    //this._adorner = new CanvasAdorner(sender as FrameworkElement, AssociatedObject);
            //}
            //e.Handled = true;
        }

    }

}
