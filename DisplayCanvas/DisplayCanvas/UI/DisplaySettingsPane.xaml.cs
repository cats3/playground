﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DisplayCanvas
{
    /// <summary>
    /// Interaction logic for DisplaySettingsPane.xaml
    /// </summary>
    public partial class DisplaySettingsPane : UserControl
    {
        public DisplaySettingsPane()
        {
            InitializeComponent();
            this.DataContextChanged += DisplaySettingsPane_DataContextChanged;
        }



        private void DisplaySettingsPane_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Display display;
            if ((display = e.NewValue as Display) == null)
                return;

            addFromList(display.Parameters);
        }

        private void addFromList(List<IDisplayParameter> parameters, bool clearCurrent = true)
        {
            if (clearCurrent)
                wrap.Children.Clear(); 

            parameters.ForEach(p =>
            {
                wrap.Children.Add(new DisplayParameterControl() { Parameter = p });
            });
        }
    }
}
