﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Cubus.Core;
using C3.Common.WPF.Input;
using System.Collections.ObjectModel;
using DisplayCanvas.UI.Canvas;
using System.Windows;
using DisplayCanvas.UI;
using System.Windows.Input;
using DisplayCanvas.Classes;
using System.Xml.Serialization;
using ServiceManager = C3.Common.Injection.Services;
using DisplayCanvas.Services;

namespace DisplayCanvas
{
    [XmlInclude(typeof(ObservableCollection<SplitterCanvasModel>))]
    public class CanvasGroup : CanvasBaseViewModel
    {
        public CanvasGroup()
        {
            management = (ServiceManager.GetService<IManagementService>() as CanvasManagementService);
            management?.CurrentState.Groups.Add(this);
            Canvases = new ObservableCollection<SplitterCanvasModel>();
        }
        
        CanvasManagementService management = null;

        ~CanvasGroup()
        {
            (ServiceManager.GetService<CanvasManagementService>() as CanvasManagementService)?.CurrentState.Groups.Remove(this);

        }

        public ObservableCollection<SplitterCanvasModel> Canvases
        { get => GetValue<ObservableCollection<SplitterCanvasModel>>();
            set { SetValue(value); } } //OnPropertyChanged() is already called

        [XmlIgnore]
        public List<Display> AvailableDisplays
        => (ServiceManager.GetService<CanvasManagementService>() as CanvasManagementService).LoadedDisplayTypes;

        public void RemoveCanvas(SplitterCanvasModel c)
        {
            if (Canvases.Contains(c))
            {
                Canvases.Remove(c);
                OnPropertyChanged(nameof(Canvases));
            }
        }

        public void AddCanvas(SplitterCanvasModel c = null)
        {
            if (c == null)
                c = new SplitterCanvasModel();

            c.Group = this;
                
            Canvases.Add(c);
            OnPropertyChanged(nameof(Canvases));
        }

        CanvasWindow myWindow;

        public void CreateNewWindow()
        {
            if (myWindow != null)
                myWindow.Close();

            myWindow = new CanvasWindow();
            var ng = GetNewWindowedGroup();

            myWindow.Group = ng;
            ng.OnPropertyChanged(nameof(AvailableDisplays));


            myWindow.Show();

        }

        private CanvasGroup GetNewWindowedGroup()
        {
            var cg = new CanvasGroup()
            {
                IsWindowed = true,
                Position = Application.Current.MainWindow.PointToScreen(Mouse.GetPosition(Application.Current.MainWindow)), //SPS: Use this for Configure View Positioning
            };

            cg.AddCanvas();

            return cg;



        }

        public Point Position
        { get; set; } = new Point(0, 0);


        public bool IsWindowed
        { get; set; } = false;


        [XmlIgnore]
        public RelayCommand AddCanvasCommand
            => new RelayCommand(o => AddCanvas());

        [XmlIgnore]
        public RelayCommand CreateNewWindowCommand
            => new RelayCommand(o => CreateNewWindow());

        internal void CloseWindow()
        {
            if (myWindow != null)
                myWindow.Close();

        }
    }
}