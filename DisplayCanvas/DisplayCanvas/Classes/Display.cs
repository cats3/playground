﻿using C3.Common.WPF.Input;
using DisplayCanvas.Classes;
using DisplayCanvas.UI.Canvas;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Xml.Serialization;

namespace DisplayCanvas
{
    [Serializable]
    public abstract class Display : CanvasBaseViewModel, IDisplay
    {
        public Display()
        {

        }

        public virtual void _internal_PrepareUIElements()
        {
            PrepareUIElements();

            if (ConfigureView == null)
                ConfigureView = new DisplaySettingsPane() { DataContext = this };

            OnPropertyChanged(nameof(CustomActions));
            OnPropertyChanged(nameof(View));
            OnPropertyChanged(nameof(ConfigureView));

        }

        public abstract void PrepareUIElements();
            
        



        public string Label
        { get => _label;
            set
            {
                _label = value;
                OnPropertyChanged();
            }
        }

        private string _label = "Click and hold this text to drag";

        [XmlIgnore]
        public UIElement View
        { get => _content;
            set
            {
                _content = value;
                OnPropertyChanged();
            }
        }

        [XmlIgnore]
        private UIElement _content = new TextBlock()
        {
            HorizontalAlignment = HorizontalAlignment.Center,
            VerticalAlignment = VerticalAlignment.Center,
            TextWrapping = TextWrapping.WrapWithOverflow,
            Text = "Blue Border = Resize handles. Right click on Tab Bar for Add Tab, Right click anywhere on the canvas for more",
            FontSize = 14
        };

        [XmlIgnore]
        public UIElement ConfigureView
        { get => _settingsControl; set { _settingsControl = value; OnPropertyChanged(); } }
        [XmlIgnore]
        private UIElement _settingsControl;

        [XmlIgnore]
        private List<IDisplayParameter> _parameters = new List<IDisplayParameter>();

        [XmlIgnore]
        public virtual List<IDisplayParameter> Parameters
        { get => _parameters;
            set
            {
                _parameters = value;
                OnPropertyChanged();
            }

        }

        public abstract void RequestAction(eAction action);

        public abstract string DisplayName { get; }

        public abstract string DisplayDescription { get; }

        [XmlIgnore]
        public List<ToolbarAction> CustomActions
        {
            get;
            set;
        } = new List<ToolbarAction>();

        public int Row
        { get; set; } = 0;

        public int Column
        { get; set; } = 0;

        [XmlIgnore]
        public eExpandMode ExpandMode
        { get; private set; } = eExpandMode.Normal;

        [XmlIgnore]
        public int CurrentRowSpan { get; internal set; }

        [XmlIgnore]
        public int CurrentColumnSpan { get; internal set; }


       
        


    }

    public enum eAction
    {
        Play,
        Pause,
        Reset,
    }
}