﻿using DisplayCanvas.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

namespace DisplayCanvas
{
    //GLOBAL TODO  | (Search for "SPS:"  for work)
    //===========
    //
    // WEEK 1 - COMPLETED
    //-------

    //
    // WEEK 2 
    // ------
    //  DONE: Creation of SplitGrid and SplitterCanvas
    //  DONE: Fix Display > Expand
    //  DONE: Finish Adorners
    //  DONE: Drag/Move of Displays
    //  DONE: Display Toolbox 
    //  DONE: Make sure MVVM is completely impelemted thorougly
    //  DONE: Code Cleanup (Delete old stuff, Namespacing, etc)
    // 
    //
    // WEEK 3
    // ------
    //  DONE: (De)Serialisation
    //  DONE: Multi-Window support
    //  DONE: Tab Dragging
    //  TODO: Create TestInteropService (Grab/Push XML config, Test Start/Stop tie-ins, Template Loading)
    //  TODO: Host within Cubus
    //  TODO: Create theme engine and send how-to's to Chris and Lamar for implementation in their displays.
    //  TODO: Finish SignalService (with temporary signal access)
    //  DONE: Base Display services off of IService (C3.Common.Injection)
    //  
    // 
    //
    // WEEK 4
    // ------
    //  TODO: Help integrate other's displays
    //  TODO: Finalise Signal Service with Iain's new Acquisition Service
    //  TODO: Test Thoroughly
    //  TODO: Finalise Cubus 2.X Stylesheet
    //  TODO: Finish DigitalDisplayList. Needs ability to add/select signal, and mode (direct, mean, etc.
    //  TODO: Create "New Canvas" Quick-Layout Page and Design Doc

    //===============================
    //======= Core Completed ========
    //===============================


    //public static class CanvasManager 
    //{
    //    //public static CanvasSettings CurrentState
    //    //{ get; private set; } = new CanvasSettings();


    //    //public static List<CanvasGroup> ActiveCanvasGroups
    //    //{ get; set; } = new List<CanvasGroup>();


    //    //public static List<IDisplayService> DisplayServices
    //    //{ get; private set; }

    //    //public static T GetServiceOfType<T>() where T : IDisplayService
    //    //{
    //    //    var ret = DisplayServices.OfType<T>().ToList();

    //    //    if (ret.Count > 0)
    //    //        return ret.First();

    //    //    var serv = (T)Activator.CreateInstance(typeof(T));

    //    //    DisplayServices.Add(serv);

    //    //    return serv;
    //    //}

    //    //public static string GetSaveData()
    //    //{
    //    //    var s = "";
    //    //    SettingsData sd = new SettingsData();
    //    //    int i = 0;
    //    //    ActiveCanvasGroups.ForEach(acg => sd.Entries.Add(new SettingsEntry<CanvasGroup>() { EntryName = "CanvasGroup" + i++, Instance = acg, TypeName = typeof(CanvasGroup).FullName } ));

    //    //    return sd.GetXML();
    //    //}

    //    //public static void LoadFromXML(string xmlAsText)
    //    //{
    //    //    var s = SettingsData.FromXML(xmlAsText);

    //    //    ActiveCanvasGroups.ForEach(cg => cg.CloseWindow());

    //    //    ActiveCanvasGroups = s.Entries.Where(p => p.Instance is CanvasGroup).Select(p => p.Instance as CanvasGroup).ToList();
    //    //    ActiveCanvasGroups.Where(cg => cg.IsWindowed).ToList().ForEach(cg => cg.CreateNewWindow());
    //    //}


    //    //public static List<Display> LoadedDisplayTypes 
    //    //{ get; set; } = new List<Display>();




    //    ////SPS: Implement AvailableSignals?

    //    //private static CanvasGroup getGroup(this Display display)
    //    //{
    //    //    return ActiveCanvasGroups.FirstOrDefault(cg => cg.Canvases.Any(c => c.Displays.Contains(display)));
    //    //}



    //    //private static T GetValue<T>([CallerMemberName] string propertyName = "", bool autoInitialise = false)
    //    //{
    //    //    if (!CurrentState.Entries.Any(p => p.EntryName == propertyName))
    //    //        CurrentState.Entries.Add(new SettingsEntry<T>() { EntryName = propertyName, Instance = autoInitialise ? (T)Activator.CreateInstance(typeof(T)) : default(T) });

    //    //    return (T)CurrentState[propertyName].Instance;
    //    //}

    //    ///// <summary>
    //    ///// Sets the SettingsData entry for the Member in question, and calles OnPropertyChanged().
    //    ///// </summary>
    //    ///// <typeparam name="T"></typeparam>
    //    ///// <param name="value"></param>
    //    ///// <param name="propertyName"></param>
    //    //private static void SetValue<T>(T value, [CallerMemberName] string propertyName = "")
    //    //{
    //    //    if (!CurrentState.Entries.Any(p => p.EntryName == propertyName))
    //    //        CurrentState.Entries.Add(new SettingsEntry<T>() { EntryName = propertyName, Instance = value });
    //    //    else
    //    //        CurrentState[propertyName].Instance = value;

    //    //}
    //}



}
