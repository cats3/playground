﻿namespace DisplayCanvas
{
    public interface IDisplayable
    {
        int Column { get; set; }
        int Row { get; set; }
    }
}