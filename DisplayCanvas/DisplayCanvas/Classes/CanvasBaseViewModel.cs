﻿using C3.Common.WPF.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using C3.Common;
using C3.Common.Models;

namespace DisplayCanvas.Classes
{   [Serializable]
    public class CanvasBaseViewModel : BaseModel, ISaveable
    {
        [XmlIgnore]
        public SettingsData Settings { get; set; } = new SettingsData();

        public string ElementName => GetType().FullName;

        public T GetValue<T>([CallerMemberName] string propertyName = "")
        {
            AddIfNotPresent(default(T), propertyName);
            return (T)Settings[propertyName].Instance;
        }

        /// <summary>
        /// Sets the SettingsData entry for the Member in question, and calles OnPropertyChanged().
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value"></param>
        /// <param name="propertyName"></param>
        public void SetValue<T>(T value, [CallerMemberName] string propertyName = "")
        {
           if (!AddIfNotPresent(value, propertyName))
                Settings[propertyName].Instance = value;

            OnPropertyChanged(propertyName);
        }

        private bool AddIfNotPresent<T>(T value, string entryName)
        {
            if (!Settings.ContainsEntry(entryName))
            {
                Settings.AddEntry(value, entryName);
                return true;
            }
            return false;
        }


        protected virtual void OnLoad()
        {
            

            Settings.Entries.ForEach(s => OnPropertyChanged(s.EntryName));
        }


        
    }




}
