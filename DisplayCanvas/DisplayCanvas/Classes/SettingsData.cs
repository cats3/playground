﻿using DisplayCanvas.Classes;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Xml.Serialization;

namespace DisplayCanvas
{
    [Serializable]
    public class SettingsData : ISettings
    {
        [XmlIgnore]
        public List<SettingsEntry> Entries
        { get; set; } = new List<SettingsEntry>();

        [XmlIgnore]
        public string Path
        { get; set; }

        public bool ContainsEntry(string entryName)
        {
            return Entries.Any(e => e.EntryName == entryName);
        }

        public void AddEntry<T>(T value, string entryName)
        {
            if (!ContainsEntry(entryName))
            {
                if (value is ISaveable)
                {
                    Entries.Add(new SettingsEntry()
                    {
                        Instance = value,
                        Settings = (value as ISaveable).Settings,
                        EntryName = entryName,
                        TypeName = value.GetType().FullName
                    });
                }
                else if (value is IEnumerable<ISaveable>)
                {
                    Entries.Add(new SettingsEntry()
                    {
                        Instance = value,
                        //    ChildEntries = (value as IEnumerable<ISaveable>).ToList().Select(c => new SaveableEntry()
                        //{
                        //        Instance = c,
                        //        Settings = (c as ISaveable).Settings,
                        //        EntryName = c.ElementName,
                        //        TypeName = c.GetType().FullName
                        //    }).ToList(),

                        EntryName = entryName,
                        TypeName = value.GetType().FullName,
                    } as SettingsEntry);
                }
                else
                {
                    Entries.Add(new SettingsEntry()
                    {
                        EntryName = entryName,
                        Instance = value,
                        TypeName = value.GetType().FullName
                    });
                }
            }
        }
       

        public void RemoveEntry(string entryName)
        {
            if (ContainsEntry(entryName))
            {
                var entry = Entries.FirstOrDefault(e => e.EntryName == entryName);
                Entries.Remove(entry);
            }
        }

        public void Instantiate()
        {
            Entries.ForEach(e =>
            {
                if (e.Instance == null)
                {
                    e.Instance = Activator.CreateInstance(Type.GetType(e.TypeName));

                    if (e.Instance is ISaveable)
                    {
                        (e.Instance as ISaveable).Settings = (e as SettingsEntry).Settings;
                    }
                    else if (e.Instance is IEnumerable<ISaveable>)
                    {
                        (e as SettingsEntry).ChildEntries.ForEach(s => s.Settings.Instantiate());
                    }
                }
            });
        }

        //public List<SerializableEntry> SerialisableEntries { get; set; }
        //{ get => Entries.Select(e => SerializableEntry.FromInterface(e)).ToList();
        //    set => Entries = value.Cast<ISettingsEntry>().ToList();  }


        public static SettingsData LoadFromXML(string xmlstring)
        {
//            List<Type> extraTypes = new List<Type>(CanvasManager.LoadedDisplayTypes.Select(d => d.GetType())){ typeof(SplitGridModel),
//typeof(DisplayHostBaseViewModel), typeof(SplitterCanvasModel), typeof(ObservableCollection<SplitterCanvasModel>), typeof(CanvasGroup) };

            MemoryStream memstream = new MemoryStream();
            var byts = Encoding.ASCII.GetBytes(xmlstring);
            memstream.Write(byts, 0, byts.Length);
            memstream.Position = 0;
            XmlSerializer xml = new XmlSerializer(typeof(SettingsData));//, extraTypes.ToArray());

            SettingsData sd = (SettingsData)xml.Deserialize(memstream);



           // sd.Entries = sd.SerialisableEntries.Cast<ISettingsEntry>().ToList();

            return sd;
        }


      


        public virtual string GetXML()
        {
            //            List<Type> extraTypes = new List<Type>(CanvasManager.LoadedDisplayTypes.Select(d => d.GetType())){ typeof(SplitGridModel), 
            //typeof(DisplayHostBaseViewModel), typeof(SplitterCanvasModel), typeof(ObservableCollection<SplitterCanvasModel>), typeof(CanvasGroup) };

            // SerialisableEntries = Entries.Select(e => SerializableEntry.FromInterface(e)).ToList();

            XmlSerializer xml = new XmlSerializer(typeof(SettingsData));//, extraTypes.ToArray());
            MemoryStream memstream = new MemoryStream();

             xml.Serialize(memstream, this);

            memstream.Position = 0;
            return new StreamReader(memstream).ReadToEnd();
        }

        internal static SettingsData LoadFrom(string path)
        {
            return SettingsData.LoadFromXML(File.ReadAllText(path));
        }

        internal void Save()
        {
            if (Path != null)
                File.WriteAllText(Path, GetXML());
        }

        /// <summary>
        /// Adds the settings from the provided ISaveable to this SettingsData.Entries.
        /// </summary>
        /// <param name="item"></param>
        internal void AddSaveable(ISaveable item)
        {
            Entries.Add( new SettingsEntry() { EntryName = item.ElementName, Instance = item.Settings, TypeName = item.GetType().FullName } );
        }
      

        public SettingsEntry this[[CallerMemberName] string propertyName = ""]
        {
            get { return Entries.FirstOrDefault(s => s.EntryName == propertyName); }
            set
            {
                if (Entries.Find(s => s.EntryName == propertyName) != null)
                {
                    Entries[Entries.IndexOf(Entries.Find(s => s.EntryName == propertyName))]= value;
                }
                else
                {
                    Entries.Add(value);
                }
            }
        }

    }

}