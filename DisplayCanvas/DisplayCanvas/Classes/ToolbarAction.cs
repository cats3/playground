﻿using C3.Common.WPF.Input;
using System;
using System.Windows.Controls;

namespace DisplayCanvas
{
    public class ToolbarAction : RelayCommand
    {
        public ToolbarAction() : base(null)
        {

        }
        public ToolbarAction(Action<object> action) :base(action)
        {

        }

        public ToolbarAction(Action<object> action, Predicate<object> canExecute = null, string description = "" , Image icon = null) : base(action, canExecute)
        {
            Description = description;
        }

        public Image Icon
        { get; private set; }

        public string Description
        { get; private set; }



    }

    public class ToolbarToggleAction : ToolbarAction
    {
        public ToolbarToggleAction(Action<object> action) : base(action)
        {

        }

        public ToolbarToggleAction(Action<object> action, Predicate<object> canExecute,  Image UncheckedIcon = null, Image Checkedicon = null) : base(action, canExecute)
        {

        }

        public Image UncheckedIcon
        { get; private set; }

        public Image CheckedIcon
        { get; private set; }

        public bool Checked
        { get; set; }

    }

}