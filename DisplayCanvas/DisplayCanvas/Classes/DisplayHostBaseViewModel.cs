﻿using C3.Common.WPF.Input;
using DisplayCanvas.Classes;
using DisplayCanvas.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DisplayCanvas
{
    [Serializable]
    public abstract class DisplayHostBaseViewModel : CanvasBaseViewModel, IToolbarParticipant
    {
        public DisplayHostBaseViewModel()
        {
            DisplayControl.ExpandedViewRequested += DisplayControl_ExpandedViewRequested;
        }

        ~DisplayHostBaseViewModel()
        {
            DisplayControl.ExpandedViewRequested -= DisplayControl_ExpandedViewRequested;
        }

        private void DisplayControl_ExpandedViewRequested(object sender, Display e)
        {
           // if (Displays.Contains(e))
                ShowExpandedView.Execute(e);
        }

        [XmlIgnore]
        public abstract string CanvasName { get; set; }

        
        public abstract List<Display> Displays { get; set; }

        [XmlIgnore]
        public abstract List<ToolbarAction> CustomActions
        { get; }

        [XmlIgnore]
        public abstract RelayCommand<Display> ShowExpandedView { get; }

        [XmlIgnore]
        public CanvasGroup Group { get; set; }
    }


}