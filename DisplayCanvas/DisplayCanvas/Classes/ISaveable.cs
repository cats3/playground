﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace DisplayCanvas.Classes
{
    public interface ISaveable
    {
        SettingsData Settings { get; set; }

        T GetValue<T>([CallerMemberName] string propertyName = "");
        void SetValue<T>(T value, [CallerMemberName] string propertyName = "");

        /// <summary>
        /// Unique Name used for Serialisation
        /// </summary>
        string ElementName { get; }


      
    }

  

}