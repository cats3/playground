﻿using DisplayCanvas.Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;

namespace DisplayCanvas
{
    //public class SettingsEntry<T> : ISettingsEntry
    //{
    //    public SettingsEntry()
    //    {
    //        TypeName = typeof(T).FullName;
    //    }

    //    public T Instance
    //    {
    //        get;set;
    //    }

    //    public string EntryName
    //    { get; set; }
    //    public string TypeName { get; set; }

    //    object ISettingsEntry.Instance { get => Instance; set => Instance = (T)value; }

      
    //}

    public class SettingsEntry : ISettingsEntry
    {
        public SettingsEntry()
        {

        }
        public string EntryName { get; set; }

        public object Instance { get; set; }

        public string TypeName { get; set; }

        [XmlIgnore]
        public SettingsData Settings { get; set; }


        //public object SerialisableInstance
        //{
        //    get => Instance is IEnumerable<ISaveable> ? (Instance as IEnumerable<ISaveable>).ToList() : Instance;
        //    set => _serialisableInstance = value;
        //}

        //internal object _serialisableInstance = null;

        //internal List<SettingsEntry> _ChildEntries = new List<SettingsEntry>();

        [XmlIgnore]
        public List<SettingsEntry> ChildEntries
        {
            get; set;
            //get => (Instance as IEnumerable<ISaveable>)?.ToList().Select(c => new SettingsEntry()
            //{
            //    Instance = c,
            //    Settings = (c as ISaveable).Settings,
            //    EntryName = c.ElementName,
            //    TypeName = c.GetType().FullName
            //}).ToList()?? new List<SettingsEntry>();

            //set
            //{
            //    _ChildEntries = value;//SPS: Do activation of type here?
            //}
        } = new List<SettingsEntry>();
    }

   

    //public class SaveableList<T> : SettingsEntry<T>
    //{
        
    //    public List<SaveableEntry> ChildEntries
    //    {
    //        get => (Instance as IEnumerable<ISaveable>).ToList().Select(c => new SaveableEntry()
    //        {
    //            Instance = c,
    //            Settings = (c as ISaveable).Settings,
    //            EntryName = c.ElementName,
    //            TypeName = c.GetType().FullName
    //        }).ToList();

    //        set
    //        {

    //        }
    //    } //= new List<SaveableEntry>();

    //}

}