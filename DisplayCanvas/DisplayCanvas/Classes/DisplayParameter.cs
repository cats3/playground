﻿using C3.Common.Models;
using C3.Common.WPF.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DisplayCanvas
{

    public interface IDisplayParameter
    {
        object Value { get; set; }
      

        eDisplayType DisplayType
        { get; }

        string Name
        { get; }

    }

    public enum eDisplayType
    {
        Numeric,
        Text,
        List,
        Unknown,
    }

    public class DisplayParameter<T> : BaseModel, IDisplayParameter
    {
        private T _value;
        public T Value
        { get =>_value;
            set
            {
                _value = value;
                ValueChangedAction?.Invoke((T)value);
                OnPropertyChanged();
            }
        }

        object IDisplayParameter.Value { get => Value; set => Value =  (T)getRealValue(value); }


        object getRealValue(object o)
        {
            if (IsNumber(Value) && !IsNumber(o))
            {
                return double.Parse(o.ToString()) as object;


            }

            return o;
           
        }



        public static bool IsNumber(object value)
        {
            return value is sbyte
                    || value is byte
                    || value is short
                    || value is ushort
                    || value is int
                    || value is uint
                    || value is long
                    || value is ulong
                    || value is float
                    || value is double
                    || value is decimal;
        }

        private eDisplayType evaluateDisplayType()
        {
            if (Value is string)
            {
                return eDisplayType.Text;
            }
            if (Value is System.Collections.IEnumerable)
            {
                return eDisplayType.List;
            }
            if (IsNumber(Value))
            {
                return eDisplayType.Numeric;
            }

            return eDisplayType.Unknown;
        }

        public eDisplayType DisplayType => _displayType == eDisplayType.Unknown ? (_displayType = evaluateDisplayType()) : _displayType;

        public string Name { get => _name; set { _name = value; NameChangedAction?.Invoke(_name); OnPropertyChanged(); } }

        private string _name = "";

        private eDisplayType _displayType = eDisplayType.Unknown;

      

        public Action<T> ValueChangedAction
        { get; set; }

        public Action<string> NameChangedAction
        { get; set; }

      
    }
}
