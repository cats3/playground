﻿using ProjectExplorer.MVVM.ViewModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HostApplication
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window, INotifyPropertyChanged
    {
        public ProjectExplorerViewModel ProjectExplorerViewModel{ get; set; }
        public MainWindow()
        {
            InitializeComponent();
            DataContext = this;

            string path = "C:\\Cubus Projects\\cfg\\explorer.cfg";
            ProjectExplorerViewModel = new ProjectExplorerViewModel(new FilesystemExplorerService(path));
            OnPropertyChanged(nameof(ProjectExplorerViewModel));    
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string name = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}
