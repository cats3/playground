﻿using Microsoft.WindowsAPICodePack.Dialogs;
using ProjectExplorer.MVVM.ViewModel;
using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Markup;

namespace ProjectExplorer
{
    public partial class ProjectExplorerControl : UserControl
    {
        public ICommand AcquisitionFileOpenCommand
        {
            get { return (ICommand)GetValue(AcquisitionFileOpenCommandProperty); }
            set { SetValue(AcquisitionFileOpenCommandProperty, value); }
        }
        public static readonly DependencyProperty AcquisitionFileOpenCommandProperty =
            DependencyProperty.Register("AcquisitionFileOpenCommand", typeof(ICommand), typeof(ProjectExplorerControl), new PropertyMetadata(null));


        public ICommand ProjectFileOpenCommand
        {
            get { return (ICommand)GetValue(ProjectFileOpenCommandProperty); }
            set { SetValue(ProjectFileOpenCommandProperty, value); }
        }
        public static readonly DependencyProperty ProjectFileOpenCommandProperty =
            DependencyProperty.Register("ProjectFileOpenCommand", typeof(ICommand), typeof(ProjectExplorerControl), new PropertyMetadata(null));


        public ICommand DeleteProjectsCommand
        {
            get { return (ICommand)GetValue(DeleteProjectsCommandProperty); }
            set { SetValue(DeleteProjectsCommandProperty, value); }
        }
        public static readonly DependencyProperty DeleteProjectsCommandProperty =
            DependencyProperty.Register("DeleteProjectsCommand", typeof(ICommand), typeof(ProjectExplorerControl), new PropertyMetadata(null));


        public ProjectExplorerControl()
        {
            InitializeComponent();
            Language = XmlLanguage.GetLanguage(Thread.CurrentThread.CurrentCulture.Name);
            Settings.Visibility = Visibility.Collapsed;
        }

        private void DeleteSelectedProjectButton_Click(object sender, RoutedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void AddRepostioryButton_Click(object sender, RoutedEventArgs e)
        {
            var dc = DataContext as ProjectExplorerViewModel;
            CommonOpenFileDialog dialog = new CommonOpenFileDialog
            {
                IsFolderPicker = true
            };
            var result = dialog.ShowDialog();
            if (result == CommonFileDialogResult.Ok && !String.IsNullOrEmpty(dialog.FileName))
            {
                dc.AddRepository(dialog.FileName);
            }
        }

        private void OpenCloseSettings_Click(object sender, RoutedEventArgs e)
        {
            if (Settings.Visibility == Visibility.Collapsed)
                Settings.Visibility = Visibility.Visible;
            else
                Settings.Visibility = Visibility.Collapsed;
        }

        private void AcquisitionListView_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (sender is ListView listView && e.ClickCount == 2)
            {
                if (AcquisitionFileOpenCommand != null)
                {
                    AcquisitionFileOpenCommand.Execute(listView.SelectedItem);
                }
            }
        }

        private void ProjectDataGrid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (sender is DataGrid dataGrid && e.ClickCount == 2)
            {
                if (ProjectFileOpenCommand != null)
                {
                    ProjectFileOpenCommand.Execute(dataGrid.SelectedItem);
                }
            }
        }

        private void ProjectDataGrid_KeyDown(object sender, KeyEventArgs e)
        {
            if (sender is DataGrid dataGrid && (e.Key == Key.Delete))
            {
                if (DeleteProjectsCommand != null)
                {
                    DeleteProjectsCommand.Execute(dataGrid.SelectedItems);
                }
            }
        }
    }
}
