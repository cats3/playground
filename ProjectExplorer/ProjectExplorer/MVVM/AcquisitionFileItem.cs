﻿using System.IO;

namespace ProjectExplorer.MVVM.Model
{
    class AcquisitionFileItem
    {
        public string FilePath { get; set; }
        public string FileName
        {
            get { return Path.GetFileName(FilePath); }
            private set { }
        }
        public AcquisitionFileItem(string path)
        {
            FilePath = path;
        }
    }
}
