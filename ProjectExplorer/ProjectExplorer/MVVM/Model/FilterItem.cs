﻿
namespace ProjectExplorer.MVVM.Model
{
    public class FilterItem
    {
        public string Name { get; set; }
        public FilterItem(string name)
        {
            Name = name;
        }
    }
}
