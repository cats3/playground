﻿using System.IO;

namespace ProjectExplorer.MVVM.Model
{
    public class AcquisitionFileItem
    {
        public string FilePath { get; set; }
        public string FileName
        {
            get { 
                if (File.Exists(FilePath))
                    return Path.GetFileName(FilePath);
                return default;
            }
            private set { }
        }
        public AcquisitionFileItem() : this(null) { }
        public AcquisitionFileItem(string path)
        {
            FilePath = path;
        }
    }
}
