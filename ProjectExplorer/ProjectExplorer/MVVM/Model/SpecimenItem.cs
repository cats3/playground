﻿using ProjectExplorer.Core;
using System;
using System.Collections.Generic;
using System.IO;

namespace ProjectExplorer.MVVM.Model
{
    public class SpecimenItem
    {
        public DateTime TestDate { get; private set; }
        public string FolderPath { get; private set; }
        public string Name { get; private set; }

        public string State { get; private set; } = "Passed";

        public List<AcquisitionFileItem> AcquisitionFiles { get; set; }
        public SpecimenItem(string path)
        {
            FolderPath = path;
            TestDate = Directory.GetCreationTime(path);
            Name = new DirectoryInfo(path).Name;
            AcquisitionFiles = GetAcquistionFilesFromPath(path);
        }

        private List<AcquisitionFileItem> GetAcquistionFilesFromPath(string path)
        {
            List<AcquisitionFileItem> acquisitionFiles = new List<AcquisitionFileItem>();
            if (String.IsNullOrEmpty(path))
                return acquisitionFiles;

            foreach (var f in Directory.EnumerateFiles(path))
            {
                if (AcquisitionTypes.AcquistionFileExtenstions.Contains(Path.GetExtension(f)))
                    acquisitionFiles.Add(new AcquisitionFileItem(f));
            }

            return acquisitionFiles;
        }
    }
}
