﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectExplorer.MVVM.Model
{
    public class Repository
    {
        public string Path { get; set; }
        public bool Enabled { get; set; }
    }
}
