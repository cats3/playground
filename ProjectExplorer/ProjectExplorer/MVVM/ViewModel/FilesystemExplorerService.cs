﻿using ProjectExplorer.MVVM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectExplorer.MVVM.ViewModel
{
    public class FilesystemExplorerService : IExplorerService
    {
        ExplorerSettings ExplorerSettings { get; set; }
        public FilesystemExplorerService(string path)
        {
            ExplorerSettings = ExplorerSettings.LoadFrom(path);
        }
        public Repository ActiveRepository => ExplorerSettings.ActiveRepository;

        public IEnumerable<Repository> GetAllRepositories() => ExplorerSettings.Repositories;

        public Task SetDefaultRepository(Repository repository)
        {
            throw new NotImplementedException();
        }
    }
}
