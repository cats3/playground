﻿using ProjectExplorer.MVVM.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectExplorer.MVVM.ViewModel
{
    public interface IProjectService
    {
        Task<List<ProjectItem>> GetProjects();
        event EventHandler ProjectsChanged;
    }
}