﻿using ProjectExplorer.MVVM.Model;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ProjectExplorer.MVVM.ViewModel
{
    public interface IExplorerService
    {
        Task SetDefaultRepository(Repository repository);
        Repository ActiveRepository { get; }
        IEnumerable<Repository> GetAllRepositories();
    }
}