﻿using ProjectExplorer.Core;
using ProjectExplorer.MVVM.Model;
using System;
using System.ComponentModel;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Windows.Data;
using System.Collections.Generic;
using System.Windows;
using System.Threading.Tasks;
using System.Diagnostics;
using System.Collections.Concurrent;
using GalaSoft.MvvmLight.Command;

namespace ProjectExplorer.MVVM.ViewModel
{
    public class RepositoryViewModel : ObservableObject, IDisposable
    {
        //private readonly string DEFAULT_REPOSITORY_PATH = @"C:\Cubus Projects\";
        //private BlockingCollection<string> _collection = new BlockingCollection<string>();
        public bool IsRefreshing { get; set; }
        public ObservableCollection<ProjectViewModel> Projects { get; private set; } = new ObservableCollection<ProjectViewModel>();

        public CollectionViewSource ProjectsViewSource { get; set; }
        
        private ProjectViewModel _selectedProject;
        public ProjectViewModel SelectedProject { get { return _selectedProject; } set { _selectedProject = value; OnPropertyChanged(nameof(SelectedProject)); } }

        public string Path { get { return _repository.Path; } private set { } }

        private IProjectService _projectService;
        private Repository _repository;
        //private FileSystemWatcher watcher;
        public RepositoryViewModel(Repository repository, IProjectService projectService)
        {
            _repository = repository;
            _projectService = projectService;
            _projectService.ProjectsChanged += _projectService_ProjectsChanged;
            _repository.Path = System.IO.Path.GetFullPath(_repository.Path).TrimEnd('\\');

            ProjectsViewSource = new CollectionViewSource
            {
                Source = Projects
            };
            GetProjects();
        }

        private void _projectService_ProjectsChanged(object sender, EventArgs e)
        {
            Application.Current.Dispatcher.Invoke(delegate { GetProjects(); });
        }

        private async void GetProjects()
        {
            _projectService.ProjectsChanged -= _projectService_ProjectsChanged;
            List<ProjectItem> foo = await _projectService.GetProjects();
            Projects.Clear();
            foreach (var item in foo)
            {
                Projects.Add(new ProjectViewModel(item));
            }
            OnPropertyChanged(nameof(Projects));
            _projectService.ProjectsChanged += _projectService_ProjectsChanged;
        }

        //private void WatcherEvent(object sender, FileSystemEventArgs e)
        //{
        //    Application.Current.Dispatcher.Invoke(delegate { InitialiseProjectList(); });
        //}

        //private async void InitialiseProjectList()
        //{
        //    Stopwatch watch = new Stopwatch();

        //    IsRefreshing = true;
        //    OnPropertyChanged(nameof(IsRefreshing));

        //    try
        //    {
        //        Projects.Clear();
        //        watch.Start();

        //        using (BlockingCollection<string> bc = new BlockingCollection<string>())
        //        {
        //            Task t1 = Task.Run(() =>
        //            {
        //                try
        //                {
        //                    foreach (var p in Directory.EnumerateFiles(Path, "*.*", SearchOption.AllDirectories).Where(e => CheckForPathExtension(e)))
        //                    {
        //                        bc.Add(p);
        //                    }
        //                }
        //                finally
        //                {
        //                    bc.CompleteAdding();
        //                }
        //            });
                    
        //            List<ProjectViewModel> list = new List<ProjectViewModel>();
        //            Task t2 = Task.Run(() =>
        //            {
        //                try
        //                {
        //                    while (true)
        //                    {
        //                        var projectVM = new ProjectViewModel(new ProjectItem(bc.Take()));
        //                        //await Task.Run(() =>
        //                        //{
        //                        //    Application.Current?.Dispatcher.Invoke(() => Projects.Add(projectVM));
        //                        //});
        //                        list.Add(projectVM);
        //                        //Application.Current.Dispatcher.Invoke(delegate { Projects.Add(projectVM); }); 
        //                    }
        //                }
        //                catch (InvalidOperationException)
        //                {
        //                    // Take() on completed collection
        //                }
        //            });
        //            await Task.WhenAll(t1, t2);
        //            watch.Stop();
        //            Task t3 = Task.Run(async () =>
        //            {
        //                foreach (var p in list)
        //                    await Application.Current?.Dispatcher.InvokeAsync(() => Projects.Add(p));
        //            });
        //            await Task.WhenAll(t3);
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //    }
        //    IsRefreshing = false;
        //    OnPropertyChanged(nameof(IsRefreshing));
        //    OnPropertyChanged(nameof(Projects));
        //}

        //private bool CheckForPathExtension(string path)
        //{
        //    if (String.IsNullOrEmpty(path))
        //        return false;
        //    return ProjectTypes.ProjectExtensions.Contains(System.IO.Path.GetExtension(path)) && !path.Contains(".backup");
        //}

        public void Dispose()
        {
            //if (watcher != default)
            //{
            //    watcher.EnableRaisingEvents = false;
            //    watcher.Changed -= WatcherEvent;
            //    watcher.Created -= WatcherEvent;
            //    watcher.Deleted -= WatcherEvent;
            //    watcher.Renamed -= WatcherEvent;
            //    watcher.Dispose();
            //}
        }
    }
}
