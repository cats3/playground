﻿using ProjectExplorer.Core;
using ProjectExplorer.MVVM.Model;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectExplorer.MVVM.ViewModel
{
    public class FilesystemProjectService : IProjectService
    {
        private FileSystemWatcher watcher;
        internal int events = 0;

        public event EventHandler ProjectsChanged;
        private string _path;

        public FilesystemProjectService(string path)
        {
            _path = path;
        }

        public Task<List<ProjectItem>> GetProjects()
        {
            var projects = new List<ProjectItem>();
            try
            {
                foreach (var p in Directory.EnumerateFiles(_path, "*.*", SearchOption.AllDirectories).Where(e => CheckForPathExtension(e)))
                {
                    projects.Add(new ProjectItem(p));
                }
                CreatePathWatcher();
            }
            catch
            {

            }
            return Task.FromResult(projects);
        }
        private void CreatePathWatcher()
        {
            try
            {
                watcher = new FileSystemWatcher(_path)
                {
                    NotifyFilter = NotifyFilters.DirectoryName | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.Attributes
                };
                watcher.Created += WatcherEvent;
                watcher.Deleted += WatcherEvent;
                watcher.Renamed += WatcherEvent;
                watcher.IncludeSubdirectories = true;
                watcher.EnableRaisingEvents = true;
            }
            catch
            {

            }
        }

        private void WatcherEvent(object sender, FileSystemEventArgs e)
        {
            OnProjectsChanged(e);
        }

        protected virtual void OnProjectsChanged(EventArgs e)
        {
            events++;
            ProjectsChanged?.Invoke(this, e);
        }

        private bool CheckForPathExtension(string path)
        {
            if (String.IsNullOrEmpty(path))
                return false;
            return ProjectTypes.ProjectExtensions.Contains(System.IO.Path.GetExtension(path)) && !path.Contains(".backup");
        }
    }
}
