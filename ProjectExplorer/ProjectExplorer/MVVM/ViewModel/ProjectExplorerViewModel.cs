﻿using GalaSoft.MvvmLight.Command;
using ProjectExplorer.Core;
using ProjectExplorer.MVVM.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows.Data;

namespace ProjectExplorer.MVVM.ViewModel
{
    public class ProjectExplorerViewModel : ObservableObject
    {
        #region Public Properties
        public ObservableCollection<RepositoryViewModel> Repositories { get; set; }
        public RepositoryViewModel ActiveRepository { get; set; }

        private readonly IExplorerService _explorerService;

        public ExplorerSettings ExplorerSettings { get; set; }
        public ObservableCollection<DateFilter> ModifiedDateFilters { get; set; }
        private RepositoryViewModel _selectedRepository;
        public RepositoryViewModel SelectedRepository
        {
            get { return _selectedRepository; }
            set { 
                _selectedRepository = value;
                var collectionViewSource = _selectedRepository?.ProjectsViewSource;
                using (collectionViewSource?.DeferRefresh())
                {
                    collectionViewSource.GroupDescriptions.Clear();
                    collectionViewSource.GroupDescriptions.Add(new PropertyGroupDescription(nameof(ProjectViewModel.ProjectType)));
                    collectionViewSource.SortDescriptions.Clear();
                    collectionViewSource.SortDescriptions.Add(new SortDescription(nameof(ProjectViewModel.ProjectName), ListSortDirection.Ascending));
                    collectionViewSource.Filter -= CollectionViewSource_Filter;
                    collectionViewSource.Filter += CollectionViewSource_Filter;
                }
                OnPropertyChanged(nameof(SelectedRepository));
            }
        }

        private DateFilter _SelectedModifiedDate;
        public DateFilter SelectedModifiedDate
        { 
            get { return _SelectedModifiedDate; }
            set
            {
                _SelectedModifiedDate = value;
                _selectedRepository?.ProjectsViewSource.View.Refresh();
            }
        }

        private string _filterText;
        public string FilterText
        {
            get { return _filterText ?? String.Empty; }
            set { _filterText = value; _selectedRepository?.ProjectsViewSource.View.Refresh(); }
        }

        private RelayCommand<Repository> removeRepository;
        public RelayCommand<Repository> RemoveRepositoryCommand
        {
            get { return removeRepository ?? (removeRepository = new RelayCommand<Repository>(ExecuteRemoveRepository, (arg) => true)); }
        }
        #endregion

        #region Constructor
        public ProjectExplorerViewModel(IExplorerService explorerService)
        {
            _explorerService = explorerService;
            Repositories = new ObservableCollection<RepositoryViewModel>();
            foreach (var r in _explorerService.GetAllRepositories())
            {
                Repositories.Add(new RepositoryViewModel(r, new FilesystemProjectService(r.Path)));
            }

            ModifiedDateFilters = new ObservableCollection<DateFilter>
            {
                { new DateFilter() },
                { new DateFilter{ Name="Today", From = DateTime.Today, To = DateTime.Today.AddDays(1).AddTicks(-1)} },
                { new DateFilter{ Name="Yesterday", From = DateTime.Today.AddDays(-1), To=DateTime.Today.AddTicks(-1) } },
                { new DateFilter{ Name="This week", From = DateTime.Today.AddDays(-7), To=DateTime.Today.AddTicks(-1)} },
                { new DateFilter{ Name="This month", From = DateTime.Today.FirstDayOfMonth(), To=DateTime.Today.AddTicks(-1)} },
                { new DateFilter{ Name="This year", From = DateTime.Today.FirstDayOfYear(), To=DateTime.Today.AddTicks(-1)} },
            };
            SelectedModifiedDate = ModifiedDateFilters.First();

            ActiveRepository = Repositories.First(r=>r.Path == _explorerService.ActiveRepository.Path);
            SelectedRepository = Repositories.First(r => r.Path == ActiveRepository?.Path);

            OnPropertyChanged(nameof(ActiveRepository));
            OnPropertyChanged(nameof(SelectedRepository));
        }
        #endregion

        #region Methods
        public void AddRepository(string path)
        {
            //if (!String.IsNullOrEmpty(path) && Directory.Exists(path))
            //{
            //    var existingPaths = Repositories.Any(a=>Path.GetFullPath(a.Path) == Path.GetFullPath(path));
            //    if (!existingPaths)
            //    {
            //        Repository r = new Repository() { Path = path, Enabled = true };
            //        ExplorerSettings.Repositories.Add(r);
            //        ExplorerSettings.Save();
            //        Repositories.Add(new RepositoryViewModel(r));
            //        OnPropertyChanged(nameof(Repositories));
            //    }
            //}
        }
        public void DeleteSelectedProjects()
        {
            foreach(var r in Repositories)
            {
                var foo = r.Projects.ToList().Where(p => p.IsSelected);
                /* recursively delete the folder specified by the project path */
                foreach (var f in foo)
                {
                    Directory.Delete(Path.GetDirectoryName(f.ProjectPath), true);
                }
            }
        }
        #endregion

        #region Helper Methods
        private void CollectionViewSource_Filter(object sender, FilterEventArgs e)
        {
            if (e.Item is ProjectViewModel project)
            {
                e.Accepted = project.Modified.IsWithin(SelectedModifiedDate);
                if (e.Accepted)
                {
                    var splitFilterText = FilterText.Split(' ');
                    foreach (var sp in splitFilterText)
                    {
                        e.Accepted &= project.ProjectName.Contains(sp, StringComparison.InvariantCultureIgnoreCase);
                    }
                }
            }
        }
        #endregion

        #region Relay Commands
        private void ExecuteRemoveRepository(Repository obj)
        {
            if (ExplorerSettings.Repositories.Contains(obj))
            {
                ExplorerSettings.Repositories.Remove(obj);
                ExplorerSettings.Save();
                Repositories.Remove(Repositories.Where(r => r.Path == obj.Path).First());
            }
        }
        #endregion
    }
}
