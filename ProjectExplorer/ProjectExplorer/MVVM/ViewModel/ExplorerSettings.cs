﻿using ProjectExplorer.Core;
using ProjectExplorer.MVVM.Model;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Xml.Serialization;
using System.Linq;

namespace ProjectExplorer.MVVM.ViewModel
{
    [Serializable]
    public class ExplorerSettings : ObservableObject
    {
        private ExplorerSettings()
        {
        }

        private string _path = "";
        private ObservableCollection<Repository> _repositories = new ObservableCollection<Repository>();

        [XmlElement("Repositories")]
        public ObservableCollection<Repository> Repositories
        {
            get { return _repositories; }
            set
            {
                _repositories = value;
                Save();
                OnPropertyChanged(nameof(Repositories));
            }
        }

        [XmlElement("LastOpenedProject")]
        public string LastOpenedProject
        {
            get;
            set;
        }
        private Repository _activeRepository;
        public Repository ActiveRepository
        {
            get { return Repositories.FirstOrDefault(r=>r.Path == _activeRepository.Path); } 
            set { 
                _activeRepository = value;
                Save();
                OnPropertyChanged(nameof(ActiveRepository));
            }
        }

        private void saveFile()
        {
            try
            {
                bool wasHidden = false;
                if ((new DirectoryInfo(Path.GetDirectoryName(_path)).Attributes & FileAttributes.Hidden) == FileAttributes.Hidden)
                {
                    var di = new DirectoryInfo(Path.GetDirectoryName(_path)).Attributes &= ~FileAttributes.Hidden;
                    wasHidden = true;
                }

                XmlSerializer serializer = new XmlSerializer(typeof(ExplorerSettings));
                if (File.Exists(_path))
                {
                    File.SetAttributes(_path, FileAttributes.Normal);
                    File.Delete(_path);
                }

                using (var writer = new StreamWriter(_path))
                {
                    serializer.Serialize(writer.BaseStream, this);
                    File.SetAttributes(_path, FileAttributes.Hidden);
                }
                if (wasHidden)
                    new DirectoryInfo(Path.GetDirectoryName(_path)).Attributes |= FileAttributes.Hidden;
            }
            catch { }
            isSaving = false;
        }

        bool isSaving = false;

        public void Save()
        {
            if (isSaving)
                return;
            isSaving = true;
            saveFile();
            //ManagedTask.Run("Save Exporer Settings", saveFile, throwUnhandledExceptions: false);
        }

        public static ExplorerSettings LoadFrom(string path)
        {
            var l = new ExplorerSettings();
            if (File.Exists(path))
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ExplorerSettings));
                    using (var reader = new StreamReader(path))
                        l = (ExplorerSettings)serializer.Deserialize(reader);
                }
                catch
                { }
            }
            l._path = path;
            return l;
        }
    }
}
