﻿using ProjectExplorer.Core;
using ProjectExplorer.MVVM.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace ProjectExplorer.MVVM.ViewModel
{
    public class SpecimenViewModel : ObservableObject
    {
        private SpecimenItem _specimenItem;
        public DateTime TestDate { get { return _specimenItem.TestDate; } private set { } }
        public string FolderPath { get { return _specimenItem.FolderPath; } private set { } }
        public string Name { get { return _specimenItem.Name; } private set { } }
        public string State { get { return _specimenItem.State; } private set { } }
        public List<AcquisitionFileItem> AcquisitionFiles { get { return _specimenItem.AcquisitionFiles; } private set { } }
        public bool HasAcquisitionFiles => AcquisitionFiles.Count > 0;
        public AcquisitionFileItem SelectedAcquistionFile { get; set; }
        public SpecimenViewModel(SpecimenItem item)
        {
            _specimenItem = item;
            //AcquisitionFiles = new List<AcquisitionFileItem>();
            //if (_specimenItem.AcquisitionFiles != default)
            //{
            //    foreach(var s in _specimenItem.AcquisitionFiles)
            //    {
            //        AcquisitionFiles.Add(new AcquisitionFileItem(s));
            //    }
            //    OnPropertyChanged(nameof(HasAcquisitionFiles));
            //}
        }
    }
}
