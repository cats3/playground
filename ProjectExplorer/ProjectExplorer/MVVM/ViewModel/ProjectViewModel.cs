﻿using GalaSoft.MvvmLight.Command;
using ProjectExplorer.Core;
using ProjectExplorer.MVVM.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace ProjectExplorer.MVVM.ViewModel
{
    public class ProjectViewModel : ObservableObject
    {
        private ProjectItem projectItem;
        public string ProjectPath { get { return projectItem.ProjectPath; } private set { } }
        public string ProjectName { get { return projectItem.ProjectName; } private set { } }
        public string ProjectType { get { return projectItem.ProjectType; } private set { } }
        public DateTime Created { get { return projectItem.Created; } private set { } }
        public DateTime Modified { get { return projectItem.Modified; } private set { } }

        private bool isSelected = false;
        public bool IsSelected { get { return isSelected; } set { isSelected = value; OnPropertyChanged(nameof(IsSelected)); } }
        public List<SpecimenViewModel> Specimens{ get; set; }
        public bool HasSpecimens {
            get { return (Specimens.Count > 0); } private set { }
        }
        public ProjectViewModel() : this(null)
        {

        }
        public ProjectViewModel(ProjectItem item)
        {
            projectItem = item ?? new ProjectItem();
            Specimens = new List<SpecimenViewModel>();
            foreach (var s in projectItem.Specimens)
            {
                Specimens.Add(new SpecimenViewModel(s));
            }
            OnPropertyChanged(nameof(HasSpecimens));
        }

    }
}
