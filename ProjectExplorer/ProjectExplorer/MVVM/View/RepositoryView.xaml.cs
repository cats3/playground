﻿using ProjectExplorer.MVVM.ViewModel;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Input;

namespace ProjectExplorer.MVVM.View
{
    /// <summary>
    /// Interaction logic for RepositoryView.xaml
    /// </summary>
    public partial class RepositoryView : UserControl
    {
        public RepositoryView()
        {
            InitializeComponent();
        }

        private void ListViewItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            if (!e.Handled)
            {
                var element = ((ListViewItem)sender).Content as ProjectViewModel;
                Debug.WriteLine($"Ouch! A project called {element.ProjectPath}");
            }
        }
    }
}
