﻿using ProjectExplorer.MVVM.ViewModel;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Input;

namespace ProjectExplorer.MVVM.View
{
    /// <summary>
    /// Interaction logic for SpecimenView.xaml
    /// </summary>
    public partial class SpecimenView : UserControl
    {
        public SpecimenView()
        {
            InitializeComponent();
        }

        private void ListViewItem_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var element = ((ListViewItem)sender).Content as AcquisitionFileItemViewModel;
            Debug.WriteLine($"An acquisition file called {element.FileName}!");
            e.Handled = true;
        }
    }
}
