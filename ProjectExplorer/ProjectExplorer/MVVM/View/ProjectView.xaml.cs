﻿using ProjectExplorer.MVVM.ViewModel;
using System.Diagnostics;
using System.Windows.Controls;
using System.Windows.Input;

namespace ProjectExplorer.MVVM.View
{
    /// <summary>
    /// Interaction logic for ProjectView.xaml
    /// </summary>
    public partial class ProjectView : UserControl
    {
        public ProjectView()
        {
            InitializeComponent();
        }

        private void ListViewItem_PreviewMouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var foo = SpecimenListView.SelectedItem as SpecimenViewModel;
            if (foo == default)
                return;
            if (foo.SelectedAcquistionFile != default)
                return;

            var element = ((ListViewItem)sender).Content as SpecimenViewModel;
            Debug.WriteLine($"Ouch! A specimen called {element.FolderPath}");
            e.Handled = true;
        }
    }
}
