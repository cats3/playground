﻿using System;

namespace ProjectExplorer.Core
{
    public class DateFilter
    {
        public DateTime From { get; set; }
        public DateTime To { get; set; }
        public string Name { get; set; }
        public DateFilter()
        {
            From = DateTime.MinValue;
            To = DateTime.MaxValue;
            Name = "Any";
        }
    }

    public static class DateTimeExtension
    {
        public static bool IsWithin(this DateTime date, DateFilter filter)
        {
            return date >= filter.From && date <= filter.To;
        }
        public static DateTime FirstDayOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, 1);
        }
        public static int DaysInMonth(this DateTime date)
        {
            return DateTime.DaysInMonth(date.Year, date.Month);
        }
        public static DateTime LastDayOfMonth(this DateTime date)
        {
            return new DateTime(date.Year, date.Month, date.DaysInMonth());
        }
        public static DateTime FirstDayOfYear(this DateTime date)
        {
            return new DateTime(date.Year, 1, 1);
        }
    }

}
