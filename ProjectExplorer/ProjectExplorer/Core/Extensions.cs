﻿using System;

namespace ProjectExplorer.Core
{
    public static partial class Extensions
    {
        public static bool Contains(this string @this, string value, StringComparison comparisonType)
        {
            return @this.IndexOf(value, comparisonType) != -1;
        }
    }
}
