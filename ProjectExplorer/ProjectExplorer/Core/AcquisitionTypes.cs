﻿using System.Collections.Generic;

namespace ProjectExplorer.Core
{
    static class AcquisitionTypes
    {
        public static List<string> AcquistionFileExtenstions = new List<string>()
        {
            ".qper",
            ".qtpd",
            ".qlog",
            ".qres",
            ".qdrv",
            ".qfield",
            ".qdesres",
            ".qfindrv",
            ".qfinres",
            ".qrefres",
            ".qdurres",
            ".csf",
        };

    }
}
