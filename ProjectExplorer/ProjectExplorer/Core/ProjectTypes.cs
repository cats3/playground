﻿using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace ProjectExplorer.Core
{
    public static class ProjectTypes
    {
        private static readonly string GENERIC_STRING = "Generic";
        private static readonly string GENERIC_EXTENSION = ".cprj";
        private static Dictionary<string, string> projectExtensions = new Dictionary<string, string>()
        {
            { ".proj_sba", "SBA" },
            { ".proj_advcyc", "Cyclic Pro" },
            { ".proj_blk", "Block" },
            { ".proj_ramp", "Ramp" },
            { ".proj_dura", "Dura" },
            { ".proj_ext", "External" },
            { ".proj_cyc", "Cyclic" },
            { ".proj_tensile", "Tensile" },
            { ".proj_side", "Side Intrusion" },
            { ".proj_albcomp", "Albany Compression" },
            { ".proj_damper", "Damper" },
            { GENERIC_EXTENSION, GENERIC_STRING }
        };

        public static List<string> ProjectExtensions => projectExtensions.Keys.ToList();
        
        public static string GetProjectTypeFromPathExtension(string file)
        {
            var ext = Path.GetExtension(file);
            if (projectExtensions.ContainsKey(ext))
                return projectExtensions[ext];
            else
                return "Uncategorised";
        }
        
        public static bool IsGenericProjectType(string file)
        {
            return Path.GetExtension(file).Equals(GENERIC_EXTENSION);
        }
    }
}
