﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace ProjectExplorer.Core
{
    public abstract class NullConverter<T> : IValueConverter
    {
        protected NullConverter(T trueValue, T falseValue)
        {
            this.TrueValue = trueValue;
            this.FalseValue = falseValue;
        }

        [Editor(typeof(System.ComponentModel.ComponentEditor), typeof(System.ComponentModel.ComponentEditor))]
        public T TrueValue { get; set; }
        [Editor(typeof(System.ComponentModel.ComponentEditor), typeof(System.ComponentModel.ComponentEditor))]
        public T FalseValue { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => value == null ? TrueValue : FalseValue;
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => throw new NotImplementedException();
    }

    [ValueConversion(typeof(object), typeof(Visibility))]
    public class NullToVisibilityConverter : NullConverter<Visibility>
    {
        public NullToVisibilityConverter() 
            : base(Visibility.Collapsed, Visibility.Visible)
        {
        }
    }

    [ValueConversion(typeof(object), typeof(Visibility))]
    public class NotNullNullToVisibilityConverter : NullConverter<Visibility>
    {
        public NotNullNullToVisibilityConverter()
            : base(Visibility.Visible, Visibility.Collapsed)
        {
        }
    }

    public abstract class BooleanConverter<T> : IValueConverter
    {
        protected BooleanConverter(T trueValue, T falseValue)
        {
            this.TrueValue = trueValue;
            this.FalseValue = falseValue;
        }

        [Editor(typeof(System.ComponentModel.ComponentEditor), typeof(System.ComponentModel.ComponentEditor))]
        public T TrueValue { get; set; }

        public T FalseValue { get; set; }

        public T NullValue { get; set; }

        public bool IsThreeState { get; set; }

        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if(IsThreeState)
            {
                switch (value)
                {
                    case null:
                        return this.NullValue;
                    case true:
                        return this.TrueValue;
                    case false:
                        return this.FalseValue;
                }
            }

            return Object.Equals(value, true) ?
                this.TrueValue : this.FalseValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            if (IsThreeState)
            {
                if (Object.Equals(value, this.TrueValue)) return true;
                if (Object.Equals(value, this.NullValue)) return null;
                if (Object.Equals(value, this.FalseValue)) return false;
            }

            return Object.Equals(value, this.TrueValue) ? true : false;
        }
    }

    [ValueConversion(typeof(bool), typeof(bool))]
    public sealed class BooleanToObjectConverter
    : BooleanConverter<object>
    {
        public BooleanToObjectConverter()
            : base(null, null)
        { }

        public static BooleanInvertConverter Converter => new BooleanInvertConverter();
    }

    [ValueConversion(typeof(bool), typeof(bool))]
    public sealed class BooleanInvertConverter
        : BooleanConverter<bool>
    {
        public BooleanInvertConverter()
            : base(false, true)
        { }

        public static BooleanInvertConverter Converter => new BooleanInvertConverter();
    }

    [ValueConversion(typeof(bool), typeof(double))]
    public sealed class BooleanToDoubleConverter
        : BooleanConverter<double>
    {
        public BooleanToDoubleConverter()
            : base(1.0, 0.5) { }
    }

    [ValueConversion(typeof(bool), typeof(int))]
    public sealed class BooleanToIntConverter
        : BooleanConverter<int>
    {
        public BooleanToIntConverter()
            : base(1, 0) { }
    }

    [ValueConversion(typeof(bool), typeof(Visibility))]
    public sealed class BooleanToVisibilityConverter
         : BooleanConverter<Visibility>
    {
        public BooleanToVisibilityConverter()
            : base(Visibility.Visible, Visibility.Collapsed) { }
    }

    [ValueConversion(typeof(bool), typeof(Visibility))]
    public sealed class InvertedBooleanToVisibilityConverter
        : BooleanConverter<Visibility>
    {
        public InvertedBooleanToVisibilityConverter()
            : base(Visibility.Collapsed, Visibility.Visible) { }
    }

    [ValueConversion(typeof(bool), typeof(Brush))]
    public sealed class BooleanToBrushConverter
         : BooleanConverter<Brush>
    {
        public BooleanToBrushConverter()
            : base(Brushes.Black, Brushes.Gray)
        { }
    }

    [ValueConversion(typeof(bool), typeof(GridLength))]
    public sealed class BooleanToGridLengthConverter
         : BooleanConverter<GridLength>
    {
        public BooleanToGridLengthConverter()
            : base(new GridLength(0), new GridLength(1, GridUnitType.Star))
        { }
    }
}
