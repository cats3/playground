﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProjectExplorer.Core
{
    static class Features
    {
        private static Dictionary<string, GenericTestConfig> customTests = new Dictionary<string, GenericTestConfig>()
        {
            { "f80b6499-e197-4133-8e03-6091a71761d9", new GenericTestConfig(){Name = "Elastomer"} },
            { "6cd35823-f405-4950-9265-3f729b9401ed", new GenericTestConfig(){Name = "Block"} },
        };
        public static Dictionary<string, GenericTestConfig> CustomTests { get { return customTests; } }
    }

}
